package absa.cgs.com.ui.screens.apis.readcustomerprofileeapicall

import absa.cgs.com.data.RetrofitClient
import absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.model.ReadCustomerProfileARequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.model.ReadCustomerProfileAResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilebapicall.model.ReadCustomerProfileBRequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilebapicall.model.ReadCustomerProfileBResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilecapicall.model.ReadCustomerProfileCRequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilecapicall.model.ReadCustomerProfileCResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofiledapicall.model.ReadCustomerProfileDRequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofiledapicall.model.ReadCustomerProfileDResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofileeapicall.model.ReadCustomerProfileERequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofileeapicall.model.ReadCustomerProfileEResponseModel
import absa.cgs.com.ui.screens.apis.readprofileapicall.model.ReadProfileRequestModel
import absa.cgs.com.ui.screens.apis.readprofileapicall.model.ReadProfileResponseModel
import absa.cgs.com.utils.enums.HttpEnum
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ReadCustomerProfileEInteractor @Inject constructor() {

    private var readCustomerProfileEResponseModel: ReadCustomerProfileEResponseModel? = null


    interface OnCallHitListener {
        fun onSuccessReadCustomerEProfileInteractListener(readCustomerProfileEResponseModel: ReadCustomerProfileEResponseModel)
        fun onRetrofitFailureReadCustomerEProfileInteractListener(error: String)
        fun onSessionExpireReadCustomerEProfileInteractListener()
        fun onErrorReadCustomerEProfileInteractListener(readCustomerProfileEResponseModel: ReadCustomerProfileEResponseModel)
        fun onServerExceptionReadCustomerEProfileInteractListener()
    }


    fun getReadCustomerProfileEDataToServer(readCustomerProfileERequestModel: ReadCustomerProfileERequestModel, listener: OnCallHitListener) {
        RetrofitClient.instance.readCustomerProfileEData(readCustomerProfileERequestModel)
                .enqueue(object : Callback<ReadCustomerProfileEResponseModel> {
                    override fun onFailure(call: Call<ReadCustomerProfileEResponseModel>, t: Throwable) {
                        listener.onRetrofitFailureReadCustomerEProfileInteractListener(t.message.toString())
                    }

                    override fun onResponse(call: Call<ReadCustomerProfileEResponseModel>, response: Response<ReadCustomerProfileEResponseModel>) {
                        when (response.code()) {
                            HttpEnum.STATUS_UNAUTHORIZED.code -> {
                                listener.onSessionExpireReadCustomerEProfileInteractListener()
                            }

                            HttpEnum.STATUS_ERROR.code -> {
                                readCustomerProfileEResponseModel = response.body()
                                listener.onErrorReadCustomerEProfileInteractListener(readCustomerProfileEResponseModel!!)
                            }

                            HttpEnum.STATUS_OK.code -> {
                                readCustomerProfileEResponseModel = response.body()
                                listener.onSuccessReadCustomerEProfileInteractListener(readCustomerProfileEResponseModel!!)
                            }

                            else -> {
                                listener.onServerExceptionReadCustomerEProfileInteractListener()
                            }
                        }

                    }

                })
    }

}
