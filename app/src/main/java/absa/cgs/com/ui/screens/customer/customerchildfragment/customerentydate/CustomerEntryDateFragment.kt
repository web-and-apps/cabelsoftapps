package absa.cgs.com.ui.screens.customer.customerchildfragment.customerentydate

import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.addupdateprofiledapicall.AddUodateCustomerDView
import absa.cgs.com.ui.screens.apis.addupdateprofiledapicall.AddUpdateCustomerDPresenter
import absa.cgs.com.ui.screens.apis.addupdateprofileeapicall.AddUodateCustomerEView
import absa.cgs.com.ui.screens.apis.addupdateprofileeapicall.AddUpdateCustomerEPresenter
import absa.cgs.com.ui.screens.apis.addupdateprofileeapicall.model.CustomerAddUpdateEResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofileeapicall.ReadCustomerProfileEPresenter
import absa.cgs.com.ui.screens.apis.readcustomerprofileeapicall.ReadCustomerProfileEView
import absa.cgs.com.ui.screens.apis.readcustomerprofileeapicall.model.ReadCustomerProfileEResponseModel
import absa.cgs.com.ui.screens.base.BaseFragment
import absa.cgs.com.ui.screens.customer.CustomerBaseActivity
import absa.cgs.com.utils.SingletonUtils
import absa.cgs.com.utils.enums.CommonEnumUtils
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.customerbillingfragment.*
import kotlinx.android.synthetic.main.customerentrydatefragment.*
import javax.inject.Inject

class CustomerEntryDateFragment : BaseFragment(), CustomerBaseActivity.OnBackPressedListner, ReadCustomerProfileEView, AddUodateCustomerEView {

    lateinit var customerBaseActivity: CustomerBaseActivity

    @Inject
    lateinit var readCustomerProfileEPresenter: ReadCustomerProfileEPresenter<ReadCustomerProfileEView>

    @Inject
    lateinit var addUpdateCustomerEPresenter: AddUpdateCustomerEPresenter<AddUodateCustomerEView>

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        fragmentComponent().inject(this)
        readCustomerProfileEPresenter.attachView(activity!!, this)
        addUpdateCustomerEPresenter.attachView(activity!!, this)

    }


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.customerentrydatefragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()

    }

    override fun init() {
        customerBaseActivity = activity as CustomerBaseActivity
        onClickListeners()
    }

    override fun onBackPressed(): Boolean {
        activity!!.finish()
        return false
    }

    override fun onClickListeners() {
        super.onClickListeners()
        customerProfileEUpdateBtn.setOnClickListener {
            addUpdateCustomerEPresenter.validateAddUpdateCustomerE()
        }
    }

    override fun onSuccessReadCustomerProfileEResponse(readCustomerProfileEResponseModel: ReadCustomerProfileEResponseModel) {
        SingletonUtils.instance.customerEId = commonUtils.cutNull(readCustomerProfileEResponseModel.profile_e_details.customer_e_id)
        texteditRegisterConnectionDate.setText(commonUtils.cutNull(readCustomerProfileEResponseModel.profile_e_details.customer_e_connection_date))
        texteditRegisterRgeisteredDate.setText(commonUtils.cutNull(readCustomerProfileEResponseModel.profile_e_details.customer_e_register_date))
    }

    override fun onFailureReadCustomerProfileEResponse(error: String) {

    }

    override fun getCustomerID(): String {
        return SingletonUtils.instance.customerId
    }

    override fun hitGetCustomerProfileECall() {
        readCustomerProfileEPresenter.readCustomerProfileEData()
    }

    override fun onResume() {
        super.onResume()
        if (!commonUtils.cutNull(SingletonUtils.instance.customerId).isNullOrEmpty()) {
            hitGetCustomerProfileECall()
        }
    }

    override fun onSuccessAddUpdateCustomerEResponse(customerAddUpdateEResponseModel: CustomerAddUpdateEResponseModel) {
        when (commonUtils.cutNull(customerAddUpdateEResponseModel.operation)) {
            CommonEnumUtils.ADDED.toString() -> {
                customerBaseActivity.moveToProfileA()
                customerBaseActivity.hitGetCustomerTabStatusCall()
            }

            CommonEnumUtils.UPDATED.toString() -> {
                customerBaseActivity.moveToProfileA()
            }
        }
    }

    override fun onFailureAddUpdateCustomerEResponse(error: String) {

    }

    override fun getUserId(): String {
        return SingletonUtils.instance.userId
    }

    override fun getCustomerId(): String {
        return SingletonUtils.instance.customerId
    }

    override fun getCustomerEId(): String {
        return SingletonUtils.instance.customerEId
    }

    override fun getCustomerEregisterDate(): String {
        return texteditRegisterRgeisteredDate.text.toString()
    }

    override fun getCustomerEConnectionDate(): String {
        return texteditRegisterConnectionDate.text.toString()
    }

    override fun postAddUpdateCustomerE() {
        progressLoadingBar()
        addUpdateCustomerEPresenter.postCustomerEApiCall()
    }


}
