package absa.cgs.com.ui.screens.apis.addupdateprofilebapicall.model

data class CustomerAddUpdateBRequestModel(var user_id: String, var customer_id: String,
                                          var customer_b_id: String,
                                          var customer_b_billing_name: String, var customer_b_email: String,
                                          var customer_b_aadhar_no: String, var customer_b_eb_no: String,
                                          var customer_b_pan_no: String, var customer_b_security_deposite: String)