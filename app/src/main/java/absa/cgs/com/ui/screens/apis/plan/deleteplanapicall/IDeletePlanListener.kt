package absa.cgs.com.ui.screens.apis.plan.deleteplanapicall

import absa.cgs.com.ui.screens.base.BaseMvpPresenter

interface IDeletePlanListener<View : DeletePlanView> : BaseMvpPresenter<View> {
    fun deletePlanApiCall()
}