package absa.cgs.com.ui.screens.plan.planchildfragment.plandetails

import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.plan.deleteplanapicall.DeletePlanPresenter
import absa.cgs.com.ui.screens.apis.plan.deleteplanapicall.DeletePlanView
import absa.cgs.com.ui.screens.apis.plan.deleteplanapicall.model.DeletePlanResponseModel
import absa.cgs.com.ui.screens.apis.plan.getparticularplanapicall.model.GetPlanResponseModel
import absa.cgs.com.ui.screens.apis.plan.getplanapicall.GetPlanPresenter
import absa.cgs.com.ui.screens.apis.plan.getplanapicall.GetPlanView
import absa.cgs.com.ui.screens.base.BaseFragment
import absa.cgs.com.ui.screens.mainbaseactivity.OnListItemClickInterface
import absa.cgs.com.ui.screens.plan.planchildfragment.plandetails.adapter.PlanDetailsAdapter
import absa.cgs.com.ui.screens.mainbaseactivity.mainchildfragments.plans.PlanView
import absa.cgs.com.ui.screens.plan.PlanBaseActivity
import absa.cgs.com.utils.DialogUtils
import absa.cgs.com.utils.SingletonUtils
import absa.cgs.com.utils.enums.CommonEnumUtils
import absa.cgs.com.utils.enums.DialogEnum
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_plan_details_screen.*
import javax.inject.Inject

class PlanDetailsFragment : BaseFragment(), PlanView, GetPlanView, DeletePlanView, DialogUtils.OnDialogPositiveListener, PlanBaseActivity.OnBackPressedListner {


    lateinit var planBaseActivity: PlanBaseActivity

    @Inject
    lateinit var getPlanDetailsPresenter: GetPlanPresenter<GetPlanView>

    private var searchTag = ""

    private var planDetailsAdapter: PlanDetailsAdapter? = null

    @Inject
    lateinit var deletePlanPresenter: DeletePlanPresenter<DeletePlanView>


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        fragmentComponent().inject(this)
        getPlanDetailsPresenter.attachView(activity!!, this)
        deletePlanPresenter.attachView(activity!!, this)

        init()
        hitGetPlanDetails()
    }


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_plan_details_screen, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()

    }

    private fun recyclerViewInit() {
        val mLayoutManager = LinearLayoutManager(activity)
        planDetailsRcv?.layoutManager = mLayoutManager
        planDetailsRcv?.itemAnimator = DefaultItemAnimator()
    }


    override fun init() {
        planBaseActivity = activity as PlanBaseActivity
        onClickListeners()
        getAllBundleDatas()
        recyclerViewInit()
    }

    override fun onClickListeners() {
        super.onClickListeners()
    }

    override fun getAllBundleDatas() {
        if (arguments != null) {
            var FREE = arguments?.getString(CommonEnumUtils.FREE.toString())
            var PAID = arguments?.getString(CommonEnumUtils.PAID.toString())
            var BOUQ = arguments?.getString(CommonEnumUtils.BOUQ.toString())

            if (FREE != null) {
                searchTag = FREE
            }
            if (PAID != null) {
                searchTag = PAID
            }
            if (BOUQ != null) {
                searchTag = BOUQ
            }

        }
    }

    override fun onSuccessGetParticularPlanResponse(getPlanResponseModel: GetPlanResponseModel) {
        planDetailsAdapter = PlanDetailsAdapter(activity!!, getPlanResponseModel, object : OnListItemClickInterface {
            override fun OnSelectedItemClickListener(title: String, position: Int) {
                planDetailsOnClickListener(title, position, getPlanResponseModel)
            }
        }
        )
        planDetailsRcv?.adapter = planDetailsAdapter
        planDetailsAdapter!!.notifyDataSetChanged()
    }


    private fun planDetailsOnClickListener(eventString: String, position: Int, getPlanResponseModel: GetPlanResponseModel) {

        when (eventString) {
            CommonEnumUtils.VIEW.toString() -> {
                val args = Bundle()
                args.putString("plan_id", commonUtils.cutNull(getPlanResponseModel.product_details[position].plan_id))
                args.putString("plan_name", commonUtils.cutNull(getPlanResponseModel.product_details[position].plan_name))
                args.putString("plan_code", commonUtils.cutNull(getPlanResponseModel.product_details[position].plan_code))
                args.putString("plan_cost", commonUtils.cutNull(getPlanResponseModel.product_details[position].plan_cost))
                args.putString("plan_gst", commonUtils.cutNull(getPlanResponseModel.product_details[position].plan_gst))
                args.putString("plan_array_data", commonUtils.cutNull(getPlanResponseModel.product_details[position].plan_array_data))
                SingletonUtils.instance.planArrayData = commonUtils.cutNull(getPlanResponseModel.product_details[position].plan_array_data)
                args.putString("plan_tag", commonUtils.cutNull(searchTag))
                planBaseActivity.changeFragment(5, args)
            }

            CommonEnumUtils.DELETE.toString() -> {
                SingletonUtils.instance.planId = getPlanResponseModel.product_details[position].plan_id
                dialogUtils.showAlertDialog(activity!!, activity!!.resources.getString(R.string.DialogDeleteString), "", "", DialogEnum.DELETE.toString(), this)
            }
        }
    }


    override fun onFailureGetParticularPlanResponse(error: String) {

    }

    override fun getPlanIds(): String {
        return ""
    }

    override fun getFromDate(): String {
        return ""
    }

    override fun getToDate(): String {
        return ""
    }

    override fun getPageCount(): String {
        return "0"
    }

    override fun getSearchKeyword(): String {
        return ""
    }

    override fun getSearchTag(): String {
        return searchTag
    }

    override fun hitGetPlanDetails() {
        progressLoadingBar()
        getPlanDetailsPresenter.getPlanApiCall()
    }

    override fun onBackPressed(): Boolean {
        activity?.finish()
        return false
    }

    override fun setAppBarTitle(title: Int) {

    }

    override fun onDialogPositivePressed(dialog: Dialog, enumString: String) {
        when (enumString) {
            DialogEnum.DELETE.toString() -> {
                progressLoadingBar()
                deletePlanPresenter.deletePlanApiCall()
            }
        }

    }

    override fun onSuccessDeletePlanResponse(deletePlanResponseModel: DeletePlanResponseModel) {
        hitGetPlanDetails()
    }

    override fun onFailureDeletePlanResponse(error: String) {

    }

    override fun getPlanId(): String {
        return SingletonUtils.instance.planId
    }


}