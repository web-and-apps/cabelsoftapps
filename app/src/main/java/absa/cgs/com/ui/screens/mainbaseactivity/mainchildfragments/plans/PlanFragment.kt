package absa.cgs.com.ui.screens.mainbaseactivity.mainchildfragments.plans

import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.base.BaseFragment
import absa.cgs.com.ui.screens.mainbaseactivity.MainActivity
import absa.cgs.com.ui.screens.plan.PlanBaseActivity
import absa.cgs.com.utils.enums.CommonEnumUtils
import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_plan_screen.*


class PlanFragment : BaseFragment(), PlanView {
    override fun setAppBarTitle(title: Int) {

    }

    lateinit var mainActivity: MainActivity


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        fragmentComponent().inject(this)
    }


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_plan_screen, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()

    }

    override fun init() {
        mainActivity = activity as MainActivity
        onClickListeners()
    }

    override fun onClickListeners() {
        super.onClickListeners()

        freeChannelConstraint.setOnClickListener {
            navigationRoutes(PlanBaseActivity::class.java, CommonEnumUtils.FREE.toString())
        }

        paidChannelConstraint.setOnClickListener {
            navigationRoutes(PlanBaseActivity::class.java, CommonEnumUtils.PAID.toString())
        }

        bouquetsChannelConstraint.setOnClickListener {
            navigationRoutes(PlanBaseActivity::class.java, CommonEnumUtils.BOUQ.toString())
        }

        addPlanFActionButton.setOnClickListener { view ->
            mainActivity.clearAllSingletonCustomerData()
            navigationRoutes(PlanBaseActivity::class.java, CommonEnumUtils.ADDPLAN.toString())
        }
    }

    override fun getAllBundleDatas() {

    }


}