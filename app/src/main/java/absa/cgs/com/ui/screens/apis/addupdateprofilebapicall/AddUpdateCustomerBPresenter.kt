package absa.cgs.com.ui.screens.apis.addupdateprofilebapicall

import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.addexpenseapicall.model.AddExpenseRequestModel
import absa.cgs.com.ui.screens.apis.addexpenseapicall.model.AddExpenseResponseModel
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateARequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateAResponseModel
import absa.cgs.com.ui.screens.apis.addupdateprofilebapicall.model.CustomerAddUpdateBRequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofilebapicall.model.CustomerAddUpdateBResponseModel
import absa.cgs.com.ui.screens.authentication.AuthenticationBaseActivity
import absa.cgs.com.ui.screens.base.BasePresenter
import absa.cgs.com.utils.CommonUtils
import javax.inject.Inject

class AddUpdateCustomerBPresenter<View : AddUodateCustomerBView> @Inject constructor(var addUpdateCustomerBInteractor: AddUpdateCustomerBInteractor, private val commonUtils: CommonUtils) : BasePresenter<View>(), IAddUpdateCustomerBListener<View>, AddUpdateCustomerBInteractor.OnCallHitListener {


    override fun postCustomerBApiCall() {
        var customerAddUpdateBRequestModel = CustomerAddUpdateBRequestModel(getBaseMvpVieww().getUserId(), getBaseMvpVieww().getCustomerId(), getBaseMvpVieww().getCustomerBId(), getBaseMvpVieww().getcustomerBBillingName(), getBaseMvpVieww().getCustomerBEmail(), getBaseMvpVieww().getCustomerBAadharNo(), getBaseMvpVieww().getCustomerBEbNo(), getBaseMvpVieww().getCustomerBPanNo(), getBaseMvpVieww().getCustomerBSecurityDeposite())
        addUpdateCustomerBInteractor.postAddUpdateCustomerBDataToServer(customerAddUpdateBRequestModel, this)
    }

    override fun onSuccessAddUpdateCustomerBInteractListener(customerAddUpdateBResponseModel: CustomerAddUpdateBResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().onSuccessAddUpdateCustomerBResponse(customerAddUpdateBResponseModel)
        getBaseMvpVieww().showToastLong(commonUtils.cutNull(customerAddUpdateBResponseModel.message))
    }

    override fun onRetrofitFailureAddUpdateCustomerBInteractListener(error: String) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(error)
    }

    override fun onSessionExpireAddUpdateCustomerBInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        activity!!.finishAffinity()
        getBaseMvpVieww().navigationRoutes(AuthenticationBaseActivity::class.java)
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.SessionTokenExpire))
    }

    override fun onErrorAddUpdateCustomerBInteractListener(customerAddUpdateBResponseModel: CustomerAddUpdateBResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(customerAddUpdateBResponseModel.message)
    }

    override fun onServerExceptionAddUpdateCustomerBInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.ServerBusyString))
    }

    override fun validateAddUpdateCustomerB() {
        if (getBaseMvpVieww().getcustomerBBillingName().equals("")) {
            getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.CustomerNameAString))
        } else if (getBaseMvpVieww().getCustomerBEmail().equals("")) {
            getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.CustomerAreaAString))
        } else if (getBaseMvpVieww().getCustomerBAadharNo().equals("")) {
            getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.CustomerStreetAString))
        } else if (getBaseMvpVieww().getCustomerBEbNo().equals("")) {
            getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.CustomerHomeTypeAString))
        } else if (getBaseMvpVieww().getCustomerBPanNo().equals("")) {
            getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.CustomerPrimaryNoAString))
        } else if (getBaseMvpVieww().getCustomerBSecurityDeposite().equals("")) {
            getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.CustomerPrimaryNoAString))
        } else {
            getBaseMvpVieww().postAddUpdateCustomerB()
        }

    }
}