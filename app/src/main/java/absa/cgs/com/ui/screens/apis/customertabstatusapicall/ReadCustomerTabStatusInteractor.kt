package absa.cgs.com.ui.screens.apis.customertabstatusapicall

import absa.cgs.com.data.RetrofitClient
import absa.cgs.com.ui.screens.apis.customertabstatusapicall.model.CustomerTabStatusRequestModel
import absa.cgs.com.ui.screens.apis.customertabstatusapicall.model.CustomerTabStatusResponseModel
import absa.cgs.com.ui.screens.apis.readprofileapicall.model.ReadProfileRequestModel
import absa.cgs.com.ui.screens.apis.readprofileapicall.model.ReadProfileResponseModel
import absa.cgs.com.utils.enums.HttpEnum
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ReadCustomerTabStatusInteractor @Inject constructor() {

    private var customerTabStatusResponseModel: CustomerTabStatusResponseModel? = null


    interface OnCallHitListener {
        fun onSuccessCustomerTabStatusInteractListener(customerTabStatusResponseModel: CustomerTabStatusResponseModel)
        fun onRetrofitFailureCustomerTabStatusInteractListener(error: String)
        fun onSessionExpireCustomerTabStatusInteractListener()
        fun onErrorCustomerTabStatusInteractListener(customerTabStatusResponseModel: CustomerTabStatusResponseModel)
        fun onServerExceptionCustomerTabStatusInteractListener()
    }


    fun getCustomerTabStatusDataToServer(customerTabStatusRequestModel: CustomerTabStatusRequestModel, listener: OnCallHitListener) {
        RetrofitClient.instance.readCustomerTabStatusData(customerTabStatusRequestModel)
                .enqueue(object : Callback<CustomerTabStatusResponseModel> {
                    override fun onFailure(call: Call<CustomerTabStatusResponseModel>, t: Throwable) {
                        listener.onRetrofitFailureCustomerTabStatusInteractListener(t.message.toString())
                    }

                    override fun onResponse(call: Call<CustomerTabStatusResponseModel>, response: Response<CustomerTabStatusResponseModel>) {
                        when (response.code()) {
                            HttpEnum.STATUS_UNAUTHORIZED.code -> {
                                listener.onSessionExpireCustomerTabStatusInteractListener()
                            }

                            HttpEnum.STATUS_ERROR.code -> {
                                customerTabStatusResponseModel = response.body()
                                listener.onErrorCustomerTabStatusInteractListener(customerTabStatusResponseModel!!)
                            }

                            HttpEnum.STATUS_OK.code -> {
                                customerTabStatusResponseModel = response.body()
                                listener.onSuccessCustomerTabStatusInteractListener(customerTabStatusResponseModel!!)
                            }

                            else -> {
                                listener.onServerExceptionCustomerTabStatusInteractListener()
                            }
                        }

                    }

                })
    }

}
