package absa.cgs.com.ui.screens.customer

import absa.cgs.com.ui.screens.apis.customertabstatusapicall.model.CustomerTabStatusResponseModel
import absa.cgs.com.ui.screens.base.BaseMvpView
import absa.cgs.com.ui.screens.register.model.RadioButtonDataModel
import android.os.Bundle

interface CustomerView : BaseMvpView {
    fun setActionBarTitle(string: Int)
    fun changeFragment(position: Int, bundle: Bundle)
    fun isCustomerIdExist()
    fun changeCustomerTabStatusColor(customerTabStatusResponseModel: CustomerTabStatusResponseModel)
    fun clearAllSingletonCustomerData()

    fun moveToProfileA()
    fun moveToProfileB()
    fun moveToProfileC()
    fun moveToProfileD()
    fun moveToProfileE()

}