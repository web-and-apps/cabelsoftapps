package absa.cgs.com.ui.screens.apis.plan.updatenewchannelapicall

import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.addexpenseapicall.model.AddExpenseRequestModel
import absa.cgs.com.ui.screens.apis.addexpenseapicall.model.AddExpenseResponseModel
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateARequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateAResponseModel
import absa.cgs.com.ui.screens.apis.plan.addplanapicall.model.AddPlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.addplanapicall.model.AddPlanResponseModel
import absa.cgs.com.ui.screens.apis.plan.updatenewchannelapicall.model.UpdateNewChannelPlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.updateplanapicall.model.UpdatePlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.updateplanapicall.model.UpdatePlanResponseModel
import absa.cgs.com.ui.screens.authentication.AuthenticationBaseActivity
import absa.cgs.com.ui.screens.base.BasePresenter
import absa.cgs.com.utils.CommonUtils
import javax.inject.Inject

class UpdateNewChannelPlanPresenter<View : UpdateNewChannelPlanView> @Inject constructor(var updateNewChannelPlanInteractor: UpdateNewChannelPlanInteractor, private val commonUtils: CommonUtils) : BasePresenter<View>(), IUpdateNewChannelPlanListener<View>, UpdateNewChannelPlanInteractor.OnCallHitListener {


    override fun updatePlanApiCall() {
        var updateNewChannelPlanRequestModel = UpdateNewChannelPlanRequestModel(getBaseMvpVieww().getPlanId(), getBaseMvpVieww().getPlanArrayData())
        updateNewChannelPlanInteractor.updateNewChannelPlanDataToServer(updateNewChannelPlanRequestModel, this)
    }

    override fun onSuccessUpdatePlanInteractListener(updatePlanResponseModel: UpdatePlanResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().onSuccessUpdatePlanResponse(updatePlanResponseModel)

    }

    override fun onRetrofitFailureUpdatePlanInteractListener(error: String) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(error)
    }

    override fun onSessionExpireUpdatePlanInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        activity!!.finishAffinity()
        getBaseMvpVieww().navigationRoutes(AuthenticationBaseActivity::class.java)
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.SessionTokenExpire))
    }

    override fun onErrorUpdatePlanInteractListener(updatePlanResponseModel: UpdatePlanResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(updatePlanResponseModel.message)
    }

    override fun onServerExceptionUpdatePlanInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.ServerBusyString))
    }


}