package absa.cgs.com.ui.screens.apis.deletecustomerapicall

import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.addexpenseapicall.model.AddExpenseRequestModel
import absa.cgs.com.ui.screens.apis.addexpenseapicall.model.AddExpenseResponseModel
import absa.cgs.com.ui.screens.apis.deletecustomerapicall.model.DeleteCustomerRequestModel
import absa.cgs.com.ui.screens.apis.deletecustomerapicall.model.DeleteCustomerResponseModel
import absa.cgs.com.ui.screens.apis.deleteexpenseapicall.model.DeleteExpenseRequestModel
import absa.cgs.com.ui.screens.apis.deleteexpenseapicall.model.DeleteExpenseResponseModel
import absa.cgs.com.ui.screens.authentication.AuthenticationBaseActivity
import absa.cgs.com.ui.screens.base.BasePresenter
import absa.cgs.com.utils.CommonUtils
import javax.inject.Inject

class DeleteCustomerpresenter<View : DeleteCustomerView> @Inject constructor(var deleteCustomerInteractor: DeleteCustomerInteractor, private var commonUtils: CommonUtils) : BasePresenter<View>(), IDeleteCustomerListener<View>, DeleteCustomerInteractor.OnCallHitListener {


    override fun postDeleteCustomerApiCall() {
        var deleteCustomerRequestModel = DeleteCustomerRequestModel(getBaseMvpVieww().getCustomerID())
        deleteCustomerInteractor.postDeleteExpenseDataToServer(deleteCustomerRequestModel, this)
    }

    override fun onSuccessDeleteCustomerInteractListener(deleteCustomerResponseModel: DeleteCustomerResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().onSuccessDeleteCustomerResponse()
        getBaseMvpVieww().showToastLong(commonUtils.cutNull(deleteCustomerResponseModel.message))
        getBaseMvpVieww().hitCustomerDetailsCall()
    }

    override fun onRetrofitFailureDeleteCustomerInteractListener(error: String) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(error)
    }

    override fun onSessionExpireDeleteCustomerInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        activity!!.finishAffinity()
        getBaseMvpVieww().navigationRoutes(AuthenticationBaseActivity::class.java)
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.SessionTokenExpire))
    }

    override fun onErrorDeleteCustomerInteractListener(deleteCustomerResponseModel: DeleteCustomerResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(deleteCustomerResponseModel.message)
    }

    override fun onServerExceptionDeleteCustomerInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.ServerBusyString))
    }


}