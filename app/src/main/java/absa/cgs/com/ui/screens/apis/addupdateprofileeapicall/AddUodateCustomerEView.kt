package absa.cgs.com.ui.screens.apis.addupdateprofileeapicall

import absa.cgs.com.ui.screens.apis.addupdateprofileeapicall.model.CustomerAddUpdateEResponseModel
import absa.cgs.com.ui.screens.base.BaseMvpView

interface AddUodateCustomerEView : BaseMvpView {

    fun onSuccessAddUpdateCustomerEResponse(customerAddUpdateEResponseModel: CustomerAddUpdateEResponseModel)
    fun onFailureAddUpdateCustomerEResponse(error: String)

    fun getUserId(): String
    fun getCustomerId(): String
    fun getCustomerEId(): String
    fun getCustomerEregisterDate(): String
    fun getCustomerEConnectionDate(): String

    fun postAddUpdateCustomerE()

}