package absa.cgs.com.ui.screens.apis.getallcustomerapicall

import absa.cgs.com.ui.screens.base.BaseMvpPresenter


interface IGetCustomerListener<View : GetCustomerView> : BaseMvpPresenter<View> {

    fun getCustomerData()

}
