package absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall

import absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.model.ReadCustomerProfileAResponseModel
import absa.cgs.com.ui.screens.apis.readprofileapicall.model.ReadProfileResponseModel
import absa.cgs.com.ui.screens.base.BaseMvpView

interface ReadCustomerProfileAView : BaseMvpView {
    fun onSuccessReadCustomerProfileAResponse(readCustomerProfileAResponseModel: ReadCustomerProfileAResponseModel)
    fun onFailureReadCustomerProfileAResponse(error: String)

    fun getCustomerID(): String

    fun hitGetCustomerProfileACall()
}
