package absa.cgs.com.ui.screens.apis.readcustomerprofilebapicall


import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.model.ReadCustomerProfileARequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.model.ReadCustomerProfileAResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilebapicall.model.ReadCustomerProfileBRequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilebapicall.model.ReadCustomerProfileBResponseModel
import absa.cgs.com.ui.screens.apis.readexpenseapicall.model.ReadExpenseRequestModel
import absa.cgs.com.ui.screens.apis.readexpenseapicall.model.ReadExpenseResponseModel
import absa.cgs.com.ui.screens.authentication.AuthenticationBaseActivity
import absa.cgs.com.ui.screens.base.BasePresenter
import javax.inject.Inject

class ReadCustomerProfileBPresenter<View : ReadCustomerProfileBView> @Inject constructor(var readCustomerProfileBInteractor: ReadCustomerProfileBInteractor) : BasePresenter<View>(), IReadCustomerProfileBListener<View>, ReadCustomerProfileBInteractor.OnCallHitListener {




    override fun readCustomerProfileBData() {
        val readCustomerProfileBRequestModel = ReadCustomerProfileBRequestModel(
                getBaseMvpVieww().getCustomerID())
        readCustomerProfileBInteractor.getReadCustomerProfileBDataToServer(readCustomerProfileBRequestModel, this)
    }

    override fun onSuccessReadCustomerBProfileInteractListener(readCustomerProfileBResponseModel: ReadCustomerProfileBResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().onSuccessReadCustomerProfileBResponse(readCustomerProfileBResponseModel)
    }

    override fun onRetrofitFailureReadCustomerBProfileInteractListener(error: String) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(error)
    }

    override fun onSessionExpireReadCustomerBProfileInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        activity!!.finishAffinity()
        getBaseMvpVieww().navigationRoutes(AuthenticationBaseActivity::class.java)
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.SessionTokenExpire))
    }

    override fun onErrorReadCustomerBProfileInteractListener(readCustomerProfileBResponseModel: ReadCustomerProfileBResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        //getBaseMvpVieww().showToastLong(readCustomerProfileBResponseModel.message)
    }

    override fun onServerExceptionReadCustomerBProfileInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.ServerBusyString))
    }



}
