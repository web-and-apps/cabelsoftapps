package absa.cgs.com.ui.screens.apis.readcustomerprofileeapicall

import absa.cgs.com.ui.screens.base.BaseMvpPresenter


interface IReadCustomerProfileEListener<View : ReadCustomerProfileEView> : BaseMvpPresenter<View> {

    fun readCustomerProfileEData()

}
