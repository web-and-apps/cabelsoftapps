package absa.cgs.com.ui.screens.register

import absa.cgs.com.ui.screens.base.BaseMvpPresenter
import absa.cgs.com.ui.screens.customer.CustomerView

interface RegistrationPresenterListener<View : CustomerView> : BaseMvpPresenter<View> {

}