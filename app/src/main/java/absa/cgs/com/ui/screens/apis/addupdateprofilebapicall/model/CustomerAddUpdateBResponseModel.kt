package absa.cgs.com.ui.screens.apis.addupdateprofilebapicall.model

class CustomerAddUpdateBResponseModel(var status_code: Int, var message: String, var operation: String)