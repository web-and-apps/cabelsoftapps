package absa.cgs.com.ui.screens.plan.planchildfragment.plandetails.adapter

import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.plan.getparticularplanapicall.model.GetPlanResponseModel
import absa.cgs.com.ui.screens.mainbaseactivity.OnListItemClickInterface
import absa.cgs.com.utils.enums.CommonEnumUtils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView

import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView


class PlanDetailsAdapter(context: Context, private val getPlanResponseModel: GetPlanResponseModel, val onListItemClickInterface: OnListItemClickInterface) : RecyclerView.Adapter<PlanDetailsAdapter.DemoAdapter>() {


    inner class DemoAdapter(view: View) : RecyclerView.ViewHolder(view) {
        var planDetailsNameTv: TextView
        var planDetailsAmountTv: TextView
        var planDetailsCrd: CardView
        var planDetailsDeleteImg: ImageView


        init {
            planDetailsNameTv = view.findViewById(R.id.planDetailsNameTv) as TextView
            planDetailsAmountTv = view.findViewById(R.id.planDetailsAmountTv) as TextView
            planDetailsCrd = view.findViewById(R.id.planDetailsCrd) as CardView
            planDetailsDeleteImg = view.findViewById(R.id.planDetailsDeleteImg) as ImageView
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DemoAdapter {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.custom_plan_details_list, parent, false)
        return DemoAdapter(itemView)
    }

    override fun onBindViewHolder(holder: DemoAdapter, position: Int) {
        holder.planDetailsNameTv.text = (getPlanResponseModel.product_details.get(position).plan_name)

        when (getPlanResponseModel.product_details.get(position).plan_type) {
            CommonEnumUtils.FREE.toString() -> {
                holder.planDetailsAmountTv.text = ("Code: " + getPlanResponseModel.product_details.get(position).plan_code)
            }
            else -> {
                holder.planDetailsAmountTv.text = ("Rs. " + getPlanResponseModel.product_details.get(position).plan_cost)
            }
        }


        holder.planDetailsCrd.setOnClickListener {
            onListItemClickInterface.OnSelectedItemClickListener(CommonEnumUtils.VIEW.toString(), position)
        }

        holder.planDetailsDeleteImg.setOnClickListener {
            onListItemClickInterface.OnSelectedItemClickListener(CommonEnumUtils.DELETE.toString(), position)
        }

        holder.planDetailsDeleteImg.setOnClickListener {
            onListItemClickInterface.OnSelectedItemClickListener(CommonEnumUtils.DELETE.toString(), position)
        }


    }


    override fun getItemCount(): Int {
        return getPlanResponseModel.product_details.size
    }
}
