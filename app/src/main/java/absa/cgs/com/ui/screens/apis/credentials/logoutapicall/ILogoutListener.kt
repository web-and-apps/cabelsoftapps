package absa.cgs.com.ui.screens.apis.credentials.logoutapicall

import absa.cgs.com.ui.screens.base.BaseMvpPresenter

interface ILogoutListener<View : LogoutView> : BaseMvpPresenter<View> {
    fun logoutApiCall()
}