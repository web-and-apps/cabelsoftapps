package absa.cgs.com.ui.screens.apis.addupdateprofilecapicall

import aCsa.cgs.com.ui.screens.apis.addupdateprofilecapicall.AddUpdateCustomerCInteractor
import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateARequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofilecapicall.model.CustomerAddUpdateCRequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofilecapicall.model.CustomerAddUpdateCResponseModel
import absa.cgs.com.ui.screens.authentication.AuthenticationBaseActivity
import absa.cgs.com.ui.screens.base.BasePresenter
import absa.cgs.com.utils.CommonUtils
import javax.inject.Inject

class AddUpdateCustomerCPresenter<View : AddUodateCustomerCView> @Inject constructor(var addUpdateCustomerCInteractor: AddUpdateCustomerCInteractor, private val commonUtils: CommonUtils) : BasePresenter<View>(), IAddUpdateCustomerCListener<View>, AddUpdateCustomerCInteractor.OnCallHitListener {


    override fun postCustomerCApiCall() {
        var customerAddUpdateCRequestModel = CustomerAddUpdateCRequestModel(getBaseMvpVieww().getUserId(), getBaseMvpVieww().getCustomerId(), getBaseMvpVieww().getCustomerCId(), getBaseMvpVieww().getCustomerCBillType(), getBaseMvpVieww().getCustomeCBillTime(), getBaseMvpVieww().getCustomerCAdditionalCharge())
        addUpdateCustomerCInteractor.postAddUpdateCustomerCDataToServer(customerAddUpdateCRequestModel, this)
    }

    override fun onSuccessAddUpdateCustomerCInteractListener(customerAddUpdateCResponseModel: CustomerAddUpdateCResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().onSuccessAddUpdateCustomerCResponse(customerAddUpdateCResponseModel)
        getBaseMvpVieww().showToastLong(commonUtils.cutNull(customerAddUpdateCResponseModel.message))
    }

    override fun onRetrofitFailureAddUpdateCustomerCInteractListener(error: String) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(error)
    }

    override fun onSessionExpireAddUpdateCustomerCInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        activity!!.finishAffinity()
        getBaseMvpVieww().navigationRoutes(AuthenticationBaseActivity::class.java)
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.SessionTokenExpire))
    }

    override fun onErrorAddUpdateCustomerCInteractListener(customerAddUpdateCResponseModel: CustomerAddUpdateCResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(customerAddUpdateCResponseModel.message)
    }

    override fun onServerExceptionAddUpdateCustomerCInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.ServerBusyString))
    }

    override fun validateAddUpdateCustomerC() {
        if (getBaseMvpVieww().getCustomerCBillType().equals("")) {
            getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.CustomerNameAString))
        } else if (getBaseMvpVieww().getCustomeCBillTime().equals("")) {
            getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.CustomerAreaAString))
        } else if (getBaseMvpVieww().getCustomerCAdditionalCharge().equals("")) {
            getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.CustomerStreetAString))
        } else {
            getBaseMvpVieww().postAddUpdateCustomerC()
        }
    }
}