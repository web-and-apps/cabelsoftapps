package absa.cgs.com.ui.screens.apis.readcustomerprofilebapicall

import absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.model.ReadCustomerProfileAResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilebapicall.model.ReadCustomerProfileBResponseModel
import absa.cgs.com.ui.screens.apis.readprofileapicall.model.ReadProfileResponseModel
import absa.cgs.com.ui.screens.base.BaseMvpView

interface ReadCustomerProfileBView : BaseMvpView {
    fun onSuccessReadCustomerProfileBResponse(readCustomerProfileBResponseModel: ReadCustomerProfileBResponseModel)
    fun onFailureReadCustomerProfileBResponse(error: String)

    fun getCustomerID(): String

    fun hitGetCustomerProfileBCall()
}
