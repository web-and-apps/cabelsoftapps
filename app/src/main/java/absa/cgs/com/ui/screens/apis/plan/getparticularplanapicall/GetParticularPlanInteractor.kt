package absa.cgs.com.ui.screens.apis.plan.getparticularplanapicall

import absa.cgs.com.data.RetrofitClient
import absa.cgs.com.ui.screens.apis.addexpenseapicall.model.AddExpenseRequestModel
import absa.cgs.com.ui.screens.apis.addexpenseapicall.model.AddExpenseResponseModel
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateARequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateAResponseModel
import absa.cgs.com.ui.screens.apis.plan.addplanapicall.model.AddPlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.addplanapicall.model.AddPlanResponseModel
import absa.cgs.com.ui.screens.apis.plan.deleteplanapicall.model.DeletePlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.deleteplanapicall.model.DeletePlanResponseModel
import absa.cgs.com.ui.screens.apis.plan.getparticularplanapicall.model.GetParticularPlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.getparticularplanapicall.model.GetPlanResponseModel
import absa.cgs.com.ui.screens.apis.plan.updateplanapicall.model.UpdatePlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.updateplanapicall.model.UpdatePlanResponseModel
import absa.cgs.com.utils.enums.HttpEnum
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetParticularPlanInteractor @Inject constructor() {
    private var getPlanResponseModel: GetPlanResponseModel? = null


    interface OnCallHitListener {
        fun onSuccessGetParticularPlanInteractListener(getPlanResponseModel: GetPlanResponseModel)
        fun onRetrofitFailureGetParticularPlanInteractListener(error: String)
        fun onSessionExpireGetParticularPlanInteractListener()
        fun onErrorGetParticularPlanInteractListener(getPlanResponseModel: GetPlanResponseModel)
        fun onServerExceptionGetParticularPlanInteractListener()
    }

    fun getParticularPlanDataToServer(getParticularPlanRequestModel: GetParticularPlanRequestModel, listener: OnCallHitListener) {
        RetrofitClient.instance.getParticularPlanDetails(getParticularPlanRequestModel)
                .enqueue(object : Callback<GetPlanResponseModel> {
                    override fun onFailure(call: Call<GetPlanResponseModel>, t: Throwable) {
                        listener.onRetrofitFailureGetParticularPlanInteractListener(t.message.toString())
                    }

                    override fun onResponse(call: Call<GetPlanResponseModel>, response: Response<GetPlanResponseModel>) {

                        when (response.code()) {
                            HttpEnum.STATUS_UNAUTHORIZED.code -> {
                                listener.onSessionExpireGetParticularPlanInteractListener()
                            }

                            HttpEnum.STATUS_ERROR.code -> {
                                getPlanResponseModel = response.body()
                                listener.onErrorGetParticularPlanInteractListener(getPlanResponseModel!!)
                            }

                            HttpEnum.STATUS_OK.code -> {
                                getPlanResponseModel = response.body()
                                listener.onSuccessGetParticularPlanInteractListener(getPlanResponseModel!!)
                            }

                            else -> {
                                listener.onServerExceptionGetParticularPlanInteractListener()
                            }
                        }

                    }

                })
    }
}