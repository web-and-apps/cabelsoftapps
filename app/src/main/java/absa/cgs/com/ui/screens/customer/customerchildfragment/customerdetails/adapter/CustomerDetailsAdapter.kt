package absa.cgs.com.ui.screens.customer.customerchildfragment.customerdetails.adapter

import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.getallcustomerapicall.model.GetAllCustomerResponseModel
import absa.cgs.com.ui.screens.apis.readexpenseapicall.model.ReadExpenseResponseModel
import absa.cgs.com.ui.screens.mainbaseactivity.OnListItemClickInterface
import absa.cgs.com.utils.enums.CommonEnumUtils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView

import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView


class CustomerDetailsAdapter(context: Context, private val getAllCustomerResponseModel: GetAllCustomerResponseModel, val onListItemClickInterface: OnListItemClickInterface) : RecyclerView.Adapter<CustomerDetailsAdapter.DemoAdapter>() {


    inner class DemoAdapter(view: View) : RecyclerView.ViewHolder(view) {
        var customCustomerDetailsUserTv: TextView
        var customCustomerDetailsPhoneTv: TextView
        var customCustomerDetailsCrd: CardView
        var customCustomerDetailsDeleteImg: ImageView


        init {
            customCustomerDetailsUserTv = view.findViewById(R.id.customCustomerDetailsUserTv) as TextView
            customCustomerDetailsPhoneTv = view.findViewById(R.id.customCustomerDetailsPhoneTv) as TextView
            customCustomerDetailsCrd = view.findViewById(R.id.customCustomerDetailsCrd) as CardView
            customCustomerDetailsDeleteImg = view.findViewById(R.id.customCustomerDetailsDeleteImg) as ImageView
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DemoAdapter {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.custom_customer_details_list, parent, false)
        return DemoAdapter(itemView)
    }

    override fun onBindViewHolder(holder: DemoAdapter, position: Int) {
        holder.customCustomerDetailsUserTv.text = (getAllCustomerResponseModel.customer_details.get(position).customer_name)
        holder.customCustomerDetailsPhoneTv.text = (getAllCustomerResponseModel.customer_details.get(position).customer_mobile)

        holder.customCustomerDetailsCrd.setOnClickListener {
            onListItemClickInterface.OnSelectedItemClickListener(CommonEnumUtils.VIEW.toString(), position)
        }

        holder.customCustomerDetailsDeleteImg.setOnClickListener {
            onListItemClickInterface.OnSelectedItemClickListener(CommonEnumUtils.DELETE.toString(), position)
        }


    }


    override fun getItemCount(): Int {
        return getAllCustomerResponseModel.customer_details.size
    }
}
