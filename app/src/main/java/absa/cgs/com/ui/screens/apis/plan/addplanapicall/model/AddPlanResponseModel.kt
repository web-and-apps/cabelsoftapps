package absa.cgs.com.ui.screens.apis.plan.addplanapicall.model

data class AddPlanResponseModel(var status_code: Int, var message: String)