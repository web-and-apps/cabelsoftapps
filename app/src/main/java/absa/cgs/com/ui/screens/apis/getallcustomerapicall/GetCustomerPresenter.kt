package absa.cgs.com.ui.screens.apis.getallcustomerapicall


import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.getallcustomerapicall.model.GetAllCustomerRequestModel
import absa.cgs.com.ui.screens.apis.getallcustomerapicall.model.GetAllCustomerResponseModel
import absa.cgs.com.ui.screens.apis.readexpenseapicall.model.ReadExpenseRequestModel
import absa.cgs.com.ui.screens.apis.readexpenseapicall.model.ReadExpenseResponseModel
import absa.cgs.com.ui.screens.authentication.AuthenticationBaseActivity
import absa.cgs.com.ui.screens.base.BasePresenter
import javax.inject.Inject

class GetCustomerPresenter<View : GetCustomerView> @Inject constructor(var getCustomerInteractor: GetCustomerInteractor) : BasePresenter<View>(), IGetCustomerListener<View>, GetCustomerInteractor.OnCallHitListener {
    override fun getCustomerData() {
        val getAllCustomerRequestModel = GetAllCustomerRequestModel(
                getBaseMvpVieww().getUserID(),
                getBaseMvpVieww().getFromDate(),
                getBaseMvpVieww().getToDate(),
                getBaseMvpVieww().getPageCount(),
                getBaseMvpVieww().getSearchKeyword())
        getCustomerInteractor.getCustomerDataToServer(getAllCustomerRequestModel, this)
    }

    override fun onSuccessGetCustomerInteractListener(getAllCustomerResponseModel: GetAllCustomerResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().onSuccessGetCustomerResponse(getAllCustomerResponseModel)
    }

    override fun onRetrofitFailureGetCustomerInteractListener(error: String) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(error)
    }

    override fun onSessionExpireGetCustomerInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        activity!!.finishAffinity()
        getBaseMvpVieww().navigationRoutes(AuthenticationBaseActivity::class.java)
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.SessionTokenExpire))
    }

    override fun onErrorGetCustomerInteractListener(getAllCustomerResponseModel: GetAllCustomerResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(getAllCustomerResponseModel.message)
    }

    override fun onServerExceptionGetCustomerInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.ServerBusyString))
    }


}
