package absa.cgs.com.ui.screens.apis.getallcustomerapicall.model

data class CustomerListDetails(var user_id: String, var customer_id: String, var customer_name: String, var customer_mobile: String, var customer_date: String)