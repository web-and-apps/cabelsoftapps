package absa.cgs.com.ui.screens.apis.addupdateprofilebapicall

import absa.cgs.com.data.RetrofitClient
import absa.cgs.com.ui.screens.apis.addexpenseapicall.model.AddExpenseRequestModel
import absa.cgs.com.ui.screens.apis.addexpenseapicall.model.AddExpenseResponseModel
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateARequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateAResponseModel
import absa.cgs.com.ui.screens.apis.addupdateprofilebapicall.model.CustomerAddUpdateBRequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofilebapicall.model.CustomerAddUpdateBResponseModel
import absa.cgs.com.utils.enums.HttpEnum
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AddUpdateCustomerBInteractor @Inject constructor() {
    private var customerAddUpdateBResponseModel: CustomerAddUpdateBResponseModel? = null


    interface OnCallHitListener {
        fun onSuccessAddUpdateCustomerBInteractListener(customerAddUpdateBResponseModel: CustomerAddUpdateBResponseModel)
        fun onRetrofitFailureAddUpdateCustomerBInteractListener(error: String)
        fun onSessionExpireAddUpdateCustomerBInteractListener()
        fun onErrorAddUpdateCustomerBInteractListener(customerAddUpdateBResponseModel: CustomerAddUpdateBResponseModel)
        fun onServerExceptionAddUpdateCustomerBInteractListener()
    }

    fun postAddUpdateCustomerBDataToServer(customerAddUpdateBRequestModel: CustomerAddUpdateBRequestModel, listener: OnCallHitListener) {
        RetrofitClient.instance.addUpdateCustomerB(customerAddUpdateBRequestModel)
                .enqueue(object : Callback<CustomerAddUpdateBResponseModel> {
                    override fun onFailure(call: Call<CustomerAddUpdateBResponseModel>, t: Throwable) {
                        listener.onRetrofitFailureAddUpdateCustomerBInteractListener(t.message.toString())
                    }

                    override fun onResponse(call: Call<CustomerAddUpdateBResponseModel>, response: Response<CustomerAddUpdateBResponseModel>) {

                        when (response.code()) {
                            HttpEnum.STATUS_UNAUTHORIZED.code -> {
                                listener.onSessionExpireAddUpdateCustomerBInteractListener()
                            }

                            HttpEnum.STATUS_ERROR.code -> {
                                customerAddUpdateBResponseModel = response.body()
                                listener.onErrorAddUpdateCustomerBInteractListener(customerAddUpdateBResponseModel!!)
                            }

                            HttpEnum.STATUS_OK.code -> {
                                customerAddUpdateBResponseModel = response.body()
                                listener.onSuccessAddUpdateCustomerBInteractListener(customerAddUpdateBResponseModel!!)
                            }

                            else -> {
                                listener.onServerExceptionAddUpdateCustomerBInteractListener()
                            }
                        }

                    }

                })
    }
}