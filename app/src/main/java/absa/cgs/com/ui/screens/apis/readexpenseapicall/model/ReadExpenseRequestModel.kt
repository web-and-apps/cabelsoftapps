package absa.cgs.com.ui.screens.apis.readexpenseapicall.model

data class ReadExpenseRequestModel(var user_id: String, var search_keyword: String, var page_count: String, var from_date: String, var to_date: String)