package absa.cgs.com.ui.screens.apis.readcustomerprofiledapicall


import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.model.ReadCustomerProfileARequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.model.ReadCustomerProfileAResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilebapicall.model.ReadCustomerProfileBRequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilebapicall.model.ReadCustomerProfileBResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilecapicall.model.ReadCustomerProfileCRequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilecapicall.model.ReadCustomerProfileCResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofiledapicall.model.ReadCustomerProfileDRequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofiledapicall.model.ReadCustomerProfileDResponseModel
import absa.cgs.com.ui.screens.apis.readexpenseapicall.model.ReadExpenseRequestModel
import absa.cgs.com.ui.screens.apis.readexpenseapicall.model.ReadExpenseResponseModel
import absa.cgs.com.ui.screens.authentication.AuthenticationBaseActivity
import absa.cgs.com.ui.screens.base.BasePresenter
import javax.inject.Inject

class ReadCustomerProfileDPresenter<View : ReadCustomerProfileDView> @Inject constructor(var readCustomerProfileDInteractor: ReadCustomerProfileDInteractor) : BasePresenter<View>(), IReadCustomerProfileDListener<View>, ReadCustomerProfileDInteractor.OnCallHitListener {
    override fun readCustomerProfileDData() {
        val readCustomerProfileDRequestModel = ReadCustomerProfileDRequestModel(
                getBaseMvpVieww().getCustomerID())
        readCustomerProfileDInteractor.getReadCustomerProfileDDataToServer(readCustomerProfileDRequestModel, this)
    }

    override fun onSuccessReadCustomerDProfileInteractListener(readCustomerProfileDResponseModel: ReadCustomerProfileDResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().onSuccessReadCustomerProfileDResponse(readCustomerProfileDResponseModel)
    }

    override fun onRetrofitFailureReadCustomerDProfileInteractListener(error: String) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(error)
    }

    override fun onSessionExpireReadCustomerDProfileInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        activity!!.finishAffinity()
        getBaseMvpVieww().navigationRoutes(AuthenticationBaseActivity::class.java)
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.SessionTokenExpire))
    }

    override fun onErrorReadCustomerDProfileInteractListener(readCustomerProfileDResponseModel: ReadCustomerProfileDResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        //getBaseMvpVieww().showToastLong(readCustomerProfileDResponseModel.message)
    }

    override fun onServerExceptionReadCustomerDProfileInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.ServerBusyString))
    }


}
