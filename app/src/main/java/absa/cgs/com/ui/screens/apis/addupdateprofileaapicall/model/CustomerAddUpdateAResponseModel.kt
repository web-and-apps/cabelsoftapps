package absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model

class CustomerAddUpdateAResponseModel(var status_code: Int, var message: String, var customer_id: String, var operation: String)