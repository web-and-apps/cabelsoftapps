package absa.cgs.com.ui.screens.apis.readcustomerprofilecapicall

import absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.model.ReadCustomerProfileAResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilebapicall.model.ReadCustomerProfileBResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilecapicall.model.ReadCustomerProfileCResponseModel
import absa.cgs.com.ui.screens.apis.readprofileapicall.model.ReadProfileResponseModel
import absa.cgs.com.ui.screens.base.BaseMvpView

interface ReadCustomerProfileCView : BaseMvpView {
    fun onSuccessReadCustomerProfileCResponse(readCustomerProfileCResponseModel: ReadCustomerProfileCResponseModel)
    fun onFailureReadCustomerProfileCResponse(error: String)

    fun getCustomerID(): String

    fun hitGetCustomerProfileCCall()
}
