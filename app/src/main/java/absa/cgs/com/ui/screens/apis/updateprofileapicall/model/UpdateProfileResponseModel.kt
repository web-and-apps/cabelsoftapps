package absa.cgs.com.ui.screens.apis.updateprofileapicall.model

data class UpdateProfileResponseModel(var status_code: Int, var message: String)