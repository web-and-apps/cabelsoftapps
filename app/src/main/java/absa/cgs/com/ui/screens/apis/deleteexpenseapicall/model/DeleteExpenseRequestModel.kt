package absa.cgs.com.ui.screens.apis.deleteexpenseapicall.model

data class DeleteExpenseRequestModel(var expense_id: String)