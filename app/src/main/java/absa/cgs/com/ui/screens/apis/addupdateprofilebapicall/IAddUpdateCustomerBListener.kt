package absa.cgs.com.ui.screens.apis.addupdateprofilebapicall

import absa.cgs.com.ui.screens.base.BaseMvpPresenter

interface IAddUpdateCustomerBListener<View : AddUodateCustomerBView> : BaseMvpPresenter<View> {
    fun postCustomerBApiCall()
    fun validateAddUpdateCustomerB()
}