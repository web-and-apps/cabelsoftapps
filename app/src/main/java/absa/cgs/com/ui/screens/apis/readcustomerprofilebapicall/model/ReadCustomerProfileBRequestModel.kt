package absa.cgs.com.ui.screens.apis.readcustomerprofilebapicall.model

data class ReadCustomerProfileBRequestModel(var customer_id: String)