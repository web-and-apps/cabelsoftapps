package absa.cgs.com.ui.screens.apis.deletecustomerapicall.model

data class DeleteCustomerRequestModel(var customer_id: String)