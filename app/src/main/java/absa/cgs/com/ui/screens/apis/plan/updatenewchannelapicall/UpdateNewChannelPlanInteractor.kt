package absa.cgs.com.ui.screens.apis.plan.updatenewchannelapicall

import absa.cgs.com.data.RetrofitClient
import absa.cgs.com.ui.screens.apis.addexpenseapicall.model.AddExpenseRequestModel
import absa.cgs.com.ui.screens.apis.addexpenseapicall.model.AddExpenseResponseModel
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateARequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateAResponseModel
import absa.cgs.com.ui.screens.apis.plan.addplanapicall.model.AddPlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.addplanapicall.model.AddPlanResponseModel
import absa.cgs.com.ui.screens.apis.plan.deleteplanapicall.model.DeletePlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.deleteplanapicall.model.DeletePlanResponseModel
import absa.cgs.com.ui.screens.apis.plan.updatenewchannelapicall.model.UpdateNewChannelPlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.updateplanapicall.model.UpdatePlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.updateplanapicall.model.UpdatePlanResponseModel
import absa.cgs.com.utils.enums.HttpEnum
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UpdateNewChannelPlanInteractor @Inject constructor() {
    private var updatePlanResponseModel: UpdatePlanResponseModel? = null


    interface OnCallHitListener {
        fun onSuccessUpdatePlanInteractListener(deletePlanResponseModel: UpdatePlanResponseModel)
        fun onRetrofitFailureUpdatePlanInteractListener(error: String)
        fun onSessionExpireUpdatePlanInteractListener()
        fun onErrorUpdatePlanInteractListener(deletePlanResponseModel: UpdatePlanResponseModel)
        fun onServerExceptionUpdatePlanInteractListener()
    }

    fun updateNewChannelPlanDataToServer(updateNewChannelPlanRequestModel: UpdateNewChannelPlanRequestModel, listener: OnCallHitListener) {
        RetrofitClient.instance.updateNewChannelsPlan(updateNewChannelPlanRequestModel)
                .enqueue(object : Callback<UpdatePlanResponseModel> {
                    override fun onFailure(call: Call<UpdatePlanResponseModel>, t: Throwable) {
                        listener.onRetrofitFailureUpdatePlanInteractListener(t.message.toString())
                    }

                    override fun onResponse(call: Call<UpdatePlanResponseModel>, response: Response<UpdatePlanResponseModel>) {

                        when (response.code()) {
                            HttpEnum.STATUS_UNAUTHORIZED.code -> {
                                listener.onSessionExpireUpdatePlanInteractListener()
                            }

                            HttpEnum.STATUS_ERROR.code -> {
                                updatePlanResponseModel = response.body()
                                listener.onErrorUpdatePlanInteractListener(updatePlanResponseModel!!)
                            }

                            HttpEnum.STATUS_OK.code -> {
                                updatePlanResponseModel = response.body()
                                listener.onSuccessUpdatePlanInteractListener(updatePlanResponseModel!!)
                            }

                            else -> {
                                listener.onServerExceptionUpdatePlanInteractListener()
                            }
                        }

                    }

                })
    }
}