package absa.cgs.com.ui.screens.apis.addupdateprofileaapicall

import absa.cgs.com.data.RetrofitClient
import absa.cgs.com.ui.screens.apis.addexpenseapicall.model.AddExpenseRequestModel
import absa.cgs.com.ui.screens.apis.addexpenseapicall.model.AddExpenseResponseModel
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateARequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateAResponseModel
import absa.cgs.com.utils.enums.HttpEnum
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AddUpdateCustomerAInteractor @Inject constructor() {
    private var customerAddUpdateAResponseModel: CustomerAddUpdateAResponseModel? = null


    interface OnCallHitListener {
        fun onSuccessAddUpdateCustomerAInteractListener(customerAddUpdateAResponseModel: CustomerAddUpdateAResponseModel)
        fun onRetrofitFailureAddUpdateCustomerAInteractListener(error: String)
        fun onSessionExpireAddUpdateCustomerAInteractListener()
        fun onErrorAddUpdateCustomerAInteractListener(customerAddUpdateAResponseModel: CustomerAddUpdateAResponseModel)
        fun onServerExceptionAddUpdateCustomerAInteractListener()
    }

    fun postAddUpdateCustomerADataToServer(customerAddUpdateARequestModel: CustomerAddUpdateARequestModel, listener: OnCallHitListener) {
        RetrofitClient.instance.addUpdateCustomerA(customerAddUpdateARequestModel)
                .enqueue(object : Callback<CustomerAddUpdateAResponseModel> {
                    override fun onFailure(call: Call<CustomerAddUpdateAResponseModel>, t: Throwable) {
                        listener.onRetrofitFailureAddUpdateCustomerAInteractListener(t.message.toString())
                    }

                    override fun onResponse(call: Call<CustomerAddUpdateAResponseModel>, response: Response<CustomerAddUpdateAResponseModel>) {

                        when (response.code()) {
                            HttpEnum.STATUS_UNAUTHORIZED.code -> {
                                listener.onSessionExpireAddUpdateCustomerAInteractListener()
                            }

                            HttpEnum.STATUS_ERROR.code -> {
                                customerAddUpdateAResponseModel = response.body()
                                listener.onErrorAddUpdateCustomerAInteractListener(customerAddUpdateAResponseModel!!)
                            }

                            HttpEnum.STATUS_OK.code -> {
                                customerAddUpdateAResponseModel = response.body()
                                listener.onSuccessAddUpdateCustomerAInteractListener(customerAddUpdateAResponseModel!!)
                            }

                            else -> {
                                listener.onServerExceptionAddUpdateCustomerAInteractListener()
                            }
                        }

                    }

                })
    }
}