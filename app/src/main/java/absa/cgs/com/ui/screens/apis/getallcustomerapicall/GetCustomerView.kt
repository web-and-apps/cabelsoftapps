package absa.cgs.com.ui.screens.apis.getallcustomerapicall

import absa.cgs.com.ui.screens.apis.getallcustomerapicall.model.GetAllCustomerResponseModel
import absa.cgs.com.ui.screens.apis.readexpenseapicall.model.ReadExpenseResponseModel
import absa.cgs.com.ui.screens.base.BaseMvpView

interface GetCustomerView : BaseMvpView {
    fun onSuccessGetCustomerResponse(getAllCustomerResponseModel: GetAllCustomerResponseModel)
    fun onFailureGetCustomerResponse(error: String)

    fun getUserID(): String
    fun getSearchKeyword(): String
    fun getPageCount(): String
    fun getFromDate(): String
    fun getToDate(): String

    fun hitCustomerDetailsCall()
    fun customerDetailsOnClickListener(eventString: String, position: Int, getAllCustomerResponseModel: GetAllCustomerResponseModel)


}
