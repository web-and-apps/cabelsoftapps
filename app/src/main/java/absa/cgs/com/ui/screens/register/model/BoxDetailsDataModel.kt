package absa.cgs.com.ui.screens.register.model

data class BoxDetailsDataModel(var boxName: String?, var boxNo: String?, var boxType: String?, var securityDeposite: String?)