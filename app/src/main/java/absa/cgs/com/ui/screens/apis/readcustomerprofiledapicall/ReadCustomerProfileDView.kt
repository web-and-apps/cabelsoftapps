package absa.cgs.com.ui.screens.apis.readcustomerprofiledapicall

import absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.model.ReadCustomerProfileAResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilebapicall.model.ReadCustomerProfileBResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilecapicall.model.ReadCustomerProfileCResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofiledapicall.model.ReadCustomerProfileDResponseModel
import absa.cgs.com.ui.screens.apis.readprofileapicall.model.ReadProfileResponseModel
import absa.cgs.com.ui.screens.base.BaseMvpView

interface ReadCustomerProfileDView : BaseMvpView {
    fun onSuccessReadCustomerProfileDResponse(readCustomerProfileDResponseModel: ReadCustomerProfileDResponseModel)
    fun onFailureReadCustomerProfileDResponse(error: String)

    fun getCustomerID(): String

    fun hitGetCustomerProfileDCall()
}
