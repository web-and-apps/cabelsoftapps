package absa.cgs.com.ui.screens.apis.addupdateprofiledapicall.model

data class CustomerAddUpdateDRequestModel(var user_id: String, var customer_id: String,
                                          var customer_d_id: String,
                                          var customer_d_settop_no: String, var customer_d_settop_name: String,
                                          var customer_d_settop_type: String, var customer_d_smart_card_no: String)