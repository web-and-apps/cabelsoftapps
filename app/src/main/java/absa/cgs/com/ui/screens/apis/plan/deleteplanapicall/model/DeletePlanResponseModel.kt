package absa.cgs.com.ui.screens.apis.plan.deleteplanapicall.model

data class DeletePlanResponseModel(var status_code: Int, var message: String)