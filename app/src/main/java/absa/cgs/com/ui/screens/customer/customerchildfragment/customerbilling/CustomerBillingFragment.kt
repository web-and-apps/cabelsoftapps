package absa.cgs.com.ui.screens.customer.customerchildfragment.customerbilling

import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.AddUodateCustomerAView
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.AddUpdateCustomerAPresenter
import absa.cgs.com.ui.screens.apis.addupdateprofilecapicall.AddUodateCustomerCView
import absa.cgs.com.ui.screens.apis.addupdateprofilecapicall.AddUpdateCustomerCPresenter
import absa.cgs.com.ui.screens.apis.addupdateprofilecapicall.model.CustomerAddUpdateCResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilecapicall.ReadCustomerProfileCPresenter
import absa.cgs.com.ui.screens.apis.readcustomerprofilecapicall.ReadCustomerProfileCView
import absa.cgs.com.ui.screens.apis.readcustomerprofilecapicall.model.ReadCustomerProfileCResponseModel
import absa.cgs.com.ui.screens.base.BaseFragment
import absa.cgs.com.ui.screens.customer.CustomerBaseActivity
import absa.cgs.com.utils.SingletonUtils
import absa.cgs.com.utils.enums.CommonEnumUtils
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.customerbillingfragment.*
import kotlinx.android.synthetic.main.customerprofilebfragment.*
import javax.inject.Inject

class CustomerBillingFragment : BaseFragment(), CustomerBaseActivity.OnBackPressedListner, ReadCustomerProfileCView, AddUodateCustomerCView {


    lateinit var customerBaseActivity: CustomerBaseActivity

    @Inject
    lateinit var readCustomerProfileCPresenter: ReadCustomerProfileCPresenter<ReadCustomerProfileCView>

    @Inject
    lateinit var addUpdateCustomerCPresenter: AddUpdateCustomerCPresenter<AddUodateCustomerCView>

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        fragmentComponent().inject(this)
        readCustomerProfileCPresenter.attachView(activity!!, this)
        addUpdateCustomerCPresenter.attachView(activity!!, this)

    }


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.customerbillingfragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()

    }

    override fun init() {
        customerBaseActivity = activity as CustomerBaseActivity
        onClickListeners()
    }

    override fun onBackPressed(): Boolean {
        activity!!.finish()
        return false
    }

    override fun onSuccessReadCustomerProfileCResponse(readCustomerProfileCResponseModel: ReadCustomerProfileCResponseModel) {
        SingletonUtils.instance.customerCId = commonUtils.cutNull(readCustomerProfileCResponseModel.profile_c_details.customer_c_id)
        texteditRegisterBillType.setText(commonUtils.cutNull(readCustomerProfileCResponseModel.profile_c_details.customer_c_bill_type))
        texteditRegisterBillTime.setText(commonUtils.cutNull(readCustomerProfileCResponseModel.profile_c_details.customer_c_bill_time))
        texteditRegisterAdditionalCharge.setText(commonUtils.cutNull(readCustomerProfileCResponseModel.profile_c_details.customer_c_additional_charge))
    }

    override fun onFailureReadCustomerProfileCResponse(error: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onClickListeners() {
        super.onClickListeners()
        customerProfileCUpdateBtn.setOnClickListener {
            addUpdateCustomerCPresenter.validateAddUpdateCustomerC()
        }
    }

    override fun getCustomerID(): String {
        return SingletonUtils.instance.customerId
    }

    override fun hitGetCustomerProfileCCall() {
        progressLoadingBar()
        readCustomerProfileCPresenter.readCustomerProfileCData()
    }

    override fun onResume() {
        super.onResume()
        if (!commonUtils.cutNull(SingletonUtils.instance.customerId).isNullOrEmpty()) {
            hitGetCustomerProfileCCall()
        }
    }

    override fun onSuccessAddUpdateCustomerCResponse(customerAddUpdateCResponseModel: CustomerAddUpdateCResponseModel) {
        when (commonUtils.cutNull(customerAddUpdateCResponseModel.operation)) {
            CommonEnumUtils.ADDED.toString() -> {
                customerBaseActivity.moveToProfileD()
                customerBaseActivity.hitGetCustomerTabStatusCall()
            }
            CommonEnumUtils.UPDATED.toString() -> {
                customerBaseActivity.moveToProfileD()
            }
        }
    }

    override fun onFailureAddUpdateCustomerCResponse(error: String) {

    }

    override fun getUserId(): String {
        return SingletonUtils.instance.userId
    }

    override fun getCustomerId(): String {
        return SingletonUtils.instance.customerId
    }

    override fun getCustomerCId(): String {
        return SingletonUtils.instance.customerCId
    }

    override fun getCustomerCBillType(): String {
        return texteditRegisterBillType.text.toString()
    }

    override fun getCustomeCBillTime(): String {
        return texteditRegisterBillTime.text.toString()
    }

    override fun getCustomerCAdditionalCharge(): String {
        return texteditRegisterAdditionalCharge.text.toString()
    }

    override fun postAddUpdateCustomerC() {
        progressLoadingBar()
        addUpdateCustomerCPresenter.postCustomerCApiCall()
    }

}
