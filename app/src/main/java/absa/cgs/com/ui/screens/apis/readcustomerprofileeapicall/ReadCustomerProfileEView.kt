package absa.cgs.com.ui.screens.apis.readcustomerprofileeapicall

import absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.model.ReadCustomerProfileAResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilebapicall.model.ReadCustomerProfileBResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilecapicall.model.ReadCustomerProfileCResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofiledapicall.model.ReadCustomerProfileDResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofileeapicall.model.ReadCustomerProfileEResponseModel
import absa.cgs.com.ui.screens.apis.readprofileapicall.model.ReadProfileResponseModel
import absa.cgs.com.ui.screens.base.BaseMvpView

interface ReadCustomerProfileEView : BaseMvpView {
    fun onSuccessReadCustomerProfileEResponse(readCustomerProfileEResponseModel: ReadCustomerProfileEResponseModel)
    fun onFailureReadCustomerProfileEResponse(error: String)

    fun getCustomerID(): String

    fun hitGetCustomerProfileECall()
}
