package absa.cgs.com.ui.screens.apis.getnomineerelation.model

data class NomineeRelations(var nom_relation_id: String, var nominee_relations: String, var nom_relation_created: String)