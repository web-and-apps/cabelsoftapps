package absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall

import absa.cgs.com.ui.screens.base.BaseMvpPresenter


interface IReadCustomerProfileAListener<View : ReadCustomerProfileAView> : BaseMvpPresenter<View> {

    fun readCustomerProfileAData()

}
