package absa.cgs.com.ui.screens.customer.customerchildfragment.customersettopbox

import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.addupdateprofilecapicall.AddUodateCustomerCView
import absa.cgs.com.ui.screens.apis.addupdateprofilecapicall.AddUpdateCustomerCPresenter
import absa.cgs.com.ui.screens.apis.addupdateprofiledapicall.AddUodateCustomerDView
import absa.cgs.com.ui.screens.apis.addupdateprofiledapicall.AddUpdateCustomerDPresenter
import absa.cgs.com.ui.screens.apis.addupdateprofiledapicall.model.CustomerAddUpdateDResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofiledapicall.ReadCustomerProfileDPresenter
import absa.cgs.com.ui.screens.apis.readcustomerprofiledapicall.ReadCustomerProfileDView
import absa.cgs.com.ui.screens.apis.readcustomerprofiledapicall.model.ReadCustomerProfileDResponseModel
import absa.cgs.com.ui.screens.base.BaseFragment
import absa.cgs.com.ui.screens.customer.CustomerBaseActivity
import absa.cgs.com.ui.screens.customer.customerchildfragment.customerdetails.adapter.CustomerDetailsAdapter
import absa.cgs.com.ui.screens.customer.customerchildfragment.customersettopbox.adapter.CustomerSettopBoxAdapter
import absa.cgs.com.ui.screens.mainbaseactivity.OnListItemClickInterface
import absa.cgs.com.utils.SingletonUtils
import absa.cgs.com.utils.enums.CommonEnumUtils
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.customersettopboxfragment.*
import kotlinx.android.synthetic.main.fragment_customer_screen.*
import javax.inject.Inject

class CustomerSettopBoxFragment : BaseFragment(), CustomerBaseActivity.OnBackPressedListner, ReadCustomerProfileDView, AddUodateCustomerDView {


    lateinit var customerBaseActivity: CustomerBaseActivity

    @Inject
    lateinit var readCustomerProfileDPresenter: ReadCustomerProfileDPresenter<ReadCustomerProfileDView>

    private var customerSettopBoxAdapter: CustomerSettopBoxAdapter? = null

    @Inject
    lateinit var addUpdateCustomerDPresenter: AddUpdateCustomerDPresenter<AddUodateCustomerDView>

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        fragmentComponent().inject(this)
        readCustomerProfileDPresenter.attachView(activity!!, this)
        addUpdateCustomerDPresenter.attachView(activity!!, this)

    }


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.customersettopboxfragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()

    }

    override fun init() {
        customerBaseActivity = activity as CustomerBaseActivity
        onClickListeners()
        recyclerViewInit()
    }

    private fun recyclerViewInit() {
        val mLayoutManager = LinearLayoutManager(activity)
        boxDetailRecyclerView?.layoutManager = mLayoutManager
        boxDetailRecyclerView?.itemAnimator = DefaultItemAnimator()
    }

    override fun onBackPressed(): Boolean {
        activity!!.finish()
        return false
    }

    override fun onClickListeners() {
        super.onClickListeners()
        textviewBoxDetailAddBadge.setOnClickListener {
            addUpdateCustomerDPresenter.validateAddUpdateCustomerD()
        }

    }

    override fun onSuccessReadCustomerProfileDResponse(readCustomerProfileDResponseModel: ReadCustomerProfileDResponseModel) {
        customerSettopBoxAdapter = CustomerSettopBoxAdapter(activity!!, readCustomerProfileDResponseModel!!, object : OnListItemClickInterface {
            override fun OnSelectedItemClickListener(title: String, position: Int) {
                //customerDetailsOnClickListener(title, position, getAllCustomerResponseModel)
            }
        }
        )
        boxDetailRecyclerView?.adapter = customerSettopBoxAdapter
        customerSettopBoxAdapter!!.notifyDataSetChanged()
    }

    override fun onFailureReadCustomerProfileDResponse(error: String) {

    }

    override fun getCustomerID(): String {
        return SingletonUtils.instance.customerId
    }

    override fun hitGetCustomerProfileDCall() {
        progressLoadingBar()
        readCustomerProfileDPresenter.readCustomerProfileDData()
    }

    override fun onResume() {
        super.onResume()
        if (!commonUtils.cutNull(SingletonUtils.instance.customerId).isNullOrEmpty()) {
            hitGetCustomerProfileDCall()
        }
    }

    override fun onSuccessAddUpdateCustomerDResponse(customerAddUpdateDResponseModel: CustomerAddUpdateDResponseModel) {
        when (commonUtils.cutNull(customerAddUpdateDResponseModel.operation)) {
            CommonEnumUtils.ADDED.toString() -> {
                customerBaseActivity.moveToProfileE()
                customerBaseActivity.hitGetCustomerTabStatusCall()
            }
            CommonEnumUtils.UPDATED.toString() -> {
                customerBaseActivity.moveToProfileE()
            }
        }
    }

    override fun onFailureAddUpdateCustomerDResponse(error: String) {

    }

    override fun getUserId(): String {
        return SingletonUtils.instance.userId
    }

    override fun getCustomerId(): String {
        return SingletonUtils.instance.customerId
    }

    override fun getCustomerDId(): String {
        return ""
    }

    override fun getCustomerCSettopNo(): String {
        return texteditBoxNo.text.toString()
    }

    override fun getCustomeCSettopName(): String {
        return texteditRegisterBoxName.text.toString()
    }

    override fun getCustomerCSettopType(): String {
        return texteditLoginBoxType.text.toString()
    }

    override fun getCustomerCSmartCardNo(): String {
        return texteditLoginBoxSmartCardNo.text.toString()
    }

    override fun postAddUpdateCustomerD() {
        progressLoadingBar()
        addUpdateCustomerDPresenter.postCustomerDApiCall()
    }
}
