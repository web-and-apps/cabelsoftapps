package absa.cgs.com.ui.screens.apis.readcustomerprofiledapicall.model

class ReadCustomerProfileDResponseModel(var status_code: Int, var message: String, var product_d_details: List<CustomerProfileDDetails>)