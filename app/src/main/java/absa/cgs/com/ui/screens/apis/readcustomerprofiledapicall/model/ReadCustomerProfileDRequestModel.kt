package absa.cgs.com.ui.screens.apis.readcustomerprofiledapicall.model

data class ReadCustomerProfileDRequestModel(var customer_id: String)