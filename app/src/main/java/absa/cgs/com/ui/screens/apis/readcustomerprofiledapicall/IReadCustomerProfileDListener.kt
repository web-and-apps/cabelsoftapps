package absa.cgs.com.ui.screens.apis.readcustomerprofiledapicall

import absa.cgs.com.ui.screens.base.BaseMvpPresenter


interface IReadCustomerProfileDListener<View : ReadCustomerProfileDView> : BaseMvpPresenter<View> {

    fun readCustomerProfileDData()

}
