package aCsa.cgs.com.ui.screens.apis.addupdateprofilecapicall

import absa.cgs.com.data.RetrofitClient

import absa.cgs.com.ui.screens.apis.addupdateprofilecapicall.model.CustomerAddUpdateCRequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofilecapicall.model.CustomerAddUpdateCResponseModel
import absa.cgs.com.ui.screens.apis.addupdateprofiledapicall.model.CustomerAddUpdateDRequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofiledapicall.model.CustomerAddUpdateDResponseModel
import absa.cgs.com.utils.enums.HttpEnum
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AddUpdateCustomerDInteractor @Inject constructor() {
    private var customerAddUpdateDResponseModel: CustomerAddUpdateDResponseModel? = null


    interface OnCallHitListener {
        fun onSuccessAddUpdateCustomerDInteractListener(customerAddUpdateDResponseModel: CustomerAddUpdateDResponseModel)
        fun onRetrofitFailureAddUpdateCustomerDInteractListener(error: String)
        fun onSessionExpireAddUpdateCustomerDInteractListener()
        fun onErrorAddUpdateCustomerDInteractListener(customerAddUpdateDResponseModel: CustomerAddUpdateDResponseModel)
        fun onServerExceptionAddUpdateCustomerDInteractListener()
    }

    fun postAddUpdateCustomerDDataToServer(customerAddUpdateDRequestModel: CustomerAddUpdateDRequestModel, listener: OnCallHitListener) {
        RetrofitClient.instance.addUpdateCustomerD(customerAddUpdateDRequestModel)
                .enqueue(object : Callback<CustomerAddUpdateDResponseModel> {
                    override fun onFailure(call: Call<CustomerAddUpdateDResponseModel>, t: Throwable) {
                        listener.onRetrofitFailureAddUpdateCustomerDInteractListener(t.message.toString())
                    }

                    override fun onResponse(call: Call<CustomerAddUpdateDResponseModel>, response: Response<CustomerAddUpdateDResponseModel>) {

                        when (response.code()) {
                            HttpEnum.STATUS_UNAUTHORIZED.code -> {
                                listener.onSessionExpireAddUpdateCustomerDInteractListener()
                            }

                            HttpEnum.STATUS_ERROR.code -> {
                                customerAddUpdateDResponseModel = response.body()
                                listener.onErrorAddUpdateCustomerDInteractListener(customerAddUpdateDResponseModel!!)
                            }

                            HttpEnum.STATUS_OK.code -> {
                                customerAddUpdateDResponseModel = response.body()
                                listener.onSuccessAddUpdateCustomerDInteractListener(customerAddUpdateDResponseModel!!)
                            }

                            else -> {
                                listener.onServerExceptionAddUpdateCustomerDInteractListener()
                            }
                        }

                    }

                })
    }
}