package absa.cgs.com.ui.screens.customer.customerchildfragment.customerprofileB

import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.AddUodateCustomerAView
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.AddUpdateCustomerAPresenter
import absa.cgs.com.ui.screens.apis.addupdateprofilebapicall.AddUodateCustomerBView
import absa.cgs.com.ui.screens.apis.addupdateprofilebapicall.AddUpdateCustomerBPresenter
import absa.cgs.com.ui.screens.apis.addupdateprofilebapicall.model.CustomerAddUpdateBResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilebapicall.ReadCustomerProfileBPresenter
import absa.cgs.com.ui.screens.apis.readcustomerprofilebapicall.ReadCustomerProfileBView
import absa.cgs.com.ui.screens.apis.readcustomerprofilebapicall.model.ReadCustomerProfileBResponseModel
import absa.cgs.com.ui.screens.base.BaseFragment
import absa.cgs.com.ui.screens.customer.CustomerBaseActivity
import absa.cgs.com.utils.SingletonUtils
import absa.cgs.com.utils.enums.CommonEnumUtils
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.customerprofilebfragment.*
import javax.inject.Inject

class CustomerProfileBFragment : BaseFragment(), CustomerBaseActivity.OnBackPressedListner, ReadCustomerProfileBView, AddUodateCustomerBView {


    lateinit var customerBaseActivity: CustomerBaseActivity

    @Inject
    lateinit var readCustomerProfileBPresenter: ReadCustomerProfileBPresenter<ReadCustomerProfileBView>

    @Inject
    lateinit var addUpdateCustomerBPresenter: AddUpdateCustomerBPresenter<AddUodateCustomerBView>

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        fragmentComponent().inject(this)
        readCustomerProfileBPresenter.attachView(activity!!, this)
        addUpdateCustomerBPresenter.attachView(activity!!, this)

    }


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.customerprofilebfragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()

    }

    override fun init() {
        customerBaseActivity = activity as CustomerBaseActivity
        onClickListeners()
    }

    override fun onBackPressed(): Boolean {
        activity!!.finish()
        return false
    }

    override fun onClickListeners() {
        super.onClickListeners()
        customerProfileBUpdateBtn.setOnClickListener {
            addUpdateCustomerBPresenter.validateAddUpdateCustomerB()
        }
    }

    override fun onSuccessReadCustomerProfileBResponse(readCustomerProfileBResponseModel: ReadCustomerProfileBResponseModel) {
        SingletonUtils.instance.customerBId = commonUtils.cutNull(readCustomerProfileBResponseModel.profile_b_details.customer_b_id)
        texteditLoginBillingName.setText(commonUtils.cutNull(readCustomerProfileBResponseModel.profile_b_details.customer_b_billing_name))
        textedit_register_cs_emailid.setText(commonUtils.cutNull(readCustomerProfileBResponseModel.profile_b_details.customer_b_email))
        texteditRegisterCsAadhar.setText(commonUtils.cutNull(readCustomerProfileBResponseModel.profile_b_details.customer_b_aadhar_no))
        texteditRegisterEBNumber.setText(commonUtils.cutNull(readCustomerProfileBResponseModel.profile_b_details.customer_b_eb_no))
        textedit_register_cs_pan.setText(commonUtils.cutNull(readCustomerProfileBResponseModel.profile_b_details.customer_b_pan_no))
        texteditRegisterBoxAmount.setText(commonUtils.cutNull(readCustomerProfileBResponseModel.profile_b_details.customer_b_security_deposite))
    }

    override fun onFailureReadCustomerProfileBResponse(error: String) {

    }

    override fun getCustomerID(): String {
        return SingletonUtils.instance.customerId
    }

    override fun hitGetCustomerProfileBCall() {
        progressLoadingBar()
        readCustomerProfileBPresenter.readCustomerProfileBData()
    }

    override fun onResume() {
        super.onResume()
        if (!commonUtils.cutNull(SingletonUtils.instance.customerId).isNullOrEmpty()) {
            hitGetCustomerProfileBCall()
        }
    }


    override fun onSuccessAddUpdateCustomerBResponse(customerAddUpdateBResponseModel: CustomerAddUpdateBResponseModel) {

        when (commonUtils.cutNull(customerAddUpdateBResponseModel.operation)) {
            CommonEnumUtils.ADDED.toString() -> {
                customerBaseActivity.moveToProfileC()
                customerBaseActivity.hitGetCustomerTabStatusCall()
            }

            CommonEnumUtils.UPDATED.toString() -> {
                customerBaseActivity.moveToProfileC()
            }
        }

    }

    override fun onFailureAddUpdateCustomerBResponse(error: String) {

    }

    override fun getUserId(): String {
        return SingletonUtils.instance.userId
    }

    override fun getCustomerId(): String {
        return SingletonUtils.instance.customerId
    }

    override fun getCustomerBId(): String {
        return SingletonUtils.instance.customerBId
    }

    override fun getcustomerBBillingName(): String {
        return texteditLoginBillingName.text.toString()
    }

    override fun getCustomerBEmail(): String {
        return textedit_register_cs_emailid.text.toString()
    }

    override fun getCustomerBAadharNo(): String {
        return texteditRegisterCsAadhar.text.toString()
    }

    override fun getCustomerBEbNo(): String {
        return texteditRegisterEBNumber.text.toString()
    }

    override fun getCustomerBPanNo(): String {
        return textedit_register_cs_pan.text.toString()
    }

    override fun getCustomerBSecurityDeposite(): String {
        return texteditRegisterBoxAmount.text.toString()
    }

    override fun postAddUpdateCustomerB() {
        progressLoadingBar()
        addUpdateCustomerBPresenter.postCustomerBApiCall()
    }


}
