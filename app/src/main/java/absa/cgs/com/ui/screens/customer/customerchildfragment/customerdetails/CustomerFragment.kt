package absa.cgs.com.ui.screens.customer.customerchildfragment.customerdetails

import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.deletecustomerapicall.DeleteCustomerView
import absa.cgs.com.ui.screens.apis.deletecustomerapicall.DeleteCustomerpresenter
import absa.cgs.com.ui.screens.apis.getallcustomerapicall.GetCustomerPresenter
import absa.cgs.com.ui.screens.apis.getallcustomerapicall.GetCustomerView
import absa.cgs.com.ui.screens.apis.getallcustomerapicall.model.GetAllCustomerResponseModel
import absa.cgs.com.ui.screens.base.BaseFragment
import absa.cgs.com.ui.screens.customer.CustomerBaseActivity
import absa.cgs.com.ui.screens.customer.customerchildfragment.customerdetails.adapter.CustomerDetailsAdapter
import absa.cgs.com.ui.screens.expense.expensechildfragment.expensedetails.adapter.ExpenseDetailsAdapter
import absa.cgs.com.ui.screens.mainbaseactivity.MainActivity
import absa.cgs.com.ui.screens.mainbaseactivity.OnListItemClickInterface
import absa.cgs.com.utils.DialogUtils
import absa.cgs.com.utils.SingletonUtils
import absa.cgs.com.utils.enums.CommonEnumUtils
import absa.cgs.com.utils.enums.DialogEnum
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.expensedetailsfragment.*
import kotlinx.android.synthetic.main.fragment_customer_screen.*
import javax.inject.Inject


class CustomerFragment : BaseFragment(), GetCustomerView, DeleteCustomerView, DialogUtils.OnDialogPositiveListener {


    @Inject
    lateinit var getCustomerPresenter: GetCustomerPresenter<GetCustomerView>

    @Inject
    lateinit var deleteCustomerPresenter: DeleteCustomerpresenter<DeleteCustomerView>

    private var customerDetailsAdapter: CustomerDetailsAdapter? = null

    lateinit var mainActivity: MainActivity

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        fragmentComponent().inject(this)
        getCustomerPresenter.attachView(activity!!, this)
        deleteCustomerPresenter.attachView(activity!!, this)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_customer_screen, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun customerRegistration() {
        fcCustomerRegisterFActionButton.setOnClickListener {
            mainActivity.clearAllSingletonCustomerData()
            val intent = Intent(activity, CustomerBaseActivity::class.java)
            startActivity(intent)
        }

    }

    override fun init() {
        mainActivity = activity as MainActivity
        onClickListeners()
        recyclerViewInit()
    }

    override fun onClickListeners() {
        super.onClickListeners()
        customerRegistration()
    }

    private fun recyclerViewInit() {
        val mLayoutManager = LinearLayoutManager(activity)
        customerDetailsRcv?.layoutManager = mLayoutManager
        customerDetailsRcv?.itemAnimator = DefaultItemAnimator()
    }

    override fun onSuccessGetCustomerResponse(getAllCustomerResponseModel: GetAllCustomerResponseModel) {
        customerDetailsAdapter = CustomerDetailsAdapter(activity!!, getAllCustomerResponseModel, object : OnListItemClickInterface {
            override fun OnSelectedItemClickListener(title: String, position: Int) {
                customerDetailsOnClickListener(title, position, getAllCustomerResponseModel)
            }
        }
        )
        customerDetailsRcv?.adapter = customerDetailsAdapter
        customerDetailsAdapter!!.notifyDataSetChanged()
    }

    override fun onFailureGetCustomerResponse(error: String) {

    }

    override fun getUserID(): String {
        return SingletonUtils.instance.userId
    }

    override fun getSearchKeyword(): String {
        return ""
    }

    override fun getPageCount(): String {
        return "0"
    }

    override fun getFromDate(): String {
        return ""
    }

    override fun getToDate(): String {
        return ""
    }

    override fun onResume() {
        super.onResume()
        hitCustomerDetailsCall()
    }

    override fun hitCustomerDetailsCall() {
        progressLoadingBar()
        getCustomerPresenter.getCustomerData()
    }

    override fun customerDetailsOnClickListener(eventString: String, position: Int, getAllCustomerResponseModel: GetAllCustomerResponseModel) {
        when (eventString) {
            CommonEnumUtils.VIEW.toString() -> {
                SingletonUtils.instance.customerId = commonUtils.cutNull(getAllCustomerResponseModel.customer_details[position].customer_id)
                navigationRoutes(CustomerBaseActivity::class.java)
            }

            CommonEnumUtils.DELETE.toString() -> {
                SingletonUtils.instance.customerId = commonUtils.cutNull(getAllCustomerResponseModel.customer_details[position].customer_id)
                dialogUtils.showAlertDialog(activity!!, activity!!.resources.getString(R.string.DialogDeleteString), "", "", DialogEnum.DELETE.toString(), this)
            }
        }
    }

    override fun onDialogPositivePressed(dialog: Dialog, enumString: String) {
        when (enumString) {
            DialogEnum.DELETE.toString() -> {
                progressLoadingBar()
                deleteCustomerPresenter.postDeleteCustomerApiCall()
            }
        }

    }

    override fun onSuccessDeleteCustomerResponse() {

    }

    override fun onFailureDeleteCustomerResponse(error: String) {

    }

    override fun getCustomerID(): String {
        return SingletonUtils.instance.customerId
    }


}
