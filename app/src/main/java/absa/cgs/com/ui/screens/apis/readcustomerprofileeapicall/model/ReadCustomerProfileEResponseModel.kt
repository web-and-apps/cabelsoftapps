package absa.cgs.com.ui.screens.apis.readcustomerprofileeapicall.model

class ReadCustomerProfileEResponseModel(var status_code: Int, var message: String, var profile_e_details: CustomerProfileEDetails)