package absa.cgs.com.ui.screens.apis.readcustomerprofileeapicall.model

data class ReadCustomerProfileERequestModel(var customer_id: String)