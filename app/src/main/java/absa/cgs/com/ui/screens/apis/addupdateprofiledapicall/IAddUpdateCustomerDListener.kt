package absa.cgs.com.ui.screens.apis.addupdateprofiledapicall

import absa.cgs.com.ui.screens.base.BaseMvpPresenter

interface IAddUpdateCustomerDListener<View : AddUodateCustomerDView> : BaseMvpPresenter<View> {
    fun postCustomerDApiCall()
    fun validateAddUpdateCustomerD()
}