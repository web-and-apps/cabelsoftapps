package absa.cgs.com.ui.screens.customer.customerchildfragment.customersettopbox.adapter

import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.getallcustomerapicall.model.GetAllCustomerResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofiledapicall.model.ReadCustomerProfileDResponseModel
import absa.cgs.com.ui.screens.apis.readexpenseapicall.model.ReadExpenseResponseModel
import absa.cgs.com.ui.screens.mainbaseactivity.OnListItemClickInterface
import absa.cgs.com.utils.CommonUtils
import absa.cgs.com.utils.enums.CommonEnumUtils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView

import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import javax.inject.Inject


class CustomerSettopBoxAdapter(context: Context, private val readCustomerProfileDResponseModel: ReadCustomerProfileDResponseModel, val onListItemClickInterface: OnListItemClickInterface) : RecyclerView.Adapter<CustomerSettopBoxAdapter.DemoAdapter>() {


    inner class DemoAdapter(view: View) : RecyclerView.ViewHolder(view) {
        var customBoxDetailBoxNumber: TextView
        var customBoxDetailBoxName: TextView
        var customBoxDetailBoxType: TextView
        var customBoxDetailSmartCardNO: TextView
        var customBoxDetailImageViewDelete: ImageView


        init {
            customBoxDetailBoxNumber = view.findViewById(R.id.customBoxDetailBoxNumber) as TextView
            customBoxDetailBoxName = view.findViewById(R.id.customBoxDetailBoxName) as TextView
            customBoxDetailBoxType = view.findViewById(R.id.customBoxDetailBoxType) as TextView
            customBoxDetailSmartCardNO = view.findViewById(R.id.customBoxDetailSmartCardNO) as TextView
            customBoxDetailImageViewDelete = view.findViewById(R.id.customBoxDetailImageViewDelete) as ImageView
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DemoAdapter {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.custom_boxdetail_layout, parent, false)
        return DemoAdapter(itemView)
    }

    override fun onBindViewHolder(holder: DemoAdapter, position: Int) {
        holder.customBoxDetailBoxNumber.text = (readCustomerProfileDResponseModel.product_d_details.get(position).customer_d_settop_no)
        holder.customBoxDetailBoxName.text = (readCustomerProfileDResponseModel.product_d_details.get(position).customer_d_settop_name)
        holder.customBoxDetailBoxType.text = (readCustomerProfileDResponseModel.product_d_details.get(position).customer_d_settop_type)
        holder.customBoxDetailSmartCardNO.text = (readCustomerProfileDResponseModel.product_d_details.get(position).customer_d_smart_card_no)



        holder.customBoxDetailImageViewDelete.setOnClickListener {
            onListItemClickInterface.OnSelectedItemClickListener(CommonEnumUtils.DELETE.toString(), position)
        }


    }


    override fun getItemCount(): Int {
        return readCustomerProfileDResponseModel.product_d_details.size
    }
}
