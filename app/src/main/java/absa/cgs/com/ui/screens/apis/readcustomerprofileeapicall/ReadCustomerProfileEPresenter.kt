package absa.cgs.com.ui.screens.apis.readcustomerprofileeapicall


import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.model.ReadCustomerProfileARequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.model.ReadCustomerProfileAResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilebapicall.model.ReadCustomerProfileBRequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilebapicall.model.ReadCustomerProfileBResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilecapicall.model.ReadCustomerProfileCRequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilecapicall.model.ReadCustomerProfileCResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofiledapicall.model.ReadCustomerProfileDRequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofiledapicall.model.ReadCustomerProfileDResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofileeapicall.model.ReadCustomerProfileERequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofileeapicall.model.ReadCustomerProfileEResponseModel
import absa.cgs.com.ui.screens.apis.readexpenseapicall.model.ReadExpenseRequestModel
import absa.cgs.com.ui.screens.apis.readexpenseapicall.model.ReadExpenseResponseModel
import absa.cgs.com.ui.screens.authentication.AuthenticationBaseActivity
import absa.cgs.com.ui.screens.base.BasePresenter
import javax.inject.Inject

class ReadCustomerProfileEPresenter<View : ReadCustomerProfileEView> @Inject constructor(var readCustomerProfileEInteractor: ReadCustomerProfileEInteractor) : BasePresenter<View>(), IReadCustomerProfileEListener<View>, ReadCustomerProfileEInteractor.OnCallHitListener {
    override fun readCustomerProfileEData() {
        val readCustomerProfileERequestModel = ReadCustomerProfileERequestModel(
                getBaseMvpVieww().getCustomerID())
        readCustomerProfileEInteractor.getReadCustomerProfileEDataToServer(readCustomerProfileERequestModel, this)
    }

    override fun onSuccessReadCustomerEProfileInteractListener(readCustomerProfileEResponseModel: ReadCustomerProfileEResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().onSuccessReadCustomerProfileEResponse(readCustomerProfileEResponseModel)
    }

    override fun onRetrofitFailureReadCustomerEProfileInteractListener(error: String) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(error)
    }

    override fun onSessionExpireReadCustomerEProfileInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        activity!!.finishAffinity()
        getBaseMvpVieww().navigationRoutes(AuthenticationBaseActivity::class.java)
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.SessionTokenExpire))
    }

    override fun onErrorReadCustomerEProfileInteractListener(readCustomerProfileEResponseModel: ReadCustomerProfileEResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        //getBaseMvpVieww().showToastLong(readCustomerProfileEResponseModel.message)
    }

    override fun onServerExceptionReadCustomerEProfileInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.ServerBusyString))
    }


}
