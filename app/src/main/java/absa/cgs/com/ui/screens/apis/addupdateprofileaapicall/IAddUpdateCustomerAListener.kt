package absa.cgs.com.ui.screens.apis.addupdateprofileaapicall

import absa.cgs.com.ui.screens.base.BaseMvpPresenter

interface IAddUpdateCustomerAListener<View : AddUodateCustomerAView> : BaseMvpPresenter<View> {
    fun postCustomerAApiCall()
    fun validateAddUpdateCustomerA()
}