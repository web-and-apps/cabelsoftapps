package absa.cgs.com.ui.screens.apis.plan.addplanapicall

import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateAResponseModel
import absa.cgs.com.ui.screens.apis.plan.addplanapicall.model.AddPlanResponseModel
import absa.cgs.com.ui.screens.base.BaseMvpView

interface AddPlanView : BaseMvpView {

    fun onSuccessAddPlanResponse(addPlanResponseModel: AddPlanResponseModel)
    fun onFailureAddPlanResponse(error: String)

    fun getPlanName(): String
    fun getPlanCost(): String
    fun getPlanCode(): String
    fun getPlanType(): String
    fun getPlanGst(): String
    fun getPlanArrayData(): String
    fun postNewPlan()
    fun getProductTypeCheckedRadioButtonValue()
    fun validatePlanTypeRadioGroupChecked(): Boolean

}