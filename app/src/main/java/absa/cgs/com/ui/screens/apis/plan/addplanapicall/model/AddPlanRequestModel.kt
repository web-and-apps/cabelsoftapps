package absa.cgs.com.ui.screens.apis.plan.addplanapicall.model

data class AddPlanRequestModel(var plan_name: String, var plan_cost: String, var plan_code: String, var plan_type: String, var plan_gst: String,var plan_array_data: String)