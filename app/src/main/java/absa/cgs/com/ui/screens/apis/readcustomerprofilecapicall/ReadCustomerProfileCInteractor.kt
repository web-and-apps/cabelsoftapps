package absa.cgs.com.ui.screens.apis.readcustomerprofilecapicall

import absa.cgs.com.data.RetrofitClient
import absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.model.ReadCustomerProfileARequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.model.ReadCustomerProfileAResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilebapicall.model.ReadCustomerProfileBRequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilebapicall.model.ReadCustomerProfileBResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilecapicall.model.ReadCustomerProfileCRequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilecapicall.model.ReadCustomerProfileCResponseModel
import absa.cgs.com.ui.screens.apis.readprofileapicall.model.ReadProfileRequestModel
import absa.cgs.com.ui.screens.apis.readprofileapicall.model.ReadProfileResponseModel
import absa.cgs.com.utils.enums.HttpEnum
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ReadCustomerProfileCInteractor @Inject constructor() {

    private var readCustomerProfileCResponseModel: ReadCustomerProfileCResponseModel? = null


    interface OnCallHitListener {
        fun onSuccessReadCustomerCProfileInteractListener(readCustomerProfileCResponseModel: ReadCustomerProfileCResponseModel)
        fun onRetrofitFailureReadCustomerCProfileInteractListener(error: String)
        fun onSessionExpireReadCustomerCProfileInteractListener()
        fun onErrorReadCustomerCProfileInteractListener(readCustomerProfileCResponseModel: ReadCustomerProfileCResponseModel)
        fun onServerExceptionReadCustomerCProfileInteractListener()
    }


    fun getReadCustomerProfileCDataToServer(readCustomerProfileCRequestModel: ReadCustomerProfileCRequestModel, listener: OnCallHitListener) {
        RetrofitClient.instance.readCustomerProfileCData(readCustomerProfileCRequestModel)
                .enqueue(object : Callback<ReadCustomerProfileCResponseModel> {
                    override fun onFailure(call: Call<ReadCustomerProfileCResponseModel>, t: Throwable) {
                        listener.onRetrofitFailureReadCustomerCProfileInteractListener(t.message.toString())
                    }

                    override fun onResponse(call: Call<ReadCustomerProfileCResponseModel>, response: Response<ReadCustomerProfileCResponseModel>) {
                        when (response.code()) {
                            HttpEnum.STATUS_UNAUTHORIZED.code -> {
                                listener.onSessionExpireReadCustomerCProfileInteractListener()
                            }

                            HttpEnum.STATUS_ERROR.code -> {
                                readCustomerProfileCResponseModel = response.body()
                                listener.onErrorReadCustomerCProfileInteractListener(readCustomerProfileCResponseModel!!)
                            }

                            HttpEnum.STATUS_OK.code -> {
                                readCustomerProfileCResponseModel = response.body()
                                listener.onSuccessReadCustomerCProfileInteractListener(readCustomerProfileCResponseModel!!)
                            }

                            else -> {
                                listener.onServerExceptionReadCustomerCProfileInteractListener()
                            }
                        }

                    }

                })
    }

}
