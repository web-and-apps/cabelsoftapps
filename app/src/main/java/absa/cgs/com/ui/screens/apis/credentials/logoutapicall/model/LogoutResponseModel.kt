package absa.cgs.com.ui.screens.apis.credentials.logoutapicall.model

data class LogoutResponseModel(var status: Int, var message: String)