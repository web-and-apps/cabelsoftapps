package absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model

data class CustomerAddUpdateARequestModel(var user_id: String, var customer_id: String,
                                          var customer_name: String, var customer_area: String,
                                          var customer_street: String, var customer_home_type: String,
                                          var customer_mobile: String)