package absa.cgs.com.ui.screens.apis.plan.updateplanapicall.model

data class UpdatePlanRequestModel(var plan_id: String, var plan_name: String, var plan_cost: String, var plan_code: String, var plan_gst: String, var plan_array_data: String)