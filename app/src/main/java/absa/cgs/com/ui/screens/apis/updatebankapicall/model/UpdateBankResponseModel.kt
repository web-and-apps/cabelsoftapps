package absa.cgs.com.ui.screens.apis.updatebankapicall.model

data class UpdateBankResponseModel(var status_code: Int, var message: String)