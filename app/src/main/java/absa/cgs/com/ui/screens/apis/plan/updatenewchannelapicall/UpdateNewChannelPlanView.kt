package absa.cgs.com.ui.screens.apis.plan.updatenewchannelapicall

import absa.cgs.com.ui.screens.apis.plan.updateplanapicall.model.UpdatePlanResponseModel
import absa.cgs.com.ui.screens.base.BaseMvpView

interface UpdateNewChannelPlanView : BaseMvpView {

    fun onSuccessUpdatePlanResponse(updatePlanResponseModel: UpdatePlanResponseModel)
    fun onFailureUpdatePlanResponse(error: String)
    fun getPlanId(): String
    fun getPlanArrayData(): String
}