package absa.cgs.com.ui.screens.apis.plan.updateplanapicall

import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.addexpenseapicall.model.AddExpenseRequestModel
import absa.cgs.com.ui.screens.apis.addexpenseapicall.model.AddExpenseResponseModel
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateARequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateAResponseModel
import absa.cgs.com.ui.screens.apis.plan.addplanapicall.model.AddPlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.addplanapicall.model.AddPlanResponseModel
import absa.cgs.com.ui.screens.apis.plan.updateplanapicall.model.UpdatePlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.updateplanapicall.model.UpdatePlanResponseModel
import absa.cgs.com.ui.screens.authentication.AuthenticationBaseActivity
import absa.cgs.com.ui.screens.base.BasePresenter
import absa.cgs.com.utils.CommonUtils
import absa.cgs.com.utils.enums.CommonEnumUtils
import javax.inject.Inject

class UpdatePlanPresenter<View : UpdatePlanView> @Inject constructor(var updatePlanInteractor: UpdatePlanInteractor, private val commonUtils: CommonUtils) : BasePresenter<View>(), IUpdatePlanListener<View>, UpdatePlanInteractor.OnCallHitListener {


    override fun updatePlanApiCall() {
        var updatePlanRequestModel = UpdatePlanRequestModel(getBaseMvpVieww().getPlanId(), getBaseMvpVieww().getPlanName(), getBaseMvpVieww().getPlanCost(), getBaseMvpVieww().getPlanCode(), getBaseMvpVieww().getPlanGst(), getBaseMvpVieww().getPlanArrayData())
        updatePlanInteractor.updatePlanDataToServer(updatePlanRequestModel, this)
    }

    override fun onSuccessUpdatePlanInteractListener(updatePlanResponseModel: UpdatePlanResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().onSuccessUpdatePlanResponse(updatePlanResponseModel)
        //getBaseMvpVieww().showToastLong(commonUtils.cutNull(updatePlanResponseModel.message))
    }

    override fun onRetrofitFailureUpdatePlanInteractListener(error: String) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(error)
    }

    override fun onSessionExpireUpdatePlanInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        activity!!.finishAffinity()
        getBaseMvpVieww().navigationRoutes(AuthenticationBaseActivity::class.java)
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.SessionTokenExpire))
    }

    override fun onErrorUpdatePlanInteractListener(updatePlanResponseModel: UpdatePlanResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(updatePlanResponseModel.message)
    }

    override fun onServerExceptionUpdatePlanInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.ServerBusyString))
    }

    override fun validatePlanUpdateDetails() {

        when (getBaseMvpVieww().getPlanTyp()) {
            CommonEnumUtils.FREE.toString() -> {
                if (getBaseMvpVieww().getPlanName().equals("")) {
                    getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.NeedPlanName))
                } else if (getBaseMvpVieww().getPlanCode().equals("")) {
                    getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.NeedPlanCode))
                } else {
                    getBaseMvpVieww().hitUpdatePlanCall()
                }
            }
            else -> {
                if (getBaseMvpVieww().getPlanName().equals("")) {
                    getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.NeedPlanName))
                } else if (getBaseMvpVieww().getPlanCode().equals("")) {
                    getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.NeedPlanCode))
                } else if (getBaseMvpVieww().getPlanCost().equals("") or getBaseMvpVieww().getPlanCost().equals("0")) {
                    getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.NeedPlanCost))
                } else if (getBaseMvpVieww().getPlanGst().equals("")) {
                    getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.NeedPlanGst))
                } else {
                    getBaseMvpVieww().hitUpdatePlanCall()
                }
            }
        }


    }
}