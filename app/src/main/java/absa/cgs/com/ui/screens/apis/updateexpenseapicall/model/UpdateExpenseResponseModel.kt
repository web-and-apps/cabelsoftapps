package absa.cgs.com.ui.screens.apis.updateexpenseapicall.model

data class UpdateExpenseResponseModel(var status_code: Int, var message: String)