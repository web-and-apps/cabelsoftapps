package absa.cgs.com.ui.screens.plan.planchildfragment.updatenewchannels

import absa.cgs.com.kotlinplayground.R

import absa.cgs.com.ui.screens.apis.plan.getparticularplanapicall.model.GetPlanResponseModel
import absa.cgs.com.ui.screens.apis.plan.getplanapicall.GetPlanPresenter
import absa.cgs.com.ui.screens.apis.plan.getplanapicall.GetPlanView
import absa.cgs.com.ui.screens.apis.plan.updatenewchannelapicall.UpdateNewChannelPlanPresenter
import absa.cgs.com.ui.screens.apis.plan.updatenewchannelapicall.UpdateNewChannelPlanView
import absa.cgs.com.ui.screens.apis.plan.updateplanapicall.model.UpdatePlanResponseModel
import absa.cgs.com.ui.screens.base.BaseFragment
import absa.cgs.com.ui.screens.mainbaseactivity.OnListItemClickInterface
import absa.cgs.com.ui.screens.mainbaseactivity.mainchildfragments.plans.PlanView
import absa.cgs.com.ui.screens.plan.PlanBaseActivity
import absa.cgs.com.ui.screens.plan.planchildfragment.updatenewchannels.adapter.PlanUpdateNewChannelAdapter
import absa.cgs.com.utils.DialogUtils
import absa.cgs.com.utils.RecyclerViewItemDecorator
import absa.cgs.com.utils.SingletonUtils
import absa.cgs.com.utils.enums.CommonEnumUtils
import absa.cgs.com.utils.enums.DialogEnum
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.fragment_update_new_channel.*
import javax.inject.Inject
import kotlin.text.split as split1

class PlanUpdateNewChannelFragment : BaseFragment(), PlanView, GetPlanView, UpdateNewChannelPlanView, DialogUtils.OnDialogPositiveListener, PlanBaseActivity.OnBackPressedListner {


    lateinit var planBaseActivity: PlanBaseActivity

    @Inject
    lateinit var getPlanDetailsPresenter: GetPlanPresenter<GetPlanView>

    @Inject
    lateinit var UpdateNewChannelPlanPresenter: UpdateNewChannelPlanPresenter<UpdateNewChannelPlanView>

    private var searchTag = ""
    private var planIds = ""
    private var planId = ""
    private var planIdsArrayData: List<String>? = null
    private var planUpdateNewChannelAdapter: PlanUpdateNewChannelAdapter? = null


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        fragmentComponent().inject(this)
        getPlanDetailsPresenter.attachView(activity!!, this)
        UpdateNewChannelPlanPresenter.attachView(activity!!, this)

        init()
        hitGetPlanDetails()
    }


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_update_new_channel, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()


    }

    private fun recyclerViewInit() {
        val mLayoutManager = GridLayoutManager(activity!!, 2)
        planUpdateNewChannelDetailsRcv?.layoutManager = mLayoutManager
        planUpdateNewChannelDetailsRcv.addItemDecoration(RecyclerViewItemDecorator(1))
    }


    override fun init() {
        getAllBundleDatas()
        planBaseActivity = activity as PlanBaseActivity
        onClickListeners()
        recyclerViewInit()
    }

    override fun onSuccessUpdatePlanResponse(updatePlanResponseModel: UpdatePlanResponseModel) {
        showToastLong(commonUtils.cutNull(updatePlanResponseModel.message))
        SingletonUtils.instance.planArrayData = planIds
        planBaseActivity.popBackFragment()
    }

    override fun onFailureUpdatePlanResponse(error: String) {

    }

    override fun getPlanId(): String {
        return planId
    }

    override fun getPlanArrayData(): String {
        return planIds
    }

    override fun onClickListeners() {
        super.onClickListeners()
        addPlanOnClickListener()
    }

    override fun getAllBundleDatas() {
        if (arguments != null) {
            var plan_array_data = arguments?.getString("plan_array_data")
            var plan_id = arguments?.getString("plan_id")
            if (plan_array_data != null) {
                planIds = SingletonUtils.instance.planArrayData
                planIdsArrayData = planIds.split1(",")
            }

            if (plan_id != null) {
                planId = plan_id
            }
        }
    }

    override fun onSuccessGetParticularPlanResponse(getPlanResponseModel: GetPlanResponseModel) {

        ///This login for set already selected channel once again to the array list
        for (n in planIdsArrayData!!.indices) {
            for (x in getPlanResponseModel.product_details.indices) {
                if (planIdsArrayData!![n] == getPlanResponseModel.product_details[x].plan_id) {
                    getPlanResponseModel.product_details[x].selection = true
                }
            }
        }
        ///This login for set already selected channel once again to the array list

        planUpdateNewChannelAdapter = PlanUpdateNewChannelAdapter(activity!!, getPlanResponseModel, object : OnListItemClickInterface {
            override fun OnSelectedItemClickListener(title: String, position: Int) {
                planDetailsOnClickListener(title, position, getPlanResponseModel)
            }
        }
        )
        planUpdateNewChannelDetailsRcv?.adapter = planUpdateNewChannelAdapter
        planUpdateNewChannelAdapter!!.notifyDataSetChanged()
    }


    private fun planDetailsOnClickListener(eventString: String, position: Int, getPlanResponseModel: GetPlanResponseModel) {

        when (eventString) {
            CommonEnumUtils.VIEW.toString() -> {

            }

        }
    }


    override fun onFailureGetParticularPlanResponse(error: String) {

    }

    private fun addPlanOnClickListener() {
        planUpdateBtn.setOnClickListener {
            planIds = ""
            for (i in planUpdateNewChannelAdapter?.getPlanResponseModel()!!.product_details.indices) {
                when (planUpdateNewChannelAdapter?.getPlanResponseModel()!!.product_details[i].selection) {
                    true -> {
                        planIds += planUpdateNewChannelAdapter?.getPlanResponseModel()!!.product_details[i].plan_id + ","
                    }
                    false -> {
                    }
                }
            }
            progressLoadingBar()
            UpdateNewChannelPlanPresenter.updatePlanApiCall()
        }
    }

    override fun getPlanIds(): String {
        return ""
    }

    override fun getFromDate(): String {
        return ""
    }

    override fun getToDate(): String {
        return ""
    }

    override fun getPageCount(): String {
        return "0"
    }

    override fun getSearchKeyword(): String {
        return ""
    }

    override fun getSearchTag(): String {
        return ""
    }

    override fun hitGetPlanDetails() {
        progressLoadingBar()
        getPlanDetailsPresenter.getPlanApiCall()
    }

    override fun onBackPressed(): Boolean {
        planBaseActivity.popBackFragment()
        return false
    }

    override fun setAppBarTitle(title: Int) {

    }

    override fun onDialogPositivePressed(dialog: Dialog, enumString: String) {
        when (enumString) {
            DialogEnum.DELETE.toString() -> {

            }
        }

    }


}