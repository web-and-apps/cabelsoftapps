package absa.cgs.com.ui.screens.customer

import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.customertabstatusapicall.ReadCustomerTabStatusPresenter
import absa.cgs.com.ui.screens.apis.customertabstatusapicall.ReadCustomerTabStatusView
import absa.cgs.com.ui.screens.apis.customertabstatusapicall.model.CustomerTabStatusResponseModel
import absa.cgs.com.ui.screens.base.BaseActivity
import absa.cgs.com.ui.screens.customer.customerchildfragment.customerbilling.CustomerBillingFragment
import absa.cgs.com.ui.screens.customer.customerchildfragment.customerentydate.CustomerEntryDateFragment
import absa.cgs.com.ui.screens.customer.customerchildfragment.customerprofileA.CustomerProfileAFragment
import absa.cgs.com.ui.screens.customer.customerchildfragment.customerprofileB.CustomerProfileBFragment
import absa.cgs.com.ui.screens.customer.customerchildfragment.customersettopbox.CustomerSettopBoxFragment
import absa.cgs.com.utils.SingletonUtils
import absa.cgs.com.utils.enums.CommonEnumIntUtils
import absa.cgs.com.utils.enums.CommonEnumUtils
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_customer_register.*
import javax.inject.Inject


class CustomerBaseActivity : BaseActivity(), CustomerView, ReadCustomerTabStatusView {


    private val fragmentManager = supportFragmentManager
    private val customerProfileAFragment = CustomerProfileAFragment()
    private val customerProfileBFragment = CustomerProfileBFragment()
    private val customerBillingFragment = CustomerBillingFragment()
    private val customerSettopBoxFragment = CustomerSettopBoxFragment()
    private val customerEntryDateFragment = CustomerEntryDateFragment()

    @Inject
    lateinit var readCustomerTabStatusPresenter: ReadCustomerTabStatusPresenter<ReadCustomerTabStatusView>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setActionBarTitle()
        setContentView(R.layout.activity_customer_register)
        injection()
        init()

    }


    private fun injection() {
        activityComponent().inject(this)
        readCustomerTabStatusPresenter.attachView(this, this)
    }


    override fun init() {
        val args = Bundle()
        changeFragment(0, args)
        onClickListeners()
        isCustomerIdExist()
    }

    override fun isCustomerIdExist() {
        when (SingletonUtils.instance.customerId) {

            null -> {
            }
            "" -> {
            }
            else -> {
                hitGetCustomerTabStatusCall()
            }

        }
    }


    override fun changeFragment(position: Int, bundle: Bundle) {
        when (position) {
            0 -> {
                setActionBarTitle(R.string.CustomerProfileAString)
                ///
                val fragmentTransaction = fragmentManager.beginTransaction()
                customerProfileAFragment.arguments = bundle
                fragmentTransaction.replace(R.id.customerFragmentLayout, customerProfileAFragment)
                fragmentTransaction.commit()
            }
            1 -> {
                setActionBarTitle(R.string.CustomerProfileBString)
                ///
                val fragmentTransaction = fragmentManager.beginTransaction()
                customerProfileAFragment.arguments = bundle
                fragmentTransaction.replace(R.id.customerFragmentLayout, customerProfileBFragment)
                fragmentTransaction.commit()
            }

            2 -> {
                setActionBarTitle(R.string.CustomerBillingDetailsString)
                ///
                val fragmentTransaction = fragmentManager.beginTransaction()
                customerProfileAFragment.arguments = bundle
                fragmentTransaction.replace(R.id.customerFragmentLayout, customerBillingFragment)
                fragmentTransaction.commit()
            }
            3 -> {
                setActionBarTitle(R.string.CustomerSetTopBoxDetailsString)
                ///
                val fragmentTransaction = fragmentManager.beginTransaction()
                customerProfileAFragment.arguments = bundle
                fragmentTransaction.replace(R.id.customerFragmentLayout, customerSettopBoxFragment)
                fragmentTransaction.commit()
            }
            4 -> {
                setActionBarTitle(R.string.CustomerEntryDateDetailsString)
                ///
                val fragmentTransaction = fragmentManager.beginTransaction()
                customerProfileAFragment.arguments = bundle
                fragmentTransaction.replace(R.id.customerFragmentLayout, customerEntryDateFragment)
                fragmentTransaction.commit()
            }


        }
    }

    override fun onClickListeners() {
        super.onClickListeners()
        customerLeftStageCircleOneTv.setOnClickListener {
            val args = Bundle()
            changeFragment(0, args)
        }

        customerLeftStageCircleTwoTv.setOnClickListener {
            val args = Bundle()
            changeFragment(1, args)
        }
        customerLeftStageCircleThreeTv.setOnClickListener {
            val args = Bundle()
            changeFragment(2, args)
        }
        customerLeftStageCircleFourTv.setOnClickListener {
            val args = Bundle()
            changeFragment(3, args)
        }
        customerLeftStageCircleFiveTv.setOnClickListener {
            val args = Bundle()
            changeFragment(4, args)
        }
    }

    override fun setActionBarTitle(string: Int) {
        setTitle(string)
    }


    private fun setActionBarTitle() {
        setTitle(R.string.add_customer)
    }


    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onBackPressed() {
        val fragment = supportFragmentManager.findFragmentById(R.id.customerFragmentLayout)
        if (fragment !is CustomerBaseActivity.OnBackPressedListner || !(fragment as CustomerBaseActivity.OnBackPressedListner).onBackPressed()) {

        }
    }


    interface OnBackPressedListner {
        fun onBackPressed(): Boolean
    }

    override fun onSuccessReadCustomerTabStatusResponse(customerTabStatusResponseModel: CustomerTabStatusResponseModel) {
        changeCustomerTabStatusColor(customerTabStatusResponseModel)
    }

    override fun onFailureReadCustomerTabStatusResponse(error: String) {

    }

    override fun getCustomerId(): String {
        return SingletonUtils.instance.customerId
    }

    override fun hitGetCustomerTabStatusCall() {
        progressLoadingBar()
        readCustomerTabStatusPresenter.readCustomerTabStatusData()
    }

    override fun changeCustomerTabStatusColor(customerTabStatusResponseModel: CustomerTabStatusResponseModel) {
        if (commonUtils.cutNull(customerTabStatusResponseModel.customer_profile_status.customer_a_status) == CommonEnumUtils.Y.toString()) {
            customerLeftStageCircleOneTv.background = (this.resources.getDrawable(R.drawable.circle_outline_green))
        } else if (commonUtils.cutNull(customerTabStatusResponseModel.customer_profile_status.customer_a_status) == CommonEnumUtils.P.toString()) {
            customerLeftStageCircleOneTv.background = (this.resources.getDrawable(R.drawable.circle_outline_blue))
        } else {
            customerLeftStageCircleOneTv.background = (this.resources.getDrawable(R.drawable.circle_outline_grey))
        }

        if (commonUtils.cutNull(customerTabStatusResponseModel.customer_profile_status.customer_b_status) == CommonEnumUtils.Y.toString()) {
            customerLeftStageCircleTwoTv.background = (this.resources.getDrawable(R.drawable.circle_outline_green))
        } else if (commonUtils.cutNull(customerTabStatusResponseModel.customer_profile_status.customer_b_status) == CommonEnumUtils.P.toString()) {
            customerLeftStageCircleTwoTv.background = (this.resources.getDrawable(R.drawable.circle_outline_blue))
        } else {
            customerLeftStageCircleTwoTv.background = (this.resources.getDrawable(R.drawable.circle_outline_grey))
        }

        if (commonUtils.cutNull(customerTabStatusResponseModel.customer_profile_status.customer_c_status) == CommonEnumUtils.Y.toString()) {
            customerLeftStageCircleThreeTv.background = (this.resources.getDrawable(R.drawable.circle_outline_green))
        } else if (commonUtils.cutNull(customerTabStatusResponseModel.customer_profile_status.customer_c_status) == CommonEnumUtils.P.toString()) {
            customerLeftStageCircleThreeTv.background = (this.resources.getDrawable(R.drawable.circle_outline_blue))
        } else {
            customerLeftStageCircleThreeTv.background = (this.resources.getDrawable(R.drawable.circle_outline_grey))
        }

        if (commonUtils.cutNull(customerTabStatusResponseModel.customer_profile_status.customer_d_status) == CommonEnumUtils.Y.toString()) {
            customerLeftStageCircleFourTv.background = (this.resources.getDrawable(R.drawable.circle_outline_green))
        } else if (commonUtils.cutNull(customerTabStatusResponseModel.customer_profile_status.customer_d_status) == CommonEnumUtils.P.toString()) {
            customerLeftStageCircleFourTv.background = (this.resources.getDrawable(R.drawable.circle_outline_blue))
        } else {
            customerLeftStageCircleFourTv.background = (this.resources.getDrawable(R.drawable.circle_outline_grey))
        }

        if (commonUtils.cutNull(customerTabStatusResponseModel.customer_profile_status.customer_e_status) == CommonEnumUtils.Y.toString()) {
            customerLeftStageCircleFiveTv.background = (this.resources.getDrawable(R.drawable.circle_outline_green))
        } else if (commonUtils.cutNull(customerTabStatusResponseModel.customer_profile_status.customer_e_status) == CommonEnumUtils.P.toString()) {
            customerLeftStageCircleFiveTv.background = (this.resources.getDrawable(R.drawable.circle_outline_blue))
        } else {
            customerLeftStageCircleFiveTv.background = (this.resources.getDrawable(R.drawable.circle_outline_grey))
        }
    }

    override fun clearAllSingletonCustomerData() {
        SingletonUtils.instance.clearSingletonCustomerDatas()
    }


    override fun moveToProfileA() {
        val arg = Bundle()
        changeFragment(0, arg)
    }

    override fun moveToProfileB() {
        val arg = Bundle()
        changeFragment(1, arg)
    }

    override fun moveToProfileC() {
        val arg = Bundle()
        changeFragment(2, arg)
    }

    override fun moveToProfileD() {
        val arg = Bundle()
        changeFragment(3, arg)
    }

    override fun moveToProfileE() {
        val arg = Bundle()
        changeFragment(4, arg)
    }


}
