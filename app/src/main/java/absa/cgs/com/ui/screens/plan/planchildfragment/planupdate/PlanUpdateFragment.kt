package absa.cgs.com.ui.screens.plan.planchildfragment.planupdate

import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.plan.getparticularplanapicall.GetParticularPlanPresenter
import absa.cgs.com.ui.screens.apis.plan.getparticularplanapicall.GetParticularPlanView
import absa.cgs.com.ui.screens.apis.plan.getparticularplanapicall.model.GetPlanResponseModel
import absa.cgs.com.ui.screens.apis.plan.updateplanapicall.UpdatePlanPresenter
import absa.cgs.com.ui.screens.apis.plan.updateplanapicall.UpdatePlanView
import absa.cgs.com.ui.screens.apis.plan.updateplanapicall.model.UpdatePlanResponseModel
import absa.cgs.com.ui.screens.base.BaseFragment
import absa.cgs.com.ui.screens.mainbaseactivity.OnListItemClickInterface
import absa.cgs.com.ui.screens.mainbaseactivity.mainchildfragments.plans.PlanView
import absa.cgs.com.ui.screens.plan.PlanBaseActivity
import absa.cgs.com.ui.screens.plan.planchildfragment.planupdate.adapter.PlanGetAllChannelAdapter
import absa.cgs.com.utils.RecyclerViewItemDecorator
import absa.cgs.com.utils.SingletonUtils
import absa.cgs.com.utils.enums.CommonEnumUtils
import android.opengl.Visibility
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.fragment_plan_update_screen.*
import javax.inject.Inject


class PlanUpdateFragment : BaseFragment(), PlanView, UpdatePlanView, GetParticularPlanView, PlanBaseActivity.OnBackPressedListner {


    lateinit var planBaseActivity: PlanBaseActivity
    private var planId: String? = ""
    private var planTag: String? = ""
    private var plan_array_data: String? = ""
    var editAndCancelBool: Boolean? = true
    private var planGetAllChannelAdapter: PlanGetAllChannelAdapter? = null


    @Inject
    lateinit var updatePlanPresenter: UpdatePlanPresenter<UpdatePlanView>

    @Inject
    lateinit var getParticularPlanPresenter: GetParticularPlanPresenter<GetParticularPlanView>

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        fragmentComponent().inject(this)
        getParticularPlanPresenter.attachView(activity!!, this)
        updatePlanPresenter.attachView(activity!!, this)
    }


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_plan_update_screen, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()

    }


    override fun init() {
        super.init()
        planBaseActivity = activity as PlanBaseActivity
        setPlanDisable()
        onClickListeners()
        recyclerViewInit()
    }

    override fun onClickListeners() {
        super.onClickListeners()
        planButtonSetOnClickListener()
        editAddChannelSetOnClickListener()
    }

    override fun getAllBundleDatas() {
        if (arguments != null) {
            var planGst = arguments?.getString("plan_gst")
            var planName = arguments?.getString("plan_name")
            var planCode = arguments?.getString("plan_code")
            var planCost = arguments?.getString("plan_cost")
            planId = arguments?.getString("plan_id")
            planTag = arguments?.getString("plan_tag")
            plan_array_data = arguments?.getString("plan_array_data")

            if (planGst != null) {
                editPlanGstEd.setText(planGst)
            }
            if (planName != null) {
                editPlanNameEd.setText(planName)
            }
            if (planCode != null) {
                editPlanCodeEd.setText(planCode)
            }
            if (planCost != null) {
                editPlanCostEd.setText(planCost)
            }

            if (planTag != null) {
                when (planTag) {
                    CommonEnumUtils.FREE.toString() -> {
                        addChannelConstraints.visibility = View.GONE
                        editPlanCostTi.visibility = View.GONE
                        editPlanGstTi.visibility = View.GONE
                    }

                    CommonEnumUtils.PAID.toString() -> {
                        addChannelConstraints.visibility = View.GONE
                        editPlanCostTi.visibility = View.VISIBLE
                        editPlanGstTi.visibility = View.VISIBLE
                    }

                    CommonEnumUtils.BOUQ.toString() -> {
                        addChannelConstraints.visibility = View.VISIBLE
                        editPlanCostTi.visibility = View.VISIBLE
                        editPlanGstTi.visibility = View.VISIBLE
                    }
                }
            }


        }
    }

    override fun setAppBarTitle(title: Int) {

    }

    private fun recyclerViewInit() {
        val mLayoutManager = GridLayoutManager(activity!!, 2)
        editPlanDetailsAddChannelRcv?.layoutManager = mLayoutManager
        editPlanDetailsAddChannelRcv.addItemDecoration(RecyclerViewItemDecorator(1))
        //editPlanDetailsAddChannelRcv?.itemAnimator = DefaultItemAnimator()
    }

    private fun planButtonSetOnClickListener() {
        editPlanEditTv.setOnClickListener {
            when (editAndCancelBool) {
                true -> {
                    editPlanEditTv.text = (resources.getString(R.string.Cancel))
                    setPlanEnable()
                    editAndCancelBool = false
                }

                false -> {
                    editPlanEditTv.text = (resources.getString(R.string.Edit))
                    setPlanDisable()
                    editAndCancelBool = true
                }

            }
        }

        editPlanSaveBtn.setOnClickListener {
            updatePlanPresenter.validatePlanUpdateDetails()
        }
    }

    private fun editAddChannelSetOnClickListener() {
        editChannelEditTv.setOnClickListener {
            val args = Bundle()
            args.putString("plan_array_data", commonUtils.cutNull(plan_array_data))
            args.putString("plan_id", commonUtils.cutNull(planId))
            planBaseActivity.changeFragment(6, args)
        }
    }

    override fun onResume() {
        super.onResume()
        getAllBundleDatas()
        hitGetParticularPlanChannels()
    }

    private fun setPlanEnable() {
        editPlanNameEd.isEnabled = true
        editPlanCodeEd.isEnabled = true
        editPlanCostEd.isEnabled = true
        editPlanGstEd.isEnabled = true
        editPlanSaveBtn.visibility = View.VISIBLE
        planBaseActivity.setActionBarTitle(R.string.plan_update_plans)
        editPlanHintTv.text = resources.getText(R.string.plan_update_plans)

    }

    fun setPlanDisable() {
        editPlanNameEd.isEnabled = false
        editPlanCodeEd.isEnabled = false
        editPlanCostEd.isEnabled = false
        editPlanGstEd.isEnabled = false
        editPlanSaveBtn.visibility = View.GONE
        planBaseActivity.setActionBarTitle(R.string.PlanDetails)
        editPlanHintTv.text = resources.getText(R.string.PlanDetails)
    }

    override fun onBackPressed(): Boolean {
        planBaseActivity.popBackFragment()
        return false
    }

    override fun onSuccessUpdatePlanResponse(updatePlanResponseModel: UpdatePlanResponseModel) {
        showToastLong(commonUtils.cutNull(updatePlanResponseModel.message))
        planBaseActivity.popBackFragment()
    }

    override fun onFailureUpdatePlanResponse(error: String) {

    }

    override fun getPlanId(): String {
        return planId!!
    }

    override fun getPlanName(): String {
        return editPlanNameEd.text.toString()
    }

    override fun getPlanCost(): String {
        return editPlanCostEd.text.toString()
    }

    override fun getPlanCode(): String {
        return editPlanCodeEd.text.toString()
    }


    override fun getPlanGst(): String {
        return editPlanGstEd.text.toString()
    }

    override fun getPlanTyp(): String {
        return planTag!!
    }

    override fun getPlanArrayData(): String {
        return ""
    }


    override fun getPlanIds(): String {
        return SingletonUtils.instance.planArrayData
    }

    override fun hitUpdatePlanCall() {
        progressLoadingBar()
        updatePlanPresenter.updatePlanApiCall()
    }

    override fun onSuccessGetParticularPlanAddChannelResponse(getPlanResponseModel: GetPlanResponseModel) {
        planGetAllChannelAdapter = PlanGetAllChannelAdapter(activity!!, getPlanResponseModel, object : OnListItemClickInterface {
            override fun OnSelectedItemClickListener(title: String, position: Int) {

            }
        }
        )
        editPlanDetailsAddChannelRcv?.adapter = planGetAllChannelAdapter
        planGetAllChannelAdapter!!.notifyDataSetChanged()
    }

    override fun onFailureGetParticularPlanAddChannelResponse(error: String) {

    }

    override fun hitGetParticularPlanChannels() {
        progressLoadingBar()
        getParticularPlanPresenter.getParticularPlanApiCall()
    }

}