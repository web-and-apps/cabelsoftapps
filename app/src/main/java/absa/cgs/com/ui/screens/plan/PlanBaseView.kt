package absa.cgs.com.ui.screens.plan

import android.os.Bundle
import androidx.fragment.app.Fragment

interface PlanBaseView {

    fun changeFragment(position: Int, bundle: Bundle)
    fun loadFragment(fragment: Fragment, bundle: Bundle)
    fun setActionBarTitle(string: Int)
    fun getIntentDatas()
    fun clearAllSingletonCustomerData()
}