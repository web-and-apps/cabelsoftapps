package absa.cgs.com.ui.screens.plan

import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.base.BaseActivity
import absa.cgs.com.ui.screens.plan.planchildfragment.addplan.AddPlanFragment
import absa.cgs.com.ui.screens.plan.planchildfragment.plandetails.PlanDetailsFragment
import absa.cgs.com.ui.screens.mainbaseactivity.mainchildfragments.plans.PlanFragment
import absa.cgs.com.ui.screens.plan.planchildfragment.addnewchannels.PlanAddNewChannelFragment
import absa.cgs.com.ui.screens.plan.planchildfragment.updatenewchannels.PlanUpdateNewChannelFragment
import absa.cgs.com.ui.screens.plan.planchildfragment.planupdate.PlanUpdateFragment
import absa.cgs.com.utils.SingletonUtils
import absa.cgs.com.utils.enums.CommonEnumUtils
import android.os.Bundle
import androidx.fragment.app.Fragment


class PlanBaseActivity : BaseActivity(), PlanBaseView {

    private val planDetailsFragment = PlanDetailsFragment()
    private val addPlanFragment = AddPlanFragment()
    private val planUpdateFragment = PlanUpdateFragment()
    private val planUpdateNewChannelFragment = PlanUpdateNewChannelFragment()
    private val planAddNewChannelFragment = PlanAddNewChannelFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_plan_base)

        init()
    }


    override fun init() {
        super.init()
        getIntentDatas()
        onClickListeners()
    }

    override fun onClickListeners() {
        super.onClickListeners()
    }

    override fun changeFragment(position: Int, bundle: Bundle) {
        when (position) {
            0 -> {
                setActionBarTitle(R.string.plan_free_plans)
                loadFragment(planDetailsFragment, bundle)
            }
            1 -> {
                setActionBarTitle(R.string.plan_paid_plans)
                loadFragment(planDetailsFragment, bundle)
            }
            2 -> {
                setActionBarTitle(R.string.plan_bouquets_plans)
                loadFragment(planDetailsFragment, bundle)
            }
            3 -> {
                setActionBarTitle(R.string.plan_add_plans)
                loadFragment(addPlanFragment, bundle)
            }

            4 -> {
                val planFragment: Fragment
                planFragment = PlanFragment()
                setActionBarTitle(R.string.bottom_nav_plans)
                loadFragment(planFragment, bundle)
            }
            5 -> {
                setActionBarTitle(R.string.plan_update_plans)
                loadFragment(planUpdateFragment, bundle)
            }
            6 -> {
                setActionBarTitle(R.string.AddChannel)
                loadFragment(planUpdateNewChannelFragment, bundle)
            }

            7 -> {
                setActionBarTitle(R.string.AddChannel)
                loadFragment(planAddNewChannelFragment, bundle)
            }
        }
    }

    override fun loadFragment(fragment: Fragment, bundle: Bundle) {
        val transaction = supportFragmentManager.beginTransaction()
        fragment.arguments = bundle
        transaction.replace(R.id.contentPlanFramelayout, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun setActionBarTitle(string: Int) {
        setTitle(string)
    }

    override fun getIntentDatas() {
        val intent = intent
        if (intent.extras != null) {
            if (intent.getStringExtra(CommonEnumUtils.FREE.toString()) == CommonEnumUtils.FREE.toString()) {
                val args = Bundle()
                args.putString(CommonEnumUtils.FREE.toString(), CommonEnumUtils.FREE.toString())
                changeFragment(0, args)
            } else if (intent.getStringExtra(CommonEnumUtils.PAID.toString()) == CommonEnumUtils.PAID.toString()) {
                val args = Bundle()
                args.putString(CommonEnumUtils.PAID.toString(), CommonEnumUtils.PAID.toString())
                changeFragment(1, args)
            } else if (intent.getStringExtra(CommonEnumUtils.BOUQ.toString()) == CommonEnumUtils.BOUQ.toString()) {
                val args = Bundle()
                args.putString(CommonEnumUtils.BOUQ.toString(), CommonEnumUtils.BOUQ.toString())
                changeFragment(2, args)
            } else if (intent.getStringExtra(CommonEnumUtils.ADDPLAN.toString()) == CommonEnumUtils.ADDPLAN.toString()) {
                val args = Bundle()
                changeFragment(3, args)
            }

        }
    }

    override fun onBackPressed() {
        val fragment = supportFragmentManager.findFragmentById(R.id.contentPlanFramelayout)
        if (fragment !is PlanBaseActivity.OnBackPressedListner || !(fragment as PlanBaseActivity.OnBackPressedListner).onBackPressed()) {

        }
    }

    interface OnBackPressedListner {
        fun onBackPressed(): Boolean
    }

    fun popBackFragment() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStackImmediate()
        } else {
            super.onBackPressed()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun clearAllSingletonCustomerData() {
        SingletonUtils.instance.clearSingletonCustomerDatas()
    }
}
