package absa.cgs.com.ui.screens.apis.getallcustomerapicall.model

data class GetAllCustomerResponseModel(var status_code: Int, var message: String, var customer_details: List<CustomerListDetails>)