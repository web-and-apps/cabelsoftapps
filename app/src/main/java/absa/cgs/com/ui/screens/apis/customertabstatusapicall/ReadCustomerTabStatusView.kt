package absa.cgs.com.ui.screens.apis.customertabstatusapicall

import absa.cgs.com.ui.screens.apis.customertabstatusapicall.model.CustomerTabStatusResponseModel
import absa.cgs.com.ui.screens.apis.readprofileapicall.model.ReadProfileResponseModel
import absa.cgs.com.ui.screens.base.BaseMvpView

interface ReadCustomerTabStatusView : BaseMvpView {
    fun onSuccessReadCustomerTabStatusResponse(customerTabStatusResponseModel: CustomerTabStatusResponseModel)
    fun onFailureReadCustomerTabStatusResponse(error: String)

    fun getCustomerId(): String

    fun hitGetCustomerTabStatusCall()
}
