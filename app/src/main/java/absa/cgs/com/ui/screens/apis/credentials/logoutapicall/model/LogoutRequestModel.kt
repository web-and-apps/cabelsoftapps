package absa.cgs.com.ui.screens.apis.credentials.logoutapicall.model

data class LogoutRequestModel(var user_id: String)