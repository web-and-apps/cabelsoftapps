package absa.cgs.com.ui.screens.apis.deletecustomerapicall

import absa.cgs.com.data.RetrofitClient
import absa.cgs.com.ui.screens.apis.deletecustomerapicall.model.DeleteCustomerRequestModel
import absa.cgs.com.ui.screens.apis.deletecustomerapicall.model.DeleteCustomerResponseModel
import absa.cgs.com.ui.screens.apis.deleteexpenseapicall.model.DeleteExpenseRequestModel
import absa.cgs.com.ui.screens.apis.deleteexpenseapicall.model.DeleteExpenseResponseModel
import absa.cgs.com.utils.enums.HttpEnum
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DeleteCustomerInteractor @Inject constructor() {
    private var deleteCustomerResponseModel: DeleteCustomerResponseModel? = null


    interface OnCallHitListener {
        fun onSuccessDeleteCustomerInteractListener(deleteCustomerResponseModel: DeleteCustomerResponseModel)
        fun onRetrofitFailureDeleteCustomerInteractListener(error: String)
        fun onSessionExpireDeleteCustomerInteractListener()
        fun onErrorDeleteCustomerInteractListener(deleteCustomerResponseModel: DeleteCustomerResponseModel)
        fun onServerExceptionDeleteCustomerInteractListener()
    }

    fun postDeleteExpenseDataToServer(deleteCustomerRequestModel: DeleteCustomerRequestModel, listener: OnCallHitListener) {
        RetrofitClient.instance.deleteCustomerData(deleteCustomerRequestModel)
                .enqueue(object : Callback<DeleteCustomerResponseModel> {
                    override fun onFailure(call: Call<DeleteCustomerResponseModel>, t: Throwable) {
                        listener.onRetrofitFailureDeleteCustomerInteractListener(t.message.toString())
                    }

                    override fun onResponse(call: Call<DeleteCustomerResponseModel>, response: Response<DeleteCustomerResponseModel>) {


                        when (response.code()) {
                            HttpEnum.STATUS_UNAUTHORIZED.code -> {
                                listener.onSessionExpireDeleteCustomerInteractListener()
                            }

                            HttpEnum.STATUS_ERROR.code -> {
                                deleteCustomerResponseModel = response.body()
                                listener.onErrorDeleteCustomerInteractListener(deleteCustomerResponseModel!!)
                            }

                            HttpEnum.STATUS_OK.code -> {
                                deleteCustomerResponseModel = response.body()
                                listener.onSuccessDeleteCustomerInteractListener(deleteCustomerResponseModel!!)
                            }

                            else -> {
                                listener.onServerExceptionDeleteCustomerInteractListener()
                            }
                        }


                    }

                })
    }
}