package absa.cgs.com.ui.screens.apis.deletecustomerapicall

import absa.cgs.com.ui.screens.base.BaseMvpView

interface DeleteCustomerView : BaseMvpView {

    fun onSuccessDeleteCustomerResponse()
    fun onFailureDeleteCustomerResponse(error: String)
    fun hitCustomerDetailsCall()


    fun getCustomerID(): String
}