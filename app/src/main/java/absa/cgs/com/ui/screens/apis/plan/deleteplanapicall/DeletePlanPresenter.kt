package absa.cgs.com.ui.screens.apis.plan.deleteplanapicall

import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.addexpenseapicall.model.AddExpenseRequestModel
import absa.cgs.com.ui.screens.apis.addexpenseapicall.model.AddExpenseResponseModel
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateARequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateAResponseModel
import absa.cgs.com.ui.screens.apis.plan.addplanapicall.model.AddPlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.addplanapicall.model.AddPlanResponseModel
import absa.cgs.com.ui.screens.apis.plan.deleteplanapicall.model.DeletePlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.deleteplanapicall.model.DeletePlanResponseModel
import absa.cgs.com.ui.screens.apis.plan.updateplanapicall.model.UpdatePlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.updateplanapicall.model.UpdatePlanResponseModel
import absa.cgs.com.ui.screens.authentication.AuthenticationBaseActivity
import absa.cgs.com.ui.screens.base.BasePresenter
import absa.cgs.com.utils.CommonUtils
import javax.inject.Inject

class DeletePlanPresenter<View : DeletePlanView> @Inject constructor(var deletePlanInteractor: DeletePlanInteractor, private val commonUtils: CommonUtils) : BasePresenter<View>(), IDeletePlanListener<View>, DeletePlanInteractor.OnCallHitListener {


    override fun deletePlanApiCall() {
        var deletePlanRequestModel = DeletePlanRequestModel(getBaseMvpVieww().getPlanId())
        deletePlanInteractor.deletePlanDataToServer(deletePlanRequestModel, this)
    }

    override fun onSuccessDeletePlanInteractListener(deletePlanResponseModel: DeletePlanResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().onSuccessDeletePlanResponse(deletePlanResponseModel)
        getBaseMvpVieww().showToastLong(commonUtils.cutNull(deletePlanResponseModel.message))
    }

    override fun onRetrofitFailureDeletePlanInteractListener(error: String) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(error)
    }

    override fun onSessionExpireDeletePlanInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        activity!!.finishAffinity()
        getBaseMvpVieww().navigationRoutes(AuthenticationBaseActivity::class.java)
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.SessionTokenExpire))
    }

    override fun onErrorDeletePlanInteractListener(deletePlanResponseModel: DeletePlanResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(deletePlanResponseModel.message)
    }

    override fun onServerExceptionDeletePlanInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.ServerBusyString))
    }


}