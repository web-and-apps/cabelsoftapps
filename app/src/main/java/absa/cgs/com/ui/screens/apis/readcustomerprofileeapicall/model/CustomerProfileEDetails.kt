package absa.cgs.com.ui.screens.apis.readcustomerprofileeapicall.model

data class CustomerProfileEDetails(var customer_e_id: String, var customer_id: String, var user_id: String,
                                   var customer_e_connection_date: String, var customer_e_register_date: String,
                                   var customer_e_date: String)