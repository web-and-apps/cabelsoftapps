package absa.cgs.com.ui.screens.apis.customertabstatusapicall.model

data class CustomerTabStatusRequestModel(var customer_id: String)