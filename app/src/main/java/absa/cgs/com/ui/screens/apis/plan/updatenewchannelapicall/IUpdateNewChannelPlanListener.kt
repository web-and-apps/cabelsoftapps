package absa.cgs.com.ui.screens.apis.plan.updatenewchannelapicall

import absa.cgs.com.ui.screens.base.BaseMvpPresenter

interface IUpdateNewChannelPlanListener<View : UpdateNewChannelPlanView> : BaseMvpPresenter<View> {
    fun updatePlanApiCall()
}