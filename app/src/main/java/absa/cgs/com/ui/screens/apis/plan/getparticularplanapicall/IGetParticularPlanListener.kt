package absa.cgs.com.ui.screens.apis.plan.getparticularplanapicall

import absa.cgs.com.ui.screens.base.BaseMvpPresenter

interface IGetParticularPlanListener<View : GetParticularPlanView> : BaseMvpPresenter<View> {
    fun getParticularPlanApiCall()

}