package absa.cgs.com.ui.screens.apis.deletecustomerapicall

import absa.cgs.com.ui.screens.base.BaseMvpPresenter

interface IDeleteCustomerListener<View : DeleteCustomerView> : BaseMvpPresenter<View> {
    fun postDeleteCustomerApiCall()
}