package aCsa.cgs.com.ui.screens.apis.addupdateprofilecapicall

import absa.cgs.com.data.RetrofitClient

import absa.cgs.com.ui.screens.apis.addupdateprofilecapicall.model.CustomerAddUpdateCRequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofilecapicall.model.CustomerAddUpdateCResponseModel
import absa.cgs.com.ui.screens.apis.addupdateprofiledapicall.model.CustomerAddUpdateDRequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofiledapicall.model.CustomerAddUpdateDResponseModel
import absa.cgs.com.ui.screens.apis.addupdateprofileeapicall.model.CustomerAddUpdateERequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofileeapicall.model.CustomerAddUpdateEResponseModel
import absa.cgs.com.utils.enums.HttpEnum
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AddUpdateCustomerEInteractor @Inject constructor() {
    private var customerAddUpdateEResponseModel: CustomerAddUpdateEResponseModel? = null


    interface OnCallHitListener {
        fun onSuccessAddUpdateCustomerEInteractListener(customerAddUpdateEResponseModel: CustomerAddUpdateEResponseModel)
        fun onRetrofitFailureAddUpdateCustomerEInteractListener(error: String)
        fun onSessionExpireAddUpdateCustomerEInteractListener()
        fun onErrorAddUpdateCustomerEInteractListener(customerAddUpdateEResponseModel: CustomerAddUpdateEResponseModel)
        fun onServerExceptionAddUpdateCustomerEInteractListener()
    }

    fun postAddUpdateCustomerDDataToServer(customerAddUpdateERequestModel: CustomerAddUpdateERequestModel, listener: OnCallHitListener) {
        RetrofitClient.instance.addUpdateCustomerE(customerAddUpdateERequestModel)
                .enqueue(object : Callback<CustomerAddUpdateEResponseModel> {
                    override fun onFailure(call: Call<CustomerAddUpdateEResponseModel>, t: Throwable) {
                        listener.onRetrofitFailureAddUpdateCustomerEInteractListener(t.message.toString())
                    }

                    override fun onResponse(call: Call<CustomerAddUpdateEResponseModel>, response: Response<CustomerAddUpdateEResponseModel>) {

                        when (response.code()) {
                            HttpEnum.STATUS_UNAUTHORIZED.code -> {
                                listener.onSessionExpireAddUpdateCustomerEInteractListener()
                            }

                            HttpEnum.STATUS_ERROR.code -> {
                                customerAddUpdateEResponseModel = response.body()
                                listener.onErrorAddUpdateCustomerEInteractListener(customerAddUpdateEResponseModel!!)
                            }

                            HttpEnum.STATUS_OK.code -> {
                                customerAddUpdateEResponseModel = response.body()
                                listener.onSuccessAddUpdateCustomerEInteractListener(customerAddUpdateEResponseModel!!)
                            }

                            else -> {
                                listener.onServerExceptionAddUpdateCustomerEInteractListener()
                            }
                        }

                    }

                })
    }
}