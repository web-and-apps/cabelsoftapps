package absa.cgs.com.ui.screens.apis.updateimagesapicall.model

data class UpdateImageRequestModel(var user_id: String, var user_profile_img: String, var image_tag: String)