package absa.cgs.com.ui.screens.apis.readcustomerprofilecapicall.model

data class CustomerProfileCDetails(var customer_c_id: String, var customer_id: String, var user_id: String,
                                   var customer_c_bill_type: String, var customer_c_bill_time: String,
                                   var customer_c_additional_charge: String, var customer_c_date: String)