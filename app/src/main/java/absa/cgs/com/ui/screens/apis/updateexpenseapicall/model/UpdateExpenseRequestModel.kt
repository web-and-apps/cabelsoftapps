package absa.cgs.com.ui.screens.apis.updateexpenseapicall.model

data class UpdateExpenseRequestModel(var expense_id: String, var expense_amount: String, var expense_comment: String, var expense_type: String, var expense_user_date: String)