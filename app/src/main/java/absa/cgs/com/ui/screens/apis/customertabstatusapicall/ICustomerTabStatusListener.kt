package absa.cgs.com.ui.screens.apis.customertabstatusapicall

import absa.cgs.com.ui.screens.base.BaseMvpPresenter


interface ICustomerTabStatusListener<View : ReadCustomerTabStatusView> : BaseMvpPresenter<View> {

    fun readCustomerTabStatusData()

}
