package absa.cgs.com.ui.screens.apis.customertabstatusapicall.model

data class CustomerTabStatusResponseModel(var status_code: Int, var message: String, var customer_profile_status: CustomerProfileStatus)