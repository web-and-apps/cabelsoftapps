package absa.cgs.com.ui.screens.apis.plan.addplanapicall

import absa.cgs.com.data.RetrofitClient
import absa.cgs.com.ui.screens.apis.addexpenseapicall.model.AddExpenseRequestModel
import absa.cgs.com.ui.screens.apis.addexpenseapicall.model.AddExpenseResponseModel
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateARequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateAResponseModel
import absa.cgs.com.ui.screens.apis.plan.addplanapicall.model.AddPlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.addplanapicall.model.AddPlanResponseModel
import absa.cgs.com.utils.enums.HttpEnum
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AddPlanInteractor @Inject constructor() {
    private var addPlanResponseModel: AddPlanResponseModel? = null


    interface OnCallHitListener {
        fun onSuccessAddPlanInteractListener(addPlanResponseModel: AddPlanResponseModel)
        fun onRetrofitFailureAddPlanInteractListener(error: String)
        fun onSessionExpireAddPlanInteractListener()
        fun onErrorAddPlanInteractListener(addPlanResponseModel: AddPlanResponseModel)
        fun onServerExceptionAddPlanInteractListener()
    }

    fun postAddPlanDataToServer(addPlanRequestModel: AddPlanRequestModel, listener: OnCallHitListener) {
        RetrofitClient.instance.addNewPlanDetails(addPlanRequestModel)
                .enqueue(object : Callback<AddPlanResponseModel> {
                    override fun onFailure(call: Call<AddPlanResponseModel>, t: Throwable) {
                        listener.onRetrofitFailureAddPlanInteractListener(t.message.toString())
                    }

                    override fun onResponse(call: Call<AddPlanResponseModel>, response: Response<AddPlanResponseModel>) {

                        when (response.code()) {
                            HttpEnum.STATUS_UNAUTHORIZED.code -> {
                                listener.onSessionExpireAddPlanInteractListener()
                            }

                            HttpEnum.STATUS_ERROR.code -> {
                                addPlanResponseModel = response.body()
                                listener.onErrorAddPlanInteractListener(addPlanResponseModel!!)
                            }

                            HttpEnum.STATUS_OK.code -> {
                                addPlanResponseModel = response.body()
                                listener.onSuccessAddPlanInteractListener(addPlanResponseModel!!)
                            }

                            else -> {
                                listener.onServerExceptionAddPlanInteractListener()
                            }
                        }

                    }

                })
    }
}