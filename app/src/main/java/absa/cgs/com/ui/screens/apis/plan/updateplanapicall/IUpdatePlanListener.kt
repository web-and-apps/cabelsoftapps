package absa.cgs.com.ui.screens.apis.plan.updateplanapicall

import absa.cgs.com.ui.screens.base.BaseMvpPresenter

interface IUpdatePlanListener<View : UpdatePlanView> : BaseMvpPresenter<View> {
    fun updatePlanApiCall()
    fun validatePlanUpdateDetails()
}