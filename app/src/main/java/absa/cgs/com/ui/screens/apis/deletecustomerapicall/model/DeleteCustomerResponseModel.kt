package absa.cgs.com.ui.screens.apis.deletecustomerapicall.model

data class DeleteCustomerResponseModel(var status_code: Int, var message: String)