package absa.cgs.com.ui.screens.apis.addexpenseapicall.model

data class AddExpenseResponseModel(var status_code: Int, var message: String)