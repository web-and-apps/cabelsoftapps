package absa.cgs.com.ui.screens.apis.readprofileapicall.model

data class ReadProfileRequestModel(val user_id: String)