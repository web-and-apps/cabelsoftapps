package absa.cgs.com.ui.screens.apis.plan.deleteplanapicall.model

data class DeletePlanRequestModel(var plan_id: String)