package absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall

import absa.cgs.com.data.RetrofitClient
import absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.model.ReadCustomerProfileARequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.model.ReadCustomerProfileAResponseModel
import absa.cgs.com.ui.screens.apis.readprofileapicall.model.ReadProfileRequestModel
import absa.cgs.com.ui.screens.apis.readprofileapicall.model.ReadProfileResponseModel
import absa.cgs.com.utils.enums.HttpEnum
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ReadCustomerProfileAInteractor @Inject constructor() {

    private var readCustomerProfileAResponseModel: ReadCustomerProfileAResponseModel? = null


    interface OnCallHitListener {
        fun onSuccessReadCustomerAProfileInteractListener(readCustomerProfileAResponseModel: ReadCustomerProfileAResponseModel)
        fun onRetrofitFailureReadCustomerAProfileInteractListener(error: String)
        fun onSessionExpireReadCustomerAProfileInteractListener()
        fun onErrorReadCustomerAProfileInteractListener(readCustomerProfileAResponseModel: ReadCustomerProfileAResponseModel)
        fun onServerExceptionReadCustomerAProfileInteractListener()
    }


    fun getReadCustomerProfileADataToServer(readCustomerProfileARequestModel: ReadCustomerProfileARequestModel, listener: OnCallHitListener) {
        RetrofitClient.instance.readCustomerProfileAData(readCustomerProfileARequestModel)
                .enqueue(object : Callback<ReadCustomerProfileAResponseModel> {
                    override fun onFailure(call: Call<ReadCustomerProfileAResponseModel>, t: Throwable) {
                        listener.onRetrofitFailureReadCustomerAProfileInteractListener(t.message.toString())
                    }

                    override fun onResponse(call: Call<ReadCustomerProfileAResponseModel>, response: Response<ReadCustomerProfileAResponseModel>) {
                        when (response.code()) {
                            HttpEnum.STATUS_UNAUTHORIZED.code -> {
                                listener.onSessionExpireReadCustomerAProfileInteractListener()
                            }

                            HttpEnum.STATUS_ERROR.code -> {
                                readCustomerProfileAResponseModel = response.body()
                                listener.onErrorReadCustomerAProfileInteractListener(readCustomerProfileAResponseModel!!)
                            }

                            HttpEnum.STATUS_OK.code -> {
                                readCustomerProfileAResponseModel = response.body()
                                listener.onSuccessReadCustomerAProfileInteractListener(readCustomerProfileAResponseModel!!)
                            }

                            else -> {
                                listener.onServerExceptionReadCustomerAProfileInteractListener()
                            }
                        }

                    }

                })
    }

}
