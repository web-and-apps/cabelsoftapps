package absa.cgs.com.ui.screens.apis.readcustomerprofiledapicall

import absa.cgs.com.data.RetrofitClient
import absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.model.ReadCustomerProfileARequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.model.ReadCustomerProfileAResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilebapicall.model.ReadCustomerProfileBRequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilebapicall.model.ReadCustomerProfileBResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilecapicall.model.ReadCustomerProfileCRequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilecapicall.model.ReadCustomerProfileCResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofiledapicall.model.ReadCustomerProfileDRequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofiledapicall.model.ReadCustomerProfileDResponseModel
import absa.cgs.com.ui.screens.apis.readprofileapicall.model.ReadProfileRequestModel
import absa.cgs.com.ui.screens.apis.readprofileapicall.model.ReadProfileResponseModel
import absa.cgs.com.utils.enums.HttpEnum
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ReadCustomerProfileDInteractor @Inject constructor() {

    private var readCustomerProfileDResponseModel: ReadCustomerProfileDResponseModel? = null


    interface OnCallHitListener {
        fun onSuccessReadCustomerDProfileInteractListener(readCustomerProfileDResponseModel: ReadCustomerProfileDResponseModel)
        fun onRetrofitFailureReadCustomerDProfileInteractListener(error: String)
        fun onSessionExpireReadCustomerDProfileInteractListener()
        fun onErrorReadCustomerDProfileInteractListener(readCustomerProfileDResponseModel: ReadCustomerProfileDResponseModel)
        fun onServerExceptionReadCustomerDProfileInteractListener()
    }


    fun getReadCustomerProfileDDataToServer(readCustomerProfileDRequestModel: ReadCustomerProfileDRequestModel, listener: OnCallHitListener) {
        RetrofitClient.instance.readCustomerProfileDData(readCustomerProfileDRequestModel)
                .enqueue(object : Callback<ReadCustomerProfileDResponseModel> {
                    override fun onFailure(call: Call<ReadCustomerProfileDResponseModel>, t: Throwable) {
                        listener.onRetrofitFailureReadCustomerDProfileInteractListener(t.message.toString())
                    }

                    override fun onResponse(call: Call<ReadCustomerProfileDResponseModel>, response: Response<ReadCustomerProfileDResponseModel>) {
                        when (response.code()) {
                            HttpEnum.STATUS_UNAUTHORIZED.code -> {
                                listener.onSessionExpireReadCustomerDProfileInteractListener()
                            }

                            HttpEnum.STATUS_ERROR.code -> {
                                readCustomerProfileDResponseModel = response.body()
                                listener.onErrorReadCustomerDProfileInteractListener(readCustomerProfileDResponseModel!!)
                            }

                            HttpEnum.STATUS_OK.code -> {
                                readCustomerProfileDResponseModel = response.body()
                                listener.onSuccessReadCustomerDProfileInteractListener(readCustomerProfileDResponseModel!!)
                            }

                            else -> {
                                listener.onServerExceptionReadCustomerDProfileInteractListener()
                            }
                        }

                    }

                })
    }

}
