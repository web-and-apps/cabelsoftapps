package absa.cgs.com.ui.screens.apis.addupdateprofiledapicall

import aCsa.cgs.com.ui.screens.apis.addupdateprofilecapicall.AddUpdateCustomerCInteractor
import aCsa.cgs.com.ui.screens.apis.addupdateprofilecapicall.AddUpdateCustomerDInteractor
import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.addupdateprofilecapicall.model.CustomerAddUpdateCRequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofilecapicall.model.CustomerAddUpdateCResponseModel
import absa.cgs.com.ui.screens.apis.addupdateprofiledapicall.model.CustomerAddUpdateDRequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofiledapicall.model.CustomerAddUpdateDResponseModel
import absa.cgs.com.ui.screens.authentication.AuthenticationBaseActivity
import absa.cgs.com.ui.screens.base.BasePresenter
import absa.cgs.com.utils.CommonUtils
import javax.inject.Inject

class AddUpdateCustomerDPresenter<View : AddUodateCustomerDView> @Inject constructor(var addUpdateCustomerDInteractor: AddUpdateCustomerDInteractor, private val commonUtils: CommonUtils) : BasePresenter<View>(), IAddUpdateCustomerDListener<View>, AddUpdateCustomerDInteractor.OnCallHitListener {


    override fun postCustomerDApiCall() {
        var customerAddUpdateDRequestModel = CustomerAddUpdateDRequestModel(getBaseMvpVieww().getUserId(), getBaseMvpVieww().getCustomerId(), getBaseMvpVieww().getCustomerDId(), getBaseMvpVieww().getCustomerCSettopNo(), getBaseMvpVieww().getCustomeCSettopName(), getBaseMvpVieww().getCustomerCSettopType(), getBaseMvpVieww().getCustomerCSmartCardNo())
        addUpdateCustomerDInteractor.postAddUpdateCustomerDDataToServer(customerAddUpdateDRequestModel, this)
    }

    override fun onSuccessAddUpdateCustomerDInteractListener(customerAddUpdateDResponseModel: CustomerAddUpdateDResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().onSuccessAddUpdateCustomerDResponse(customerAddUpdateDResponseModel)
        getBaseMvpVieww().showToastLong(commonUtils.cutNull(customerAddUpdateDResponseModel.message))
    }

    override fun onRetrofitFailureAddUpdateCustomerDInteractListener(error: String) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(error)
    }

    override fun onSessionExpireAddUpdateCustomerDInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        activity!!.finishAffinity()
        getBaseMvpVieww().navigationRoutes(AuthenticationBaseActivity::class.java)
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.SessionTokenExpire))
    }

    override fun onErrorAddUpdateCustomerDInteractListener(customerAddUpdateDResponseModel: CustomerAddUpdateDResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(customerAddUpdateDResponseModel.message)
    }

    override fun onServerExceptionAddUpdateCustomerDInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.ServerBusyString))
    }

    override fun validateAddUpdateCustomerD() {
        if (getBaseMvpVieww().getCustomerCSettopNo().equals("")) {
            getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.CustomerNameAString))
        } else if (getBaseMvpVieww().getCustomeCSettopName().equals("")) {
            getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.CustomerAreaAString))
        } else if (getBaseMvpVieww().getCustomerCSettopType().equals("")) {
            getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.CustomerStreetAString))
        } else if (getBaseMvpVieww().getCustomerCSmartCardNo().equals("")) {
            getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.CustomerStreetAString))
        } else {
            getBaseMvpVieww().postAddUpdateCustomerD()
        }
    }
}