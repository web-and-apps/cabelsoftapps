package absa.cgs.com.ui.screens.apis.plan.getparticularplanapicall

import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.addexpenseapicall.model.AddExpenseRequestModel
import absa.cgs.com.ui.screens.apis.addexpenseapicall.model.AddExpenseResponseModel
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateARequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateAResponseModel
import absa.cgs.com.ui.screens.apis.plan.addplanapicall.model.AddPlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.addplanapicall.model.AddPlanResponseModel
import absa.cgs.com.ui.screens.apis.plan.getparticularplanapicall.model.GetParticularPlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.getparticularplanapicall.model.GetPlanResponseModel
import absa.cgs.com.ui.screens.apis.plan.updateplanapicall.model.UpdatePlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.updateplanapicall.model.UpdatePlanResponseModel
import absa.cgs.com.ui.screens.authentication.AuthenticationBaseActivity
import absa.cgs.com.ui.screens.base.BasePresenter
import absa.cgs.com.utils.CommonUtils
import javax.inject.Inject

class GetParticularPlanPresenter<View : GetParticularPlanView> @Inject constructor(var getParticularPlanInteractor: GetParticularPlanInteractor, private val commonUtils: CommonUtils) : BasePresenter<View>(), IGetParticularPlanListener<View>, GetParticularPlanInteractor.OnCallHitListener {


    override fun getParticularPlanApiCall() {
        var getParticularPlanRequestModel = GetParticularPlanRequestModel(getBaseMvpVieww().getPlanIds())
        getParticularPlanInteractor.getParticularPlanDataToServer(getParticularPlanRequestModel, this)
    }

    override fun onSuccessGetParticularPlanInteractListener(getPlanResponseModel: GetPlanResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().onSuccessGetParticularPlanAddChannelResponse(getPlanResponseModel)
    }

    override fun onRetrofitFailureGetParticularPlanInteractListener(error: String) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(error)
    }

    override fun onSessionExpireGetParticularPlanInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        activity!!.finishAffinity()
        getBaseMvpVieww().navigationRoutes(AuthenticationBaseActivity::class.java)
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.SessionTokenExpire))
    }

    override fun onErrorGetParticularPlanInteractListener(getPlanResponseModel: GetPlanResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
    }

    override fun onServerExceptionGetParticularPlanInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.ServerBusyString))
    }


}