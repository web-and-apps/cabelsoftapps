package absa.cgs.com.ui.screens.apis.addupdateprofiledapicall

import absa.cgs.com.ui.screens.apis.addupdateprofiledapicall.model.CustomerAddUpdateDResponseModel
import absa.cgs.com.ui.screens.base.BaseMvpView

interface AddUodateCustomerDView : BaseMvpView {

    fun onSuccessAddUpdateCustomerDResponse(customerAddUpdateDResponseModel: CustomerAddUpdateDResponseModel)
    fun onFailureAddUpdateCustomerDResponse(error: String)

    fun getUserId(): String
    fun getCustomerId(): String
    fun getCustomerDId(): String
    fun getCustomerCSettopNo(): String
    fun getCustomeCSettopName(): String
    fun getCustomerCSettopType(): String
    fun getCustomerCSmartCardNo(): String
    fun postAddUpdateCustomerD()


}