package absa.cgs.com.ui.screens.apis.readprofileapicall.model

data class ReadNomineeDetailsModel(val nominee_id: String, val nominee_name: String,
                                   val nominee_phone_number: String, val nominee_address: String,
                                   val nominee_relation_nominee: String, val nominee_created: String)