package absa.cgs.com.ui.screens.apis.plan.getplanapicall

import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.addexpenseapicall.model.AddExpenseRequestModel
import absa.cgs.com.ui.screens.apis.addexpenseapicall.model.AddExpenseResponseModel
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateARequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateAResponseModel
import absa.cgs.com.ui.screens.apis.plan.addplanapicall.model.AddPlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.addplanapicall.model.AddPlanResponseModel
import absa.cgs.com.ui.screens.apis.plan.getparticularplanapicall.model.GetParticularPlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.getparticularplanapicall.model.GetPlanResponseModel
import absa.cgs.com.ui.screens.apis.plan.getplanapicall.model.GetPlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.updateplanapicall.model.UpdatePlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.updateplanapicall.model.UpdatePlanResponseModel
import absa.cgs.com.ui.screens.authentication.AuthenticationBaseActivity
import absa.cgs.com.ui.screens.base.BasePresenter
import absa.cgs.com.utils.CommonUtils
import javax.inject.Inject

class GetPlanPresenter<View : GetPlanView> @Inject constructor(var getParticularPlanInteractor: GetPlanInteractor, private val commonUtils: CommonUtils) : BasePresenter<View>(), IGetPlanListener<View>, GetPlanInteractor.OnCallHitListener {


    override fun getPlanApiCall() {
        var getPlanRequestModel = GetPlanRequestModel(getBaseMvpVieww().getFromDate(), getBaseMvpVieww().getToDate(), getBaseMvpVieww().getPageCount(), getBaseMvpVieww().getSearchKeyword(), getBaseMvpVieww().getSearchTag())
        getParticularPlanInteractor.getPlanDataToServer(getPlanRequestModel, this)
    }

    override fun onSuccessGetPlanInteractListener(getPlanResponseModel: GetPlanResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().onSuccessGetParticularPlanResponse(getPlanResponseModel)
    }

    override fun onRetrofitFailureGetPlanInteractListener(error: String) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(error)
    }

    override fun onSessionExpireGetPlanInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        activity!!.finishAffinity()
        getBaseMvpVieww().navigationRoutes(AuthenticationBaseActivity::class.java)
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.SessionTokenExpire))
    }

    override fun onErrorGetPlanInteractListener(getPlanResponseModel: GetPlanResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(getPlanResponseModel.message)
    }

    override fun onServerExceptionGetPlanInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.ServerBusyString))
    }


}