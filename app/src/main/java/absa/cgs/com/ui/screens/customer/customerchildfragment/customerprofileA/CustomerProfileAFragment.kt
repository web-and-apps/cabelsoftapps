package absa.cgs.com.ui.screens.customer.customerchildfragment.customerprofileA

import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.AddUodateCustomerAView
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.AddUpdateCustomerAPresenter
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateAResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.ReadCustomerProfileAPresenter
import absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.ReadCustomerProfileAView
import absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.model.ReadCustomerProfileAResponseModel
import absa.cgs.com.ui.screens.base.BaseFragment
import absa.cgs.com.ui.screens.customer.CustomerBaseActivity
import absa.cgs.com.utils.SingletonUtils
import absa.cgs.com.utils.enums.CommonEnumUtils
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.customerprofileafragment.*
import javax.inject.Inject

class CustomerProfileAFragment : BaseFragment(), CustomerBaseActivity.OnBackPressedListner, ReadCustomerProfileAView, AddUodateCustomerAView {


    @Inject
    lateinit var readCustomerProfileAPresenter: ReadCustomerProfileAPresenter<ReadCustomerProfileAView>

    @Inject
    lateinit var addUpdateCustomerAPresenter: AddUpdateCustomerAPresenter<AddUodateCustomerAView>

    lateinit var customerBaseActivity: CustomerBaseActivity

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        fragmentComponent().inject(this)
        readCustomerProfileAPresenter.attachView(activity!!, this)
        addUpdateCustomerAPresenter.attachView(activity!!, this)

    }


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.customerprofileafragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()

    }

    override fun init() {
        customerBaseActivity = activity as CustomerBaseActivity
        onClickListeners()
    }

    override fun onClickListeners() {
        super.onClickListeners()
        CustomerProfileAUpdateBtn.setOnClickListener {
            addUpdateCustomerAPresenter.validateAddUpdateCustomerA()
        }
    }

    override fun onBackPressed(): Boolean {
        activity!!.finish()
        return false
    }

    override fun onSuccessReadCustomerProfileAResponse(readCustomerProfileAResponseModel: ReadCustomerProfileAResponseModel) {
        SingletonUtils.instance.customerId = commonUtils.cutNull(readCustomerProfileAResponseModel.profile_a_details.customer_id)
        textedit_login_name.setText(commonUtils.cutNull(readCustomerProfileAResponseModel.profile_a_details.customer_name))
        texteditRegisterCsArea.setText(commonUtils.cutNull(readCustomerProfileAResponseModel.profile_a_details.customer_area))
        texteditRegisterCsStreet.setText(commonUtils.cutNull(readCustomerProfileAResponseModel.profile_a_details.customer_street))
        texteditLoginHome.setText(commonUtils.cutNull(readCustomerProfileAResponseModel.profile_a_details.customer_home_type))
        texteditLoginPhoneNumber.setText(commonUtils.cutNull(readCustomerProfileAResponseModel.profile_a_details.customer_mobile))
    }

    override fun onFailureReadCustomerProfileAResponse(error: String) {

    }

    override fun getCustomerID(): String {
        return SingletonUtils.instance.customerId
    }

    override fun hitGetCustomerProfileACall() {
        progressLoadingBar()
        readCustomerProfileAPresenter.readCustomerProfileAData()
    }

    override fun onResume() {
        super.onResume()
        if (!commonUtils.cutNull(SingletonUtils.instance.customerId).isNullOrEmpty()) {
            hitGetCustomerProfileACall()
        }
    }

    override fun onSuccessAddUpdateCustomerAResponse(customerAddUpdateAResponseModel: CustomerAddUpdateAResponseModel) {
        when (commonUtils.cutNull(customerAddUpdateAResponseModel.customer_id)) {
            null -> {
            }
            "" -> {
            }
            else -> {
                SingletonUtils.instance.customerId = commonUtils.cutNull(customerAddUpdateAResponseModel.customer_id)
            }
        }

        when (commonUtils.cutNull(customerAddUpdateAResponseModel.operation)) {
            CommonEnumUtils.ADDED.toString() -> {
                customerBaseActivity.moveToProfileB()
                customerBaseActivity.hitGetCustomerTabStatusCall()
            }
            CommonEnumUtils.UPDATED.toString() -> {
                customerBaseActivity.moveToProfileB()
            }
        }

    }

    override fun onFailureAddUpdateCustomerAResponse(error: String) {

    }

    override fun getUserId(): String {
        return SingletonUtils.instance.userId
    }

    override fun getCustomerId(): String {
        return SingletonUtils.instance.customerId
    }

    override fun getCustomerName(): String {
        return textedit_login_name.text.toString()
    }

    override fun getCustomerArea(): String {
        return texteditRegisterCsArea.text.toString()
    }

    override fun getCustomerStreet(): String {
        return texteditRegisterCsStreet.text.toString()
    }

    override fun getCustomerHomeType(): String {
        return texteditLoginHome.text.toString()
    }

    override fun getCustomerMobile(): String {
        return texteditLoginPhoneNumber.text.toString()
    }

    override fun postAddUpdateCustomerA() {
        progressLoadingBar()
        addUpdateCustomerAPresenter.postCustomerAApiCall()

    }


}
