package absa.cgs.com.ui.screens.apis.plan.getparticularplanapicall.model

data class GetParticularPlanProductDetails(var plan_id: String, var plan_name: String, var plan_cost: String, var plan_code: String, var plan_type: String, var plan_gst: String, var plan_description: String, var plan_date: String, var plan_array_data: String, var selection: Boolean)