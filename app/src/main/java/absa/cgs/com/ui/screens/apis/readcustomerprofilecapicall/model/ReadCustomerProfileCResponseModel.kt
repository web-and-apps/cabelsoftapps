package absa.cgs.com.ui.screens.apis.readcustomerprofilecapicall.model

class ReadCustomerProfileCResponseModel(var status_code: Int, var message: String, var profile_c_details: CustomerProfileCDetails)