package absa.cgs.com.ui.screens.apis.addupdateprofileaapicall

import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateAResponseModel
import absa.cgs.com.ui.screens.base.BaseMvpView

interface AddUodateCustomerAView : BaseMvpView {

    fun onSuccessAddUpdateCustomerAResponse(customerAddUpdateAResponseModel: CustomerAddUpdateAResponseModel)
    fun onFailureAddUpdateCustomerAResponse(error: String)

    fun getUserId(): String
    fun getCustomerId(): String
    fun getCustomerName(): String
    fun getCustomerArea(): String
    fun getCustomerStreet(): String
    fun getCustomerHomeType(): String
    fun getCustomerMobile(): String
    fun postAddUpdateCustomerA()

}