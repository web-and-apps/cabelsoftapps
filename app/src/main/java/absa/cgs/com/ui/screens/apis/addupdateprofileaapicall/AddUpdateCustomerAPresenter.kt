package absa.cgs.com.ui.screens.apis.addupdateprofileaapicall

import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.addexpenseapicall.model.AddExpenseRequestModel
import absa.cgs.com.ui.screens.apis.addexpenseapicall.model.AddExpenseResponseModel
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateARequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateAResponseModel
import absa.cgs.com.ui.screens.authentication.AuthenticationBaseActivity
import absa.cgs.com.ui.screens.base.BasePresenter
import absa.cgs.com.utils.CommonUtils
import javax.inject.Inject

class AddUpdateCustomerAPresenter<View : AddUodateCustomerAView> @Inject constructor(var addUpdateCustomerAInteractor: AddUpdateCustomerAInteractor, private val commonUtils: CommonUtils) : BasePresenter<View>(), IAddUpdateCustomerAListener<View>, AddUpdateCustomerAInteractor.OnCallHitListener {


    override fun postCustomerAApiCall() {
        var customerAddUpdateARequestModel = CustomerAddUpdateARequestModel(getBaseMvpVieww().getUserId(), getBaseMvpVieww().getCustomerId(), getBaseMvpVieww().getCustomerName(), getBaseMvpVieww().getCustomerArea(), getBaseMvpVieww().getCustomerStreet(), getBaseMvpVieww().getCustomerHomeType(), getBaseMvpVieww().getCustomerMobile())
        addUpdateCustomerAInteractor.postAddUpdateCustomerADataToServer(customerAddUpdateARequestModel, this)
    }

    override fun onSuccessAddUpdateCustomerAInteractListener(customerAddUpdateAResponseModel: CustomerAddUpdateAResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().onSuccessAddUpdateCustomerAResponse(customerAddUpdateAResponseModel)
        getBaseMvpVieww().showToastLong(commonUtils.cutNull(customerAddUpdateAResponseModel.message))
    }

    override fun onRetrofitFailureAddUpdateCustomerAInteractListener(error: String) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(error)
    }

    override fun onSessionExpireAddUpdateCustomerAInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        activity!!.finishAffinity()
        getBaseMvpVieww().navigationRoutes(AuthenticationBaseActivity::class.java)
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.SessionTokenExpire))
    }

    override fun onErrorAddUpdateCustomerAInteractListener(customerAddUpdateAResponseModel: CustomerAddUpdateAResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(customerAddUpdateAResponseModel.message)
    }

    override fun onServerExceptionAddUpdateCustomerAInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.ServerBusyString))
    }

    override fun validateAddUpdateCustomerA() {
        if (getBaseMvpVieww().getCustomerName().equals("")) {
            getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.CustomerNameAString))
        } else if (getBaseMvpVieww().getCustomerArea().equals("")) {
            getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.CustomerAreaAString))
        } else if (getBaseMvpVieww().getCustomerStreet().equals("")) {
            getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.CustomerStreetAString))
        } else if (getBaseMvpVieww().getCustomerHomeType().equals("")) {
            getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.CustomerHomeTypeAString))
        } else if (getBaseMvpVieww().getCustomerMobile().equals("")) {
            getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.CustomerPrimaryNoAString))
        } else {
            getBaseMvpVieww().postAddUpdateCustomerA()
        }
    }
}