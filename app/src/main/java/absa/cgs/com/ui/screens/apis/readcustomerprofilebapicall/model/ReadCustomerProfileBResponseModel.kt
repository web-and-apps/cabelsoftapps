package absa.cgs.com.ui.screens.apis.readcustomerprofilebapicall.model

class ReadCustomerProfileBResponseModel(var status_code: Int, var message: String, var profile_b_details: CustomerProfileBDetails)