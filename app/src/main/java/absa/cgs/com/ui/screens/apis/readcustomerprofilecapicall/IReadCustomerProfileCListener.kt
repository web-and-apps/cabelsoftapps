package absa.cgs.com.ui.screens.apis.readcustomerprofilecapicall

import absa.cgs.com.ui.screens.base.BaseMvpPresenter


interface IReadCustomerProfileCListener<View : ReadCustomerProfileCView> : BaseMvpPresenter<View> {

    fun readCustomerProfileCData()

}
