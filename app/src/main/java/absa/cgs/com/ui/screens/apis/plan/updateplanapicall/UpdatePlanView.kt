package absa.cgs.com.ui.screens.apis.plan.updateplanapicall

import absa.cgs.com.ui.screens.apis.plan.updateplanapicall.model.UpdatePlanResponseModel
import absa.cgs.com.ui.screens.base.BaseMvpView

interface UpdatePlanView : BaseMvpView {

    fun onSuccessUpdatePlanResponse(updatePlanResponseModel: UpdatePlanResponseModel)
    fun onFailureUpdatePlanResponse(error: String)

    fun getPlanId(): String
    fun getPlanName(): String
    fun getPlanCost(): String
    fun getPlanCode(): String
    fun getPlanGst(): String
    fun getPlanTyp(): String
    fun getPlanArrayData(): String
    fun hitUpdatePlanCall()

}