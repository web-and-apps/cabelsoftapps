package absa.cgs.com.ui.screens.apis.readprofileapicall.model

data class ReadProfileDetailModel(val user_id: String, val user_name: String,
                                  val user_mailid: String, val user_mobile_number: String,
                                  val user_address: String, val user_profile_img: String,
                                  var user_banner_img: String,
                                  val user_office_number: String, val user_ower_name: String,
                                  val user_agency_name: String, val user_gst_number: String)