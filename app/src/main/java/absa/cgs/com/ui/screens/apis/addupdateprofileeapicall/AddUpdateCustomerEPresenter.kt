package absa.cgs.com.ui.screens.apis.addupdateprofileeapicall

import aCsa.cgs.com.ui.screens.apis.addupdateprofilecapicall.AddUpdateCustomerCInteractor
import aCsa.cgs.com.ui.screens.apis.addupdateprofilecapicall.AddUpdateCustomerDInteractor
import aCsa.cgs.com.ui.screens.apis.addupdateprofilecapicall.AddUpdateCustomerEInteractor
import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.addupdateprofilecapicall.model.CustomerAddUpdateCRequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofilecapicall.model.CustomerAddUpdateCResponseModel
import absa.cgs.com.ui.screens.apis.addupdateprofiledapicall.model.CustomerAddUpdateDResponseModel
import absa.cgs.com.ui.screens.apis.addupdateprofileeapicall.model.CustomerAddUpdateERequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofileeapicall.model.CustomerAddUpdateEResponseModel
import absa.cgs.com.ui.screens.authentication.AuthenticationBaseActivity
import absa.cgs.com.ui.screens.base.BasePresenter
import absa.cgs.com.utils.CommonUtils
import javax.inject.Inject

class AddUpdateCustomerEPresenter<View : AddUodateCustomerEView> @Inject constructor(var addUpdateCustomerEInteractor: AddUpdateCustomerEInteractor, private val commonUtils: CommonUtils) : BasePresenter<View>(), IAddUpdateCustomerEListener<View>, AddUpdateCustomerEInteractor.OnCallHitListener {


    override fun postCustomerEApiCall() {
        var customerAddUpdateERequestModel = CustomerAddUpdateERequestModel(getBaseMvpVieww().getUserId(), getBaseMvpVieww().getCustomerId(), getBaseMvpVieww().getCustomerEId(), getBaseMvpVieww().getCustomerEregisterDate(), getBaseMvpVieww().getCustomerEConnectionDate())
        addUpdateCustomerEInteractor.postAddUpdateCustomerDDataToServer(customerAddUpdateERequestModel, this)
    }

    override fun onSuccessAddUpdateCustomerEInteractListener(customerAddUpdateEResponseModel: CustomerAddUpdateEResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().onSuccessAddUpdateCustomerEResponse(customerAddUpdateEResponseModel)
        getBaseMvpVieww().showToastLong(commonUtils.cutNull(customerAddUpdateEResponseModel.message))
    }

    override fun onRetrofitFailureAddUpdateCustomerEInteractListener(error: String) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(error)
    }

    override fun onSessionExpireAddUpdateCustomerEInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        activity!!.finishAffinity()
        getBaseMvpVieww().navigationRoutes(AuthenticationBaseActivity::class.java)
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.SessionTokenExpire))
    }

    override fun onErrorAddUpdateCustomerEInteractListener(customerAddUpdateEResponseModel: CustomerAddUpdateEResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(customerAddUpdateEResponseModel.message)
    }

    override fun onServerExceptionAddUpdateCustomerEInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.ServerBusyString))
    }

    override fun validateAddUpdateCustomerE() {
        if (getBaseMvpVieww().getCustomerEConnectionDate().equals("")) {
            getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.CustomerNameAString))
        } else if (getBaseMvpVieww().getCustomerEregisterDate().equals("")) {
            getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.CustomerAreaAString))
        } else {
            getBaseMvpVieww().postAddUpdateCustomerE()
        }
    }
}