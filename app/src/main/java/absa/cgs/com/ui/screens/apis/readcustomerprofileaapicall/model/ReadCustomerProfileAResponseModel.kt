package absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.model

class ReadCustomerProfileAResponseModel(var status_code: Int, var message: String, var profile_a_details: CustomerProfileADetails)