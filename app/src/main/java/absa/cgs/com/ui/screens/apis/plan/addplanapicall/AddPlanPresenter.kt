package absa.cgs.com.ui.screens.apis.plan.addplanapicall

import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.addexpenseapicall.model.AddExpenseRequestModel
import absa.cgs.com.ui.screens.apis.addexpenseapicall.model.AddExpenseResponseModel
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateARequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateAResponseModel
import absa.cgs.com.ui.screens.apis.plan.addplanapicall.model.AddPlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.addplanapicall.model.AddPlanResponseModel
import absa.cgs.com.ui.screens.authentication.AuthenticationBaseActivity
import absa.cgs.com.ui.screens.base.BasePresenter
import absa.cgs.com.utils.CommonUtils
import absa.cgs.com.utils.enums.CommonEnumUtils
import javax.inject.Inject

class AddPlanPresenter<View : AddPlanView> @Inject constructor(var addPlanInteractor: AddPlanInteractor, private val commonUtils: CommonUtils) : BasePresenter<View>(), IAddPlanListener<View>, AddPlanInteractor.OnCallHitListener {


    override fun postPlanApiCall() {
        var addPlanRequestModel = AddPlanRequestModel(getBaseMvpVieww().getPlanName(), getBaseMvpVieww().getPlanCost(), getBaseMvpVieww().getPlanCode(), getBaseMvpVieww().getPlanType(), getBaseMvpVieww().getPlanGst(), getBaseMvpVieww().getPlanArrayData())
        addPlanInteractor.postAddPlanDataToServer(addPlanRequestModel, this)
    }

    override fun onSuccessAddPlanInteractListener(addPlanResponseModel: AddPlanResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().onSuccessAddPlanResponse(addPlanResponseModel)
        getBaseMvpVieww().showToastLong(commonUtils.cutNull(addPlanResponseModel.message))
    }

    override fun onRetrofitFailureAddPlanInteractListener(error: String) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(error)
    }

    override fun onSessionExpireAddPlanInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        activity!!.finishAffinity()
        getBaseMvpVieww().navigationRoutes(AuthenticationBaseActivity::class.java)
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.SessionTokenExpire))
    }

    override fun onErrorAddPlanInteractListener(addPlanResponseModel: AddPlanResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(addPlanResponseModel.message)
    }

    override fun onServerExceptionAddPlanInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.ServerBusyString))
    }

    override fun validatePlanDetails() {

        when (getBaseMvpVieww().getPlanType()) {

            CommonEnumUtils.FREE.toString() -> {
                if (getBaseMvpVieww().getPlanName().equals("")) {
                    getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.NeedPlanName))
                } else if (!getBaseMvpVieww().validatePlanTypeRadioGroupChecked()) {
                    getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.NeedPlanType))
                } else if (getBaseMvpVieww().getPlanCode().equals("")) {
                    getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.NeedPlanCode))
                } else {
                    getBaseMvpVieww().postNewPlan()
                }
            }
            else -> {
                if (getBaseMvpVieww().getPlanName().equals("")) {
                    getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.NeedPlanName))
                } else if (!getBaseMvpVieww().validatePlanTypeRadioGroupChecked()) {
                    getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.NeedPlanType))
                } else if (getBaseMvpVieww().getPlanCode().equals("")) {
                    getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.NeedPlanCode))
                } else if (getBaseMvpVieww().getPlanCost().equals("") or getBaseMvpVieww().getPlanCost().equals("0")) {
                    getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.NeedPlanCost))
                } else if (getBaseMvpVieww().getPlanGst().equals("")) {
                    getBaseMvpVieww().showToastLong(getActivityy().getString(R.string.NeedPlanGst))
                } else {
                    getBaseMvpVieww().postNewPlan()
                }
            }
        }

    }
}