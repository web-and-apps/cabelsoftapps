package absa.cgs.com.ui.screens.apis.plan.deleteplanapicall

import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateAResponseModel
import absa.cgs.com.ui.screens.apis.plan.addplanapicall.model.AddPlanResponseModel
import absa.cgs.com.ui.screens.apis.plan.deleteplanapicall.model.DeletePlanResponseModel
import absa.cgs.com.ui.screens.base.BaseMvpView

interface DeletePlanView : BaseMvpView {

    fun onSuccessDeletePlanResponse(deletePlanResponseModel: DeletePlanResponseModel)
    fun onFailureDeletePlanResponse(error: String)

    fun getPlanId(): String


}