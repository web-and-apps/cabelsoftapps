package absa.cgs.com.ui.screens.plan.planchildfragment.addplan

import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.plan.addplanapicall.AddPlanPresenter
import absa.cgs.com.ui.screens.apis.plan.addplanapicall.AddPlanView
import absa.cgs.com.ui.screens.apis.plan.addplanapicall.model.AddPlanResponseModel
import absa.cgs.com.ui.screens.apis.plan.getparticularplanapicall.GetParticularPlanPresenter
import absa.cgs.com.ui.screens.apis.plan.getparticularplanapicall.GetParticularPlanView
import absa.cgs.com.ui.screens.apis.plan.getparticularplanapicall.model.GetPlanResponseModel
import absa.cgs.com.ui.screens.base.BaseFragment
import absa.cgs.com.ui.screens.mainbaseactivity.OnListItemClickInterface
import absa.cgs.com.ui.screens.plan.PlanBaseActivity
import absa.cgs.com.ui.screens.plan.planchildfragment.planupdate.adapter.PlanGetAllChannelAdapter
import absa.cgs.com.utils.RecyclerViewItemDecorator
import absa.cgs.com.utils.SingletonUtils
import absa.cgs.com.utils.enums.CommonEnumUtils
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_add_plan_screen.*
import javax.inject.Inject
import android.widget.RadioGroup
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.fragment_plan_update_screen.*
import javax.inject.Singleton


class AddPlanFragment : BaseFragment(), AddPlanView, GetParticularPlanView, PlanBaseActivity.OnBackPressedListner {


    lateinit var planBaseActivity: PlanBaseActivity

    @Inject
    lateinit var addPlanPresenter: AddPlanPresenter<AddPlanView>

    private var planType: String = CommonEnumUtils.FREE.toString()

    private var planGetAllChannelAdapter: PlanGetAllChannelAdapter? = null

    @Inject
    lateinit var getParticularPlanPresenter: GetParticularPlanPresenter<GetParticularPlanView>

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        fragmentComponent().inject(this)
        addPlanPresenter.attachView(activity!!, this)
        getParticularPlanPresenter.attachView(activity!!, this)
    }


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_add_plan_screen, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    override fun init() {
        planBaseActivity = activity as PlanBaseActivity
        recyclerViewInit()
        onClickListeners()
    }

    override fun onClickListeners() {
        super.onClickListeners()
        addNewPlanSaveBtn.setOnClickListener {
            addPlanPresenter.validatePlanDetails()
        }
        getProductTypeCheckedRadioButtonValue()
        addNewChannelSetOnClickListener()
    }

    override fun onSuccessAddPlanResponse(addPlanResponseModel: AddPlanResponseModel) {
        activity?.finish()
    }

    override fun onFailureAddPlanResponse(error: String) {

    }

    override fun getPlanName(): String {
        return addPlanPlanNameEd.text.toString()
    }

    override fun getPlanCost(): String {
        return addPlanPlanCostEd.text.toString()
    }

    override fun getPlanCode(): String {
        return addPlanPlanCodeEd.text.toString()
    }

    override fun getPlanType(): String {
        return planType
    }

    override fun getPlanGst(): String {
        return addPlanPlanGstEd.text.toString()
    }

    override fun getPlanArrayData(): String {
        return SingletonUtils.instance.planArrayData
    }

    override fun postNewPlan() {
        progressLoadingBar()
        addPlanPresenter.postPlanApiCall()
    }

    override fun getProductTypeCheckedRadioButtonValue() {
        addPlanTypeRadioGroup.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->
            val id = addPlanTypeRadioGroup.checkedRadioButtonId
            when (id) {
                R.id.addPlanTypeRdFree -> {
                    planType = CommonEnumUtils.FREE.toString()
                    addNewChannelConstraints.visibility = View.GONE
                    addPlanPlanCostTi.visibility = View.GONE
                    addPlanPlanGstTi.visibility = View.GONE
                }

                R.id.addPlanTypeRdPaid -> {
                    planType = CommonEnumUtils.PAID.toString()
                    addNewChannelConstraints.visibility = View.GONE
                    addPlanPlanCostTi.visibility = View.VISIBLE
                    addPlanPlanGstTi.visibility = View.VISIBLE
                }

                R.id.addPlanTypeRdBouquets -> {
                    planType = CommonEnumUtils.BOUQ.toString()
                    addNewChannelConstraints.visibility = View.VISIBLE
                    addPlanPlanCostTi.visibility = View.VISIBLE
                    addPlanPlanGstTi.visibility = View.VISIBLE
                }
            }
        })

    }

    private fun addNewChannelSetOnClickListener() {
        addChannelEditTv.setOnClickListener {
            val args = Bundle()
            planBaseActivity.changeFragment(7, args)

        }
    }

    override fun validatePlanTypeRadioGroupChecked(): Boolean {
        if (addPlanTypeRadioGroup.checkedRadioButtonId == -1) {
            return false
        }
        return true
    }


    override fun onBackPressed(): Boolean {
        activity?.finish()
        return false
    }

    override fun getPlanIds(): String {
        return SingletonUtils.instance.planArrayData
    }


    override fun onSuccessGetParticularPlanAddChannelResponse(getPlanResponseModel: GetPlanResponseModel) {
        planGetAllChannelAdapter = PlanGetAllChannelAdapter(activity!!, getPlanResponseModel, object : OnListItemClickInterface {
            override fun OnSelectedItemClickListener(title: String, position: Int) {

            }
        }
        )
        addPlanDetailsAddChannelRcv?.adapter = planGetAllChannelAdapter
        planGetAllChannelAdapter!!.notifyDataSetChanged()
    }

    override fun onFailureGetParticularPlanAddChannelResponse(error: String) {

    }

    override fun hitGetParticularPlanChannels() {
        progressLoadingBar()
        getParticularPlanPresenter.getParticularPlanApiCall()
    }

    private fun recyclerViewInit() {
        val mLayoutManager = GridLayoutManager(activity!!, 2)
        addPlanDetailsAddChannelRcv?.layoutManager = mLayoutManager
        addPlanDetailsAddChannelRcv.addItemDecoration(RecyclerViewItemDecorator(1))
    }

    override fun onResume() {
        super.onResume()
        hitGetParticularPlanChannels()
    }

}