package absa.cgs.com.ui.screens.plan.planchildfragment.updatenewchannels.adapter

import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.plan.getparticularplanapicall.model.GetPlanResponseModel
import absa.cgs.com.ui.screens.mainbaseactivity.OnListItemClickInterface
import absa.cgs.com.utils.enums.CommonEnumUtils
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.chip.Chip


class PlanUpdateNewChannelAdapter(context: Context, private val getPlanResponseModel: GetPlanResponseModel, val onListItemClickInterface: OnListItemClickInterface) : RecyclerView.Adapter<PlanUpdateNewChannelAdapter.DemoAdapter>() {

    var selectedListItem: MutableList<String> = mutableListOf<String>()
    private val lastSelectedPosition = -1

    inner class DemoAdapter(view: View) : RecyclerView.ViewHolder(view) {
        val planAddChannelChip: Chip


        init {
            planAddChannelChip = view.findViewById(R.id.planAddMultipleChannelChip) as Chip
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DemoAdapter {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.custom_add_multiple_channel_layout, parent, false)
        return DemoAdapter(itemView)
    }

    override fun onBindViewHolder(holder: DemoAdapter, position: Int) {


        when (getPlanResponseModel.product_details.get(position).plan_type) {
            CommonEnumUtils.FREE.toString() -> {
                holder.planAddChannelChip.text = (getPlanResponseModel.product_details.get(position).plan_name)
            }
            else -> {
                holder.planAddChannelChip.text = (getPlanResponseModel.product_details.get(position).plan_name + " ( Rs. " + getPlanResponseModel.product_details.get(position).plan_cost + " )")
            }
        }



        when (getPlanResponseModel.product_details.get(position).selection) {
            true -> {
                holder.planAddChannelChip.setChipBackgroundColorResource(R.color.colorGreen)
            }

            false -> {
                holder.planAddChannelChip.setChipBackgroundColorResource(R.color.colorPrimary)
            }
        }

        holder.planAddChannelChip.tag = (position)
        holder.planAddChannelChip.setOnClickListener { view ->

            if (lastSelectedPosition > 0) {
                getPlanResponseModel.product_details[lastSelectedPosition].selection = false
            }

            getPlanResponseModel.product_details[position].selection = !getPlanResponseModel.product_details[position].selection
            notifyDataSetChanged()
        }
    }

    private fun getAllChannelDatas() {

    }

    fun getPlanResponseModel(): GetPlanResponseModel {
        return getPlanResponseModel
    }


    override fun getItemCount(): Int {
        return getPlanResponseModel.product_details.size
    }
}






