package absa.cgs.com.ui.screens.apis.plan.getplanapicall

import absa.cgs.com.ui.screens.apis.plan.getparticularplanapicall.model.GetPlanResponseModel
import absa.cgs.com.ui.screens.base.BaseMvpView

interface GetPlanView : BaseMvpView {

    fun onSuccessGetParticularPlanResponse(getPlanResponseModel: GetPlanResponseModel)
    fun onFailureGetParticularPlanResponse(error: String)

    fun getPlanIds(): String
    fun getFromDate():String
    fun getToDate():String
    fun getPageCount():String
    fun getSearchKeyword():String
    fun getSearchTag():String
    fun hitGetPlanDetails()


}