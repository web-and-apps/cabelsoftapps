package absa.cgs.com.ui.screens.apis.addupdateprofilecapicall

import absa.cgs.com.ui.screens.apis.addupdateprofilecapicall.model.CustomerAddUpdateCResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilecapicall.model.ReadCustomerProfileCResponseModel
import absa.cgs.com.ui.screens.base.BaseMvpView

interface AddUodateCustomerCView : BaseMvpView {

    fun onSuccessAddUpdateCustomerCResponse(customerAddUpdateCResponseModel: CustomerAddUpdateCResponseModel)
    fun onFailureAddUpdateCustomerCResponse(error: String)

    fun getUserId(): String
    fun getCustomerId(): String
    fun getCustomerCId(): String
    fun getCustomerCBillType(): String
    fun getCustomeCBillTime(): String
    fun getCustomerCAdditionalCharge(): String
    fun postAddUpdateCustomerC()


}