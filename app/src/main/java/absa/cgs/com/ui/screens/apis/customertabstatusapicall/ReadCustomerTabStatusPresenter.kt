package absa.cgs.com.ui.screens.apis.customertabstatusapicall

import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.customertabstatusapicall.model.CustomerTabStatusRequestModel
import absa.cgs.com.ui.screens.apis.customertabstatusapicall.model.CustomerTabStatusResponseModel
import absa.cgs.com.ui.screens.authentication.AuthenticationBaseActivity
import absa.cgs.com.ui.screens.base.BasePresenter
import absa.cgs.com.utils.CommonUtils
import javax.inject.Inject

class ReadCustomerTabStatusPresenter<View : ReadCustomerTabStatusView> @Inject constructor(var readCustomerTabStatusInteractor: ReadCustomerTabStatusInteractor, private val commonUtils: CommonUtils) : BasePresenter<View>(), ICustomerTabStatusListener<View>, ReadCustomerTabStatusInteractor.OnCallHitListener {
    override fun readCustomerTabStatusData() {
        val readProfileRequestModel = CustomerTabStatusRequestModel(getBaseMvpVieww().getCustomerId())
        readCustomerTabStatusInteractor.getCustomerTabStatusDataToServer(readProfileRequestModel, this)
    }

    override fun onSuccessCustomerTabStatusInteractListener(customerTabStatusResponseModel: CustomerTabStatusResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().onSuccessReadCustomerTabStatusResponse(customerTabStatusResponseModel)
    }

    override fun onRetrofitFailureCustomerTabStatusInteractListener(error: String) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(error)
    }

    override fun onSessionExpireCustomerTabStatusInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        activity!!.finishAffinity()
        getBaseMvpVieww().navigationRoutes(AuthenticationBaseActivity::class.java)
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.SessionTokenExpire))
    }

    override fun onErrorCustomerTabStatusInteractListener(customerTabStatusResponseModel: CustomerTabStatusResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        //getBaseMvpVieww().showToastLong(customerTabStatusResponseModel.message)
    }

    override fun onServerExceptionCustomerTabStatusInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.ServerBusyString))
    }


}
