package aCsa.cgs.com.ui.screens.apis.addupdateprofilecapicall

import absa.cgs.com.data.RetrofitClient

import absa.cgs.com.ui.screens.apis.addupdateprofilecapicall.model.CustomerAddUpdateCRequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofilecapicall.model.CustomerAddUpdateCResponseModel
import absa.cgs.com.utils.enums.HttpEnum
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AddUpdateCustomerCInteractor @Inject constructor() {
    private var customerAddUpdateCResponseModel: CustomerAddUpdateCResponseModel? = null


    interface OnCallHitListener {
        fun onSuccessAddUpdateCustomerCInteractListener(customerAddUpdateCResponseModel: CustomerAddUpdateCResponseModel)
        fun onRetrofitFailureAddUpdateCustomerCInteractListener(error: String)
        fun onSessionExpireAddUpdateCustomerCInteractListener()
        fun onErrorAddUpdateCustomerCInteractListener(customerAddUpdateCResponseModel: CustomerAddUpdateCResponseModel)
        fun onServerExceptionAddUpdateCustomerCInteractListener()
    }

    fun postAddUpdateCustomerCDataToServer(customerAddUpdateCRequestModel: CustomerAddUpdateCRequestModel, listener: OnCallHitListener) {
        RetrofitClient.instance.addUpdateCustomerC(customerAddUpdateCRequestModel)
                .enqueue(object : Callback<CustomerAddUpdateCResponseModel> {
                    override fun onFailure(call: Call<CustomerAddUpdateCResponseModel>, t: Throwable) {
                        listener.onRetrofitFailureAddUpdateCustomerCInteractListener(t.message.toString())
                    }

                    override fun onResponse(call: Call<CustomerAddUpdateCResponseModel>, response: Response<CustomerAddUpdateCResponseModel>) {

                        when (response.code()) {
                            HttpEnum.STATUS_UNAUTHORIZED.code -> {
                                listener.onSessionExpireAddUpdateCustomerCInteractListener()
                            }

                            HttpEnum.STATUS_ERROR.code -> {
                                customerAddUpdateCResponseModel = response.body()
                                listener.onErrorAddUpdateCustomerCInteractListener(customerAddUpdateCResponseModel!!)
                            }

                            HttpEnum.STATUS_OK.code -> {
                                customerAddUpdateCResponseModel = response.body()
                                listener.onSuccessAddUpdateCustomerCInteractListener(customerAddUpdateCResponseModel!!)
                            }

                            else -> {
                                listener.onServerExceptionAddUpdateCustomerCInteractListener()
                            }
                        }

                    }

                })
    }
}