package absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall


import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.model.ReadCustomerProfileARequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.model.ReadCustomerProfileAResponseModel
import absa.cgs.com.ui.screens.apis.readexpenseapicall.model.ReadExpenseRequestModel
import absa.cgs.com.ui.screens.apis.readexpenseapicall.model.ReadExpenseResponseModel
import absa.cgs.com.ui.screens.authentication.AuthenticationBaseActivity
import absa.cgs.com.ui.screens.base.BasePresenter
import javax.inject.Inject

class ReadCustomerProfileAPresenter<View : ReadCustomerProfileAView> @Inject constructor(var readCustomerProfileAInteractor: ReadCustomerProfileAInteractor) : BasePresenter<View>(), IReadCustomerProfileAListener<View>, ReadCustomerProfileAInteractor.OnCallHitListener {
    override fun readCustomerProfileAData() {
        val readExpenseRequestModel = ReadCustomerProfileARequestModel(
                getBaseMvpVieww().getCustomerID())
        readCustomerProfileAInteractor.getReadCustomerProfileADataToServer(readExpenseRequestModel, this)
    }

    override fun onSuccessReadCustomerAProfileInteractListener(readCustomerProfileAResponseModel: ReadCustomerProfileAResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().onSuccessReadCustomerProfileAResponse(readCustomerProfileAResponseModel)
    }

    override fun onRetrofitFailureReadCustomerAProfileInteractListener(error: String) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(error)
    }

    override fun onSessionExpireReadCustomerAProfileInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        activity!!.finishAffinity()
        getBaseMvpVieww().navigationRoutes(AuthenticationBaseActivity::class.java)
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.SessionTokenExpire))
    }

    override fun onErrorReadCustomerAProfileInteractListener(readCustomerProfileAResponseModel: ReadCustomerProfileAResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        //getBaseMvpVieww().showToastLong(readCustomerProfileAResponseModel.message)
    }

    override fun onServerExceptionReadCustomerAProfileInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.ServerBusyString))
    }


}
