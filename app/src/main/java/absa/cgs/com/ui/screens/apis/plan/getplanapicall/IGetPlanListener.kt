package absa.cgs.com.ui.screens.apis.plan.getplanapicall

import absa.cgs.com.ui.screens.base.BaseMvpPresenter

interface IGetPlanListener<View : GetPlanView> : BaseMvpPresenter<View> {
    fun getPlanApiCall()

}