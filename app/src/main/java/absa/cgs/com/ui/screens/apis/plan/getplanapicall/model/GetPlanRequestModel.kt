package absa.cgs.com.ui.screens.apis.plan.getplanapicall.model

data class GetPlanRequestModel(var from_date: String, var to_date: String, var page_count: String, var search_keyword: String, var search_tag: String)