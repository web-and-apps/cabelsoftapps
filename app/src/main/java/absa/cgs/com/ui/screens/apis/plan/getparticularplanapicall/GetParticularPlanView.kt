package absa.cgs.com.ui.screens.apis.plan.getparticularplanapicall

import absa.cgs.com.ui.screens.apis.plan.getparticularplanapicall.model.GetPlanResponseModel
import absa.cgs.com.ui.screens.base.BaseMvpView

interface GetParticularPlanView : BaseMvpView {

    fun onSuccessGetParticularPlanAddChannelResponse(getPlanResponseModel: GetPlanResponseModel)
    fun onFailureGetParticularPlanAddChannelResponse(error: String)

    fun getPlanIds(): String

    fun hitGetParticularPlanChannels()
}