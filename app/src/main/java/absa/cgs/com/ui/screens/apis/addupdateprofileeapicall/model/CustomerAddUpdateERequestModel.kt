package absa.cgs.com.ui.screens.apis.addupdateprofileeapicall.model

data class CustomerAddUpdateERequestModel(var user_id: String, var customer_id: String,
                                          var customer_e_id: String,
                                          var customer_e_register_date: String, var customer_e_connection_date: String)