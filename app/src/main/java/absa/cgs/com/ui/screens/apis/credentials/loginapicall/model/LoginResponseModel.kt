package absa.cgs.com.ui.screens.apis.credentials.loginapicall.model

data class LoginResponseModel(val status: Int, val message: String, val user_details: LoginUserDetailModel)