package absa.cgs.com.ui.screens.apis.deleteexpenseapicall.model

data class DeleteExpenseResponseModel(var status_code: Int, var message: String)