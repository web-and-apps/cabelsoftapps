package absa.cgs.com.ui.screens.apis.readcustomerprofilecapicall


import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.model.ReadCustomerProfileARequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.model.ReadCustomerProfileAResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilebapicall.model.ReadCustomerProfileBRequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilebapicall.model.ReadCustomerProfileBResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilecapicall.model.ReadCustomerProfileCRequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilecapicall.model.ReadCustomerProfileCResponseModel
import absa.cgs.com.ui.screens.apis.readexpenseapicall.model.ReadExpenseRequestModel
import absa.cgs.com.ui.screens.apis.readexpenseapicall.model.ReadExpenseResponseModel
import absa.cgs.com.ui.screens.authentication.AuthenticationBaseActivity
import absa.cgs.com.ui.screens.base.BasePresenter
import javax.inject.Inject

class ReadCustomerProfileCPresenter<View : ReadCustomerProfileCView> @Inject constructor(var readCustomerProfileCInteractor: ReadCustomerProfileCInteractor) : BasePresenter<View>(), IReadCustomerProfileCListener<View>, ReadCustomerProfileCInteractor.OnCallHitListener {
    override fun readCustomerProfileCData() {
        val readCustomerProfileCRequestModel = ReadCustomerProfileCRequestModel(
                getBaseMvpVieww().getCustomerID())
        readCustomerProfileCInteractor.getReadCustomerProfileCDataToServer(readCustomerProfileCRequestModel, this)
    }

    override fun onSuccessReadCustomerCProfileInteractListener(readCustomerProfileCResponseModel: ReadCustomerProfileCResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().onSuccessReadCustomerProfileCResponse(readCustomerProfileCResponseModel)
    }

    override fun onRetrofitFailureReadCustomerCProfileInteractListener(error: String) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(error)
    }

    override fun onSessionExpireReadCustomerCProfileInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        activity!!.finishAffinity()
        getBaseMvpVieww().navigationRoutes(AuthenticationBaseActivity::class.java)
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.SessionTokenExpire))
    }

    override fun onErrorReadCustomerCProfileInteractListener(readCustomerProfileCResponseModel: ReadCustomerProfileCResponseModel) {
        getBaseMvpVieww().hideProgressLoadingDialog()
        //getBaseMvpVieww().showToastLong(readCustomerProfileCResponseModel.message)
    }

    override fun onServerExceptionReadCustomerCProfileInteractListener() {
        getBaseMvpVieww().hideProgressLoadingDialog()
        getBaseMvpVieww().showToastLong(getActivityy().resources.getString(R.string.ServerBusyString))
    }


}
