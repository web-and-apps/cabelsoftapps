package absa.cgs.com.ui.screens.apis.plan.updateplanapicall.model

data class UpdatePlanResponseModel(var status_code: Int, var message: String)