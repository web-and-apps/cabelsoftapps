package absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.model

data class CustomerProfileADetails(var customer_id: String, var user_id: String,
                                   var customer_name: String, var customer_area: String,
                                   var customer_street: String, var customer_home_type: String,
                                   var customer_mobile: String, var customer_date: String)