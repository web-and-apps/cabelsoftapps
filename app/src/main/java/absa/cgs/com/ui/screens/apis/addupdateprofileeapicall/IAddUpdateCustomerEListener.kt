package absa.cgs.com.ui.screens.apis.addupdateprofileeapicall

import absa.cgs.com.ui.screens.base.BaseMvpPresenter

interface IAddUpdateCustomerEListener<View : AddUodateCustomerEView> : BaseMvpPresenter<View> {
    fun postCustomerEApiCall()
    fun validateAddUpdateCustomerE()
}