package absa.cgs.com.ui.screens.apis.getallcustomerapicall.model

data class GetAllCustomerRequestModel(var user_id: String, var from_date: String, var to_date: String, var page_count: String, var search_keyword: String)