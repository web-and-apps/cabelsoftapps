package absa.cgs.com.ui.screens.apis.updateimagesapicall.model

data class UpdateImageResponseModel(var status_code: Int, var message: String)