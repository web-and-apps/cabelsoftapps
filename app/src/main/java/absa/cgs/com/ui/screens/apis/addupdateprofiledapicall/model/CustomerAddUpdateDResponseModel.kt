package absa.cgs.com.ui.screens.apis.addupdateprofiledapicall.model

class CustomerAddUpdateDResponseModel(var status_code: Int, var message: String, var operation: String)