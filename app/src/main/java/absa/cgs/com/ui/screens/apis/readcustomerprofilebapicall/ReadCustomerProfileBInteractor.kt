package absa.cgs.com.ui.screens.apis.readcustomerprofilebapicall

import absa.cgs.com.data.RetrofitClient
import absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.model.ReadCustomerProfileARequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.model.ReadCustomerProfileAResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilebapicall.model.ReadCustomerProfileBRequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilebapicall.model.ReadCustomerProfileBResponseModel
import absa.cgs.com.ui.screens.apis.readprofileapicall.model.ReadProfileRequestModel
import absa.cgs.com.ui.screens.apis.readprofileapicall.model.ReadProfileResponseModel
import absa.cgs.com.utils.enums.HttpEnum
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ReadCustomerProfileBInteractor @Inject constructor() {

    private var readCustomerProfileBResponseModel: ReadCustomerProfileBResponseModel? = null


    interface OnCallHitListener {
        fun onSuccessReadCustomerBProfileInteractListener(readCustomerProfileBResponseModel: ReadCustomerProfileBResponseModel)
        fun onRetrofitFailureReadCustomerBProfileInteractListener(error: String)
        fun onSessionExpireReadCustomerBProfileInteractListener()
        fun onErrorReadCustomerBProfileInteractListener(readCustomerProfileBResponseModel: ReadCustomerProfileBResponseModel)
        fun onServerExceptionReadCustomerBProfileInteractListener()
    }


    fun getReadCustomerProfileBDataToServer(readCustomerProfileBRequestModel: ReadCustomerProfileBRequestModel, listener: OnCallHitListener) {
        RetrofitClient.instance.readCustomerProfileBData(readCustomerProfileBRequestModel)
                .enqueue(object : Callback<ReadCustomerProfileBResponseModel> {
                    override fun onFailure(call: Call<ReadCustomerProfileBResponseModel>, t: Throwable) {
                        listener.onRetrofitFailureReadCustomerBProfileInteractListener(t.message.toString())
                    }

                    override fun onResponse(call: Call<ReadCustomerProfileBResponseModel>, response: Response<ReadCustomerProfileBResponseModel>) {
                        when (response.code()) {
                            HttpEnum.STATUS_UNAUTHORIZED.code -> {
                                listener.onSessionExpireReadCustomerBProfileInteractListener()
                            }

                            HttpEnum.STATUS_ERROR.code -> {
                                readCustomerProfileBResponseModel = response.body()
                                listener.onErrorReadCustomerBProfileInteractListener(readCustomerProfileBResponseModel!!)
                            }

                            HttpEnum.STATUS_OK.code -> {
                                readCustomerProfileBResponseModel = response.body()
                                listener.onSuccessReadCustomerBProfileInteractListener(readCustomerProfileBResponseModel!!)
                            }

                            else -> {
                                listener.onServerExceptionReadCustomerBProfileInteractListener()
                            }
                        }

                    }

                })
    }

}
