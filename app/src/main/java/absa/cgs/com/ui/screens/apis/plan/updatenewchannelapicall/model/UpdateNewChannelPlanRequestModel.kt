package absa.cgs.com.ui.screens.apis.plan.updatenewchannelapicall.model

data class UpdateNewChannelPlanRequestModel(var plan_id: String, var plan_array_data: String)