package absa.cgs.com.ui.screens.apis.addupdateprofilecapicall

import absa.cgs.com.ui.screens.base.BaseMvpPresenter

interface IAddUpdateCustomerCListener<View : AddUodateCustomerCView> : BaseMvpPresenter<View> {
    fun postCustomerCApiCall()
    fun validateAddUpdateCustomerC()
}