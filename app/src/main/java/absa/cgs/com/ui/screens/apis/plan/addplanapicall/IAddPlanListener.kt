package absa.cgs.com.ui.screens.apis.plan.addplanapicall

import absa.cgs.com.ui.screens.base.BaseMvpPresenter

interface IAddPlanListener<View : AddPlanView> : BaseMvpPresenter<View> {
    fun postPlanApiCall()
    fun validatePlanDetails()
}