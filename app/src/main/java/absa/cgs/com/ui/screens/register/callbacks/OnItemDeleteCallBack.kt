package absa.cgs.com.ui.screens.register.callbacks

import java.text.FieldPosition

interface OnItemDeleteCallBack {

    fun onItemDeleteListener(position: Int)
}