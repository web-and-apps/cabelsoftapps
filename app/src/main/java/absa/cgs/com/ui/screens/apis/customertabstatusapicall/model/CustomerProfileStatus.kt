package absa.cgs.com.ui.screens.apis.customertabstatusapicall.model

data class CustomerProfileStatus(var customer_status_id: String, var user_id: String,
                                 var customer_id: String, var customer_a_status: String,
                                 var customer_b_status: String, var customer_c_status: String,
                                 var customer_d_status: String, var customer_e_status: String,
                                 var customer_status_date: String)