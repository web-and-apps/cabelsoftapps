package absa.cgs.com.ui.screens.apis.getallcustomerapicall

import absa.cgs.com.data.RetrofitClient
import absa.cgs.com.ui.screens.apis.getallcustomerapicall.model.GetAllCustomerRequestModel
import absa.cgs.com.ui.screens.apis.getallcustomerapicall.model.GetAllCustomerResponseModel
import absa.cgs.com.ui.screens.apis.readexpenseapicall.model.ReadExpenseRequestModel
import absa.cgs.com.ui.screens.apis.readexpenseapicall.model.ReadExpenseResponseModel
import absa.cgs.com.utils.enums.HttpEnum
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GetCustomerInteractor @Inject constructor() {

    private var getAllCustomerResponseModel: GetAllCustomerResponseModel? = null


    interface OnCallHitListener {
        fun onSuccessGetCustomerInteractListener(getAllCustomerResponseModel: GetAllCustomerResponseModel)
        fun onRetrofitFailureGetCustomerInteractListener(error: String)
        fun onSessionExpireGetCustomerInteractListener()
        fun onErrorGetCustomerInteractListener(getAllCustomerResponseModel: GetAllCustomerResponseModel)
        fun onServerExceptionGetCustomerInteractListener()
    }


    fun getCustomerDataToServer(getAllCustomerRequestModel: GetAllCustomerRequestModel, listener: OnCallHitListener) {
        RetrofitClient.instance.readAllCustomerData(getAllCustomerRequestModel)
                .enqueue(object : Callback<GetAllCustomerResponseModel> {
                    override fun onFailure(call: Call<GetAllCustomerResponseModel>, t: Throwable) {
                        listener.onRetrofitFailureGetCustomerInteractListener(t.message.toString())
                    }

                    override fun onResponse(call: Call<GetAllCustomerResponseModel>, response: Response<GetAllCustomerResponseModel>) {
                        when (response.code()) {
                            HttpEnum.STATUS_UNAUTHORIZED.code -> {
                                listener.onSessionExpireGetCustomerInteractListener()
                            }

                            HttpEnum.STATUS_ERROR.code -> {
                                getAllCustomerResponseModel = response.body()
                                listener.onErrorGetCustomerInteractListener(getAllCustomerResponseModel!!)
                            }

                            HttpEnum.STATUS_OK.code -> {
                                getAllCustomerResponseModel = response.body()
                                listener.onSuccessGetCustomerInteractListener(getAllCustomerResponseModel!!)
                            }

                            else -> {
                                listener.onServerExceptionGetCustomerInteractListener()
                            }
                        }

                    }

                })
    }

}
