package absa.cgs.com.ui.screens.apis.credentials.logoutapicall

import absa.cgs.com.ui.screens.base.BaseMvpView

interface LogoutView : BaseMvpView {
    fun onSuccessLogoutResponse(message: String)
    fun onFailureLogoutResponse(error: String)

    fun postLogout()

    fun naviageToLoginScreen()

    fun getUserID(): String
}