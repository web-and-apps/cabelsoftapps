package absa.cgs.com.ui.screens.plan.planchildfragment.planupdate.adapter

import absa.cgs.com.kotlinplayground.R
import absa.cgs.com.ui.screens.apis.plan.getparticularplanapicall.model.GetPlanResponseModel
import absa.cgs.com.ui.screens.mainbaseactivity.OnListItemClickInterface
import absa.cgs.com.utils.enums.CommonEnumUtils
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.chip.Chip


class PlanGetAllChannelAdapter(context: Context, private val getPlanResponseModel: GetPlanResponseModel, val onListItemClickInterface: OnListItemClickInterface) : RecyclerView.Adapter<PlanGetAllChannelAdapter.DemoAdapter>() {


    inner class DemoAdapter(view: View) : RecyclerView.ViewHolder(view) {
        var planAddChannelChip: Chip


        init {
            planAddChannelChip = view.findViewById(R.id.planAddChannelChip) as Chip

        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DemoAdapter {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.custom_add_channel_layout, parent, false)
        return DemoAdapter(itemView)
    }

    override fun onBindViewHolder(holder: DemoAdapter, position: Int) {
      
        when (getPlanResponseModel.product_details.get(position).plan_type) {
            CommonEnumUtils.FREE.toString() -> {
                holder.planAddChannelChip.text = (getPlanResponseModel.product_details.get(position).plan_name)
            }
            else -> {
                holder.planAddChannelChip.text = (getPlanResponseModel.product_details.get(position).plan_name + " ( Rs. " + getPlanResponseModel.product_details.get(position).plan_cost + " )")
            }
        }

    }


    override fun getItemCount(): Int {
        return getPlanResponseModel.product_details.size
    }
}
