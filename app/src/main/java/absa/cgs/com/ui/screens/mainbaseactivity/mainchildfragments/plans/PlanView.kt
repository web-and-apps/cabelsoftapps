package absa.cgs.com.ui.screens.mainbaseactivity.mainchildfragments.plans

interface PlanView {


    fun getAllBundleDatas()

    fun setAppBarTitle(title: Int)
}