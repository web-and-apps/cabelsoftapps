package absa.cgs.com.ui.screens.apis.updatenomineeapicall.model

data class UpdateNomineeResponseModel(var status_code: Int, var message: String)