package absa.cgs.com.ui.screens.apis.addupdateprofilebapicall

import absa.cgs.com.ui.screens.apis.addupdateprofilebapicall.model.CustomerAddUpdateBResponseModel
import absa.cgs.com.ui.screens.base.BaseMvpView

interface AddUodateCustomerBView : BaseMvpView {

    fun onSuccessAddUpdateCustomerBResponse(customerAddUpdateBResponseModel: CustomerAddUpdateBResponseModel)
    fun onFailureAddUpdateCustomerBResponse(error: String)

    fun getUserId(): String
    fun getCustomerId(): String
    fun getCustomerBId(): String
    fun getcustomerBBillingName(): String
    fun getCustomerBEmail(): String
    fun getCustomerBAadharNo(): String
    fun getCustomerBEbNo(): String
    fun getCustomerBPanNo(): String
    fun getCustomerBSecurityDeposite(): String
    fun postAddUpdateCustomerB()

}