package absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.model

data class ReadCustomerProfileARequestModel(var customer_id: String)