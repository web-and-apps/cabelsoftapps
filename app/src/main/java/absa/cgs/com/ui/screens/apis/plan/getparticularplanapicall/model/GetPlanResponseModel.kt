package absa.cgs.com.ui.screens.apis.plan.getparticularplanapicall.model

data class GetPlanResponseModel(var status_code: Int, var message: String, var product_details: List<GetParticularPlanProductDetails>)