package absa.cgs.com.ui.screens.apis.readcustomerprofilebapicall

import absa.cgs.com.ui.screens.base.BaseMvpPresenter


interface IReadCustomerProfileBListener<View : ReadCustomerProfileBView> : BaseMvpPresenter<View> {

    fun readCustomerProfileBData()


}
