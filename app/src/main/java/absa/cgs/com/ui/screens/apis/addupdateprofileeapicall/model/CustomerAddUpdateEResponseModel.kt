package absa.cgs.com.ui.screens.apis.addupdateprofileeapicall.model

class CustomerAddUpdateEResponseModel(var status_code: Int, var message: String, var operation: String)