package absa.cgs.com.ui.screens.apis.plan.deleteplanapicall

import absa.cgs.com.data.RetrofitClient
import absa.cgs.com.ui.screens.apis.addexpenseapicall.model.AddExpenseRequestModel
import absa.cgs.com.ui.screens.apis.addexpenseapicall.model.AddExpenseResponseModel
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateARequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateAResponseModel
import absa.cgs.com.ui.screens.apis.plan.addplanapicall.model.AddPlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.addplanapicall.model.AddPlanResponseModel
import absa.cgs.com.ui.screens.apis.plan.deleteplanapicall.model.DeletePlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.deleteplanapicall.model.DeletePlanResponseModel
import absa.cgs.com.utils.enums.HttpEnum
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DeletePlanInteractor @Inject constructor() {
    private var deletePlanResponseModel: DeletePlanResponseModel? = null


    interface OnCallHitListener {
        fun onSuccessDeletePlanInteractListener(deletePlanResponseModel: DeletePlanResponseModel)
        fun onRetrofitFailureDeletePlanInteractListener(error: String)
        fun onSessionExpireDeletePlanInteractListener()
        fun onErrorDeletePlanInteractListener(deletePlanResponseModel: DeletePlanResponseModel)
        fun onServerExceptionDeletePlanInteractListener()
    }

    fun deletePlanDataToServer(deletePlanRequestModel: DeletePlanRequestModel, listener: OnCallHitListener) {
        RetrofitClient.instance.deletePlanDetails(deletePlanRequestModel)
                .enqueue(object : Callback<DeletePlanResponseModel> {
                    override fun onFailure(call: Call<DeletePlanResponseModel>, t: Throwable) {
                        listener.onRetrofitFailureDeletePlanInteractListener(t.message.toString())
                    }

                    override fun onResponse(call: Call<DeletePlanResponseModel>, response: Response<DeletePlanResponseModel>) {

                        when (response.code()) {
                            HttpEnum.STATUS_UNAUTHORIZED.code -> {
                                listener.onSessionExpireDeletePlanInteractListener()
                            }

                            HttpEnum.STATUS_ERROR.code -> {
                                deletePlanResponseModel = response.body()
                                listener.onErrorDeletePlanInteractListener(deletePlanResponseModel!!)
                            }

                            HttpEnum.STATUS_OK.code -> {
                                deletePlanResponseModel = response.body()
                                listener.onSuccessDeletePlanInteractListener(deletePlanResponseModel!!)
                            }

                            else -> {
                                listener.onServerExceptionDeletePlanInteractListener()
                            }
                        }

                    }

                })
    }
}