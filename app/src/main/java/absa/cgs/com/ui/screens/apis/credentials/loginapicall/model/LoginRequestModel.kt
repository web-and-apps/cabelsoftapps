package absa.cgs.com.ui.screens.apis.credentials.loginapicall.model

data class LoginRequestModel(val user_mobile_number: String, val user_password: String)