package absa.cgs.com.ui.screens.apis.credentials.loginapicall.model

data class LoginUserDetailModel(val user_id: String, val user_name: String, val user_mailid: String, val user_mobile_number: String, val user_access_token: String, val role_id: String)