package absa.cgs.com.di.component


import absa.cgs.com.di.annotation.PerFragment
import absa.cgs.com.di.module.FragmentModule
import absa.cgs.com.ui.screens.authentication.AuthChildfragments.AuthLoginFragment
import absa.cgs.com.ui.screens.customer.customerchildfragment.customerbilling.CustomerBillingFragment
import absa.cgs.com.ui.screens.customer.customerchildfragment.customerdetails.CustomerFragment
import absa.cgs.com.ui.screens.customer.customerchildfragment.customerentydate.CustomerEntryDateFragment
import absa.cgs.com.ui.screens.customer.customerchildfragment.customerprofileA.CustomerProfileAFragment
import absa.cgs.com.ui.screens.customer.customerchildfragment.customerprofileB.CustomerProfileBFragment
import absa.cgs.com.ui.screens.customer.customerchildfragment.customersettopbox.CustomerSettopBoxFragment
import absa.cgs.com.ui.screens.expense.expensechildfragment.addexpense.AddExpenseFragment
import absa.cgs.com.ui.screens.expense.expensechildfragment.editviewexpense.EditExpenseFragment
import absa.cgs.com.ui.screens.expense.expensechildfragment.expensedetails.ExpenseDetailsFragment
import absa.cgs.com.ui.screens.plan.planchildfragment.addplan.AddPlanFragment
import absa.cgs.com.ui.screens.plan.planchildfragment.plandetails.PlanDetailsFragment
import absa.cgs.com.ui.screens.mainbaseactivity.mainchildfragments.plans.PlanFragment
import absa.cgs.com.ui.screens.plan.planchildfragment.addnewchannels.PlanAddNewChannelFragment
import absa.cgs.com.ui.screens.plan.planchildfragment.updatenewchannels.PlanUpdateNewChannelFragment
import absa.cgs.com.ui.screens.plan.planchildfragment.planupdate.PlanUpdateFragment
import absa.cgs.com.ui.screens.profile.profilechildfragment.updatebank.UpdateBankFragment
import absa.cgs.com.ui.screens.profile.profilechildfragment.updatebank.ViewBankProofFragment
import absa.cgs.com.ui.screens.profile.profilechildfragment.updatenominee.UpdateNomineeFragment
import absa.cgs.com.ui.screens.profile.profilechildfragment.updateprofile.UpdateProfileFragment
import dagger.Component

@PerFragment
@Component(dependencies = [ApplicationComponent::class], modules = [FragmentModule::class])
interface FragmentComponent {
    fun inject(authLoginFragment: AuthLoginFragment)
    fun inject(addExpenseFragment: AddExpenseFragment)
    fun inject(expenseDetailsFragment: ExpenseDetailsFragment)
    fun inject(editExpenseFragment: EditExpenseFragment)
    fun inject(updateProfileFragment: UpdateProfileFragment)
    fun inject(updateNomineeFragment: UpdateNomineeFragment)
    fun inject(updateBankFragment: UpdateBankFragment)
    fun inject(viewBankProofFragment: ViewBankProofFragment)
    fun inject(customerProfileAFragment: CustomerProfileAFragment)
    fun inject(customerProfileBFragment: CustomerProfileBFragment)
    fun inject(customerBillingFragment: CustomerBillingFragment)
    fun inject(customerSettopBoxFragment: CustomerSettopBoxFragment)
    fun inject(customerEntryDateFragment: CustomerEntryDateFragment)
    fun inject(customerFragment: CustomerFragment)
    fun inject(planFragment: PlanFragment)
    fun inject(planDetailsFragment: PlanDetailsFragment)
    fun inject(addPlanFragment: AddPlanFragment)
    fun inject(planUpdateFragment: PlanUpdateFragment)
    fun inject(planUpdateNewChannelFragment: PlanUpdateNewChannelFragment)
    fun inject(planAddNewChannelFragment: PlanAddNewChannelFragment)


}

