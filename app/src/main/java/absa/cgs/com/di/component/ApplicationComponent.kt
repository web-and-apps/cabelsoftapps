/*
 *    Copyright (C) 2018 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package absa.cgs.com.di.component

import aCsa.cgs.com.ui.screens.apis.addupdateprofilecapicall.AddUpdateCustomerCInteractor
import aCsa.cgs.com.ui.screens.apis.addupdateprofilecapicall.AddUpdateCustomerDInteractor
import aCsa.cgs.com.ui.screens.apis.addupdateprofilecapicall.AddUpdateCustomerEInteractor
import android.app.Application
import android.content.Context


import javax.inject.Singleton

import absa.cgs.com.MyApplication
import absa.cgs.com.di.module.ApplicationModule
import absa.cgs.com.ui.screens.apis.addexpenseapicall.AddExpenseInteractor
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.AddUpdateCustomerAInteractor
import absa.cgs.com.ui.screens.apis.addupdateprofilebapicall.AddUpdateCustomerBInteractor
import absa.cgs.com.ui.screens.apis.customertabstatusapicall.ReadCustomerTabStatusInteractor
import absa.cgs.com.ui.screens.apis.deletecustomerapicall.DeleteCustomerInteractor
import absa.cgs.com.ui.screens.apis.deleteexpenseapicall.DeleteExpenseInteractor
import absa.cgs.com.ui.screens.apis.getallcustomerapicall.GetCustomerInteractor
import absa.cgs.com.ui.screens.apis.getnomineerelation.GetNomineeRelationInteractor
import absa.cgs.com.ui.screens.apis.credentials.loginapicall.LoginInteractor
import absa.cgs.com.ui.screens.apis.credentials.logoutapicall.LogoutInteractor
import absa.cgs.com.ui.screens.apis.plan.addplanapicall.AddPlanInteractor
import absa.cgs.com.ui.screens.apis.plan.deleteplanapicall.DeletePlanInteractor
import absa.cgs.com.ui.screens.apis.plan.getparticularplanapicall.GetParticularPlanInteractor
import absa.cgs.com.ui.screens.apis.plan.getplanapicall.GetPlanInteractor
import absa.cgs.com.ui.screens.apis.plan.updatenewchannelapicall.UpdateNewChannelPlanInteractor
import absa.cgs.com.ui.screens.apis.plan.updateplanapicall.UpdatePlanInteractor
import absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.ReadCustomerProfileAInteractor
import absa.cgs.com.ui.screens.apis.readcustomerprofilebapicall.ReadCustomerProfileBInteractor
import absa.cgs.com.ui.screens.apis.readcustomerprofilecapicall.ReadCustomerProfileCInteractor
import absa.cgs.com.ui.screens.apis.readcustomerprofiledapicall.ReadCustomerProfileDInteractor
import absa.cgs.com.ui.screens.apis.readcustomerprofileeapicall.ReadCustomerProfileEInteractor
import absa.cgs.com.ui.screens.apis.readexpenseapicall.ReadExpenseInteractor
import absa.cgs.com.ui.screens.apis.readprofileapicall.ReadProfileInteractor
import absa.cgs.com.ui.screens.apis.updatebankapicall.UpdateBankInteractor
import absa.cgs.com.ui.screens.apis.updateexpenseapicall.UpdateExpenseInteractor
import absa.cgs.com.ui.screens.apis.updateimagesapicall.UpdateImageInteractor
import absa.cgs.com.ui.screens.apis.updatenomineeapicall.UpdateNomineeInteractor
import absa.cgs.com.ui.screens.apis.updateprofileapicall.UpdateProfileInteractor
import absa.cgs.com.ui.screens.mainbaseactivity.MainInteractor
import absa.cgs.com.utils.CommonUtils
import absa.cgs.com.utils.DialogUtils
import absa.cgs.com.utils.SessionUtils
import dagger.Component

/**
 * Created by amitshekhar on 13/01/17.
 */
@Singleton
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {

    fun inject(myApplication: MyApplication)

    //    @ApplicationContext
//    fun context(): Context
    //fun commonUtils(): CommonUtils

    fun provideContext(): Context
    fun application(): Application
    fun mainInteractor(): MainInteractor
    fun loginInteractor(): LoginInteractor
    fun logoutInteractor(): LogoutInteractor
    fun addExpenseInteractor(): AddExpenseInteractor
    fun updateExpenseInteractor(): UpdateExpenseInteractor
    fun deleteExpenseInteractor(): DeleteExpenseInteractor
    fun updateProfileInteractor(): UpdateProfileInteractor
    fun readProfileInteractor(): ReadProfileInteractor
    fun readExpenseInteractor(): ReadExpenseInteractor
    fun updateNomineeInteractor(): UpdateNomineeInteractor
    fun updateBankInteractor(): UpdateBankInteractor
    fun updateImageInteractor(): UpdateImageInteractor
    fun getNomineeRelationInteractor(): GetNomineeRelationInteractor
    fun getCustomerInteractor(): GetCustomerInteractor
    fun deleteCustomerInteractor(): DeleteCustomerInteractor
    fun readCustomerTabStatusInteractor(): ReadCustomerTabStatusInteractor
    fun readCustomerProfileAInteractor(): ReadCustomerProfileAInteractor
    fun readCustomerProfileBInteractor(): ReadCustomerProfileBInteractor
    fun readCustomerProfileCInteractor(): ReadCustomerProfileCInteractor
    fun readCustomerProfileDInteractor(): ReadCustomerProfileDInteractor
    fun readCustomerProfileEInteractor(): ReadCustomerProfileEInteractor
    fun addUpdateCustomerAInteractor(): AddUpdateCustomerAInteractor
    fun addUpdateCustomerBInteractor(): AddUpdateCustomerBInteractor
    fun addUpdateCustomerCInteractor(): AddUpdateCustomerCInteractor
    fun addUpdateCustomerDInteractor(): AddUpdateCustomerDInteractor
    fun addUpdateCustomerEInteractor(): AddUpdateCustomerEInteractor
    fun addPlanInteractor(): AddPlanInteractor
    fun updatePlanInteractor(): UpdatePlanInteractor
    fun deletePlanInteractor(): DeletePlanInteractor
    fun getParticularPlanInteractor(): GetParticularPlanInteractor
    fun getPlanInteractor(): GetPlanInteractor
    fun updateNewChannelPlanInteractor(): UpdateNewChannelPlanInteractor


    fun commonUtils(): CommonUtils
    fun dialogUtils(): DialogUtils
    fun sessionUtils(): SessionUtils


    // DataManager dataManager();

}
