package absa.cgs.com.utils


class SingletonUtils {

    private object HOLDER {
        val INSTANCE = SingletonUtils()
    }

    companion object {
        val instance: SingletonUtils by lazy { HOLDER.INSTANCE }
    }

    var authToken = ""
    var userId = ""
    var expenseId = ""
    var planId = ""
    var customerId = ""
    var customerBId = ""
    var customerCId = ""
    var customerEId = ""
    var planArrayData = ""

    //val IMAGE_URL = "http://192.168.43.147/cablesoft/api/"
    val IMAGE_URL = "http://dev.cablesoftapi.cabelsoft.in/"
    //val IMAGE_URL = "https://cabelsoft.000webhostapp.com/cablesoft/api/"

    fun clearSingletonCustomerDatas() {
        customerId = ""
        customerBId = ""
        customerCId = ""
        customerEId = ""
        planArrayData = ""
        expenseId = ""
        planId = ""

    }
}