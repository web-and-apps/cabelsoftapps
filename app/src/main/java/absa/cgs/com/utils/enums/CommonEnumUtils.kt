package absa.cgs.com.utils.enums

enum class CommonEnumUtils {

    DELETE,
    VIEW,
    NOMINEE,
    BANK,
    BANNER,
    PAN,
    ADDRESS,
    PROFILE,
    Y,
    N,
    P,
    ADDED,
    UPDATED,
    FREE,
    PAID,
    BOUQ,
    ADDPLAN

}