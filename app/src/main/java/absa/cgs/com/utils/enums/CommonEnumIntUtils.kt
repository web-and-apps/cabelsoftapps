package absa.cgs.com.utils.enums

enum class CommonEnumIntUtils(val values: Int) {
    GALLERY(1),
    CAMERA(2),
    MY_PERMISSIONS_REQUEST(3)
}