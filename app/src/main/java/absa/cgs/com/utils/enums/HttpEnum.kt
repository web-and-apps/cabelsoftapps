package absa.cgs.com.utils.enums

enum class HttpEnum(val code: Int) {

    STATUS_OK(200),
    STATUS_ERROR(201),
    STATUS_UNAUTHORIZED(401)

}