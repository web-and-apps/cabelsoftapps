package absa.cgs.com.data

import absa.cgs.com.ui.screens.apis.addexpenseapicall.model.AddExpenseRequestModel
import absa.cgs.com.ui.screens.apis.addexpenseapicall.model.AddExpenseResponseModel
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateARequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofileaapicall.model.CustomerAddUpdateAResponseModel
import absa.cgs.com.ui.screens.apis.addupdateprofilebapicall.model.CustomerAddUpdateBRequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofilebapicall.model.CustomerAddUpdateBResponseModel
import absa.cgs.com.ui.screens.apis.addupdateprofilecapicall.model.CustomerAddUpdateCRequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofilecapicall.model.CustomerAddUpdateCResponseModel
import absa.cgs.com.ui.screens.apis.addupdateprofiledapicall.model.CustomerAddUpdateDRequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofiledapicall.model.CustomerAddUpdateDResponseModel
import absa.cgs.com.ui.screens.apis.addupdateprofileeapicall.model.CustomerAddUpdateERequestModel
import absa.cgs.com.ui.screens.apis.addupdateprofileeapicall.model.CustomerAddUpdateEResponseModel
import absa.cgs.com.ui.screens.apis.customertabstatusapicall.model.CustomerTabStatusRequestModel
import absa.cgs.com.ui.screens.apis.customertabstatusapicall.model.CustomerTabStatusResponseModel
import absa.cgs.com.ui.screens.apis.deletecustomerapicall.model.DeleteCustomerRequestModel
import absa.cgs.com.ui.screens.apis.deletecustomerapicall.model.DeleteCustomerResponseModel
import absa.cgs.com.ui.screens.apis.deleteexpenseapicall.model.DeleteExpenseRequestModel
import absa.cgs.com.ui.screens.apis.deleteexpenseapicall.model.DeleteExpenseResponseModel
import absa.cgs.com.ui.screens.apis.getallcustomerapicall.model.GetAllCustomerRequestModel
import absa.cgs.com.ui.screens.apis.getallcustomerapicall.model.GetAllCustomerResponseModel
import absa.cgs.com.ui.screens.apis.getnomineerelation.model.NomineeRelationResponseModel
import absa.cgs.com.ui.screens.apis.credentials.loginapicall.model.LoginRequestModel
import absa.cgs.com.ui.screens.apis.credentials.loginapicall.model.LoginResponseModel
import absa.cgs.com.ui.screens.apis.credentials.logoutapicall.model.LogoutRequestModel
import absa.cgs.com.ui.screens.apis.credentials.logoutapicall.model.LogoutResponseModel
import absa.cgs.com.ui.screens.apis.plan.addplanapicall.model.AddPlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.addplanapicall.model.AddPlanResponseModel
import absa.cgs.com.ui.screens.apis.plan.deleteplanapicall.model.DeletePlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.deleteplanapicall.model.DeletePlanResponseModel
import absa.cgs.com.ui.screens.apis.plan.getparticularplanapicall.model.GetParticularPlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.getparticularplanapicall.model.GetPlanResponseModel
import absa.cgs.com.ui.screens.apis.plan.getplanapicall.model.GetPlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.updatenewchannelapicall.model.UpdateNewChannelPlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.updateplanapicall.model.UpdatePlanRequestModel
import absa.cgs.com.ui.screens.apis.plan.updateplanapicall.model.UpdatePlanResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.model.ReadCustomerProfileARequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofileaapicall.model.ReadCustomerProfileAResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilebapicall.model.ReadCustomerProfileBRequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilebapicall.model.ReadCustomerProfileBResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilecapicall.model.ReadCustomerProfileCRequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofilecapicall.model.ReadCustomerProfileCResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofiledapicall.model.ReadCustomerProfileDRequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofiledapicall.model.ReadCustomerProfileDResponseModel
import absa.cgs.com.ui.screens.apis.readcustomerprofileeapicall.model.ReadCustomerProfileERequestModel
import absa.cgs.com.ui.screens.apis.readcustomerprofileeapicall.model.ReadCustomerProfileEResponseModel
import absa.cgs.com.ui.screens.apis.readexpenseapicall.model.ReadExpenseRequestModel
import absa.cgs.com.ui.screens.apis.readexpenseapicall.model.ReadExpenseResponseModel
import absa.cgs.com.ui.screens.apis.readprofileapicall.model.ReadProfileRequestModel
import absa.cgs.com.ui.screens.apis.readprofileapicall.model.ReadProfileResponseModel
import absa.cgs.com.ui.screens.apis.updatebankapicall.model.UpdateBankRequestModel
import absa.cgs.com.ui.screens.apis.updatebankapicall.model.UpdateBankResponseModel
import absa.cgs.com.ui.screens.apis.updateexpenseapicall.model.UpdateExpenseRequestModel
import absa.cgs.com.ui.screens.apis.updateexpenseapicall.model.UpdateExpenseResponseModel
import absa.cgs.com.ui.screens.apis.updateimagesapicall.model.UpdateImageRequestModel
import absa.cgs.com.ui.screens.apis.updateimagesapicall.model.UpdateImageResponseModel
import absa.cgs.com.ui.screens.apis.updatenomineeapicall.model.UpdateNomineeRequestModel
import absa.cgs.com.ui.screens.apis.updatenomineeapicall.model.UpdateNomineeResponseModel
import absa.cgs.com.ui.screens.apis.updateprofileapicall.model.UpdateProfileRequestModel
import absa.cgs.com.ui.screens.apis.updateprofileapicall.model.UpdateProfileResponseModel
import absa.cgs.com.ui.screens.mainbaseactivity.MainRequest
import retrofit2.Call
import retrofit2.http.*


interface Api {

    @POST("category/GetCategoryList")
    fun createUser(@Body body: MainRequest): Call<DefaultResponse>

    @POST("/cabelsoftapi/json/userLogin")
    fun userLogin(@Body body: LoginRequestModel): Call<LoginResponseModel>

    @POST("/cabelsoftapi/json/logout")
    fun userLogout(@Body body: LogoutRequestModel): Call<LogoutResponseModel>

    @POST("/cabelsoftapi/json/addNewExpense")
    fun addExpenseData(@Body body: AddExpenseRequestModel): Call<AddExpenseResponseModel>

    @POST("/cabelsoftapi/json/updateExpense")
    fun updateExpenseData(@Body body: UpdateExpenseRequestModel): Call<UpdateExpenseResponseModel>

    @POST("/cabelsoftapi/json/deleteExpense")
    fun deleteExpenseData(@Body body: DeleteExpenseRequestModel): Call<DeleteExpenseResponseModel>

    @POST("/cabelsoftapi/json/updateUserDetails")
    fun updateProfileData(@Body body: UpdateProfileRequestModel): Call<UpdateProfileResponseModel>

    @POST("/cabelsoftapi/json/getAllUserDetails")
    fun readProfileData(@Body body: ReadProfileRequestModel): Call<ReadProfileResponseModel>

    @POST("/cabelsoftapi/json/getExpenseDetails")
    fun readExpenseData(@Body body: ReadExpenseRequestModel): Call<ReadExpenseResponseModel>

    @POST("/cabelsoftapi/json/updateNomineeDetails")
    fun updateNomineeData(@Body body: UpdateNomineeRequestModel): Call<UpdateNomineeResponseModel>

    @POST("/cabelsoftapi/json/updateBankDetails")
    fun updateBankData(@Body body: UpdateBankRequestModel): Call<UpdateBankResponseModel>

    @POST("/cabelsoftapi/json/uploadImageCall")
    fun updateImageData(@Body body: UpdateImageRequestModel): Call<UpdateImageResponseModel>

    @GET("/cabelsoftapi/json/getNomineeRelations")
    fun getNomineeRelations(): Call<NomineeRelationResponseModel>

    @POST("/cabelsoftapi/json/getAllCustomerDetails")
    fun readAllCustomerData(@Body body: GetAllCustomerRequestModel): Call<GetAllCustomerResponseModel>

    @POST("/cabelsoftapi/json/deleteCustomer")
    fun deleteCustomerData(@Body body: DeleteCustomerRequestModel): Call<DeleteCustomerResponseModel>

    @POST("/cabelsoftapi/json/getCustomerTabStatus")
    fun readCustomerTabStatusData(@Body body: CustomerTabStatusRequestModel): Call<CustomerTabStatusResponseModel>

    @POST("/cabelsoftapi/json/getProfileADetails")
    fun readCustomerProfileAData(@Body body: ReadCustomerProfileARequestModel): Call<ReadCustomerProfileAResponseModel>

    @POST("/cabelsoftapi/json/getCustomerBDetails")
    fun readCustomerProfileBData(@Body body: ReadCustomerProfileBRequestModel): Call<ReadCustomerProfileBResponseModel>

    @POST("/cabelsoftapi/json/getCustomerCDetails")
    fun readCustomerProfileCData(@Body body: ReadCustomerProfileCRequestModel): Call<ReadCustomerProfileCResponseModel>

    @POST("/cabelsoftapi/json/getCustomerDDetails")
    fun readCustomerProfileDData(@Body body: ReadCustomerProfileDRequestModel): Call<ReadCustomerProfileDResponseModel>

    @POST("/cabelsoftapi/json/getCustomerEDetails")
    fun readCustomerProfileEData(@Body body: ReadCustomerProfileERequestModel): Call<ReadCustomerProfileEResponseModel>

    @POST("/cabelsoftapi/json/profileACustomer")
    fun addUpdateCustomerA(@Body body: CustomerAddUpdateARequestModel): Call<CustomerAddUpdateAResponseModel>

    @POST("/cabelsoftapi/json/profileBCustomer")
    fun addUpdateCustomerB(@Body body: CustomerAddUpdateBRequestModel): Call<CustomerAddUpdateBResponseModel>

    @POST("/cabelsoftapi/json/profileCCustomer")
    fun addUpdateCustomerC(@Body body: CustomerAddUpdateCRequestModel): Call<CustomerAddUpdateCResponseModel>

    @POST("/cabelsoftapi/json/profileDCustomer")
    fun addUpdateCustomerD(@Body body: CustomerAddUpdateDRequestModel): Call<CustomerAddUpdateDResponseModel>

    @POST("/cabelsoftapi/json/profileECustomer")
    fun addUpdateCustomerE(@Body body: CustomerAddUpdateERequestModel): Call<CustomerAddUpdateEResponseModel>

    @POST("/cabelsoftapi/json/addNewPlan")
    fun addNewPlanDetails(@Body body: AddPlanRequestModel): Call<AddPlanResponseModel>

    @POST("/cabelsoftapi/json/updatePlan")
    fun updatePlanDetails(@Body body: UpdatePlanRequestModel): Call<UpdatePlanResponseModel>

    @POST("/cabelsoftapi/json/deletePlan")
    fun deletePlanDetails(@Body body: DeletePlanRequestModel): Call<DeletePlanResponseModel>

    @POST("/cabelsoftapi/json/getParticularPlanDetails")
    fun getParticularPlanDetails(@Body body: GetParticularPlanRequestModel): Call<GetPlanResponseModel>

    @POST("/cabelsoftapi/json/getPlanDetails")
    fun getPlanDetails(@Body body: GetPlanRequestModel): Call<GetPlanResponseModel>

    @POST("/cabelsoftapi/json/updateChannelsPlanDetails")
    fun updateNewChannelsPlan(@Body body: UpdateNewChannelPlanRequestModel): Call<UpdatePlanResponseModel>
}