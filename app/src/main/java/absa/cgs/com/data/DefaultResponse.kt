package absa.cgs.com.data

data class DefaultResponse(val status: Int, val message: String)